//
//  LabelHeight.swift
//  StarClub
//
//  Created by macOS on 22.01.18.
//  Copyright © 2018 macOS. All rights reserved.
//

import Foundation
import UIKit

extension CGFloat {
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat {
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        
        return label.frame.height
    }
}
