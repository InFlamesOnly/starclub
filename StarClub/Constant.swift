//
//  Constant.swift
//  StarClub
//
//  Created by macOS on 12.12.17.
//  Copyright © 2017 macOS. All rights reserved.
//

import Foundation
import UIKit

let SUPPORTPHONENUMBER = "+380965695223"
let TELEPHONECODE = "+38 "
let BORDERRADIUS : CGFloat = 25
let CELLBORDERRADIUS : CGFloat = 10
let LIGHTGREY = UIColor(red: 151/255.0, green: 151/255.0, blue: 151/255.0, alpha: 1.0)
let PINK = UIColor(red: 9/255.0, green: 9/255.0, blue: 121/255.0, alpha: 1.0)
//old
//let PINK = UIColor(red: 230/255.0, green: 0/255.0, blue: 126/255.0, alpha: 1.0)

let BLACK = UIColor(red: 110/255.0, green: 110/255.0, blue: 112/255.0, alpha: 1.0)
let GREEN = UIColor(red: 126/255.0, green: 211/255.0, blue: 33/255.0, alpha: 1.0)
let LIGHTGREEN = UIColor(red: 222/255.0, green: 255/255.0, blue: 186/255.0, alpha: 1.0)
let LIGHTPINK = PINK.withAlphaComponent(0.3)
//old
//let LIGHTPINK = UIColor(red: 252/255.0, green: 229/255.0, blue: 242/255.0, alpha: 1.0)
