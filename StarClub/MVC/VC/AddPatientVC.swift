//
//  AddPatientVC.swift
//  StarClub
//
//  Created by Dima on 18.03.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit

class AddPatientVC: LightViewController {
    
    @IBOutlet weak var genderDropDown : DropDown!
    @IBOutlet weak var ageTextField : UITextField!
    @IBOutlet weak var nameTextField : UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.genderDropDown.optionArray = ["Чоловіча", "Жіноча"]
        
        self.genderDropDown.didSelect{(selectedText , index ,id) in
            self.genderDropDown.placeholder = selectedText
            self.genderDropDown.textAlignment = .center
        }
    }
    
    @IBAction func send(_ sender: Any) {
        var sendGender = "m"
        if  genderDropDown.placeholder == "Жіноча" || genderDropDown.text == "Жіноча" {
            sendGender = "f"
        }
        
        if self.ageTextField.text?.count == 0 && self.nameTextField.text?.count == 0 {
            self.alert(message: "Заповніть всі поля")
            return
        }
        
        if self.ageTextField.text?.count == 0 {
            self.alert(message: "Вкажіть вік")
            return
        }
        
        if self.nameTextField.text?.count == 0 {
            self.alert(message: "Вкажіть ім'я")
            return
        }
        
        let patient = Patient()
        patient.name = self.nameTextField.text!
        patient.sex = sendGender
        patient.age = self.ageTextField.text!
        
        self.send(patient: patient)
    }
    
    func send (patient : Patient) {
        RequsetManager.sharedInstance.sendPatient(patient: patient, success: { (info) in
            //todo next screen
        }) { (errorCode) in
            
        }
    }
}
