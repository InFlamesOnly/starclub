//
//  VideoCasesVC.swift
//  StarClub
//
//  Created by macOS on 4/4/21.
//  Copyright © 2021 macOS. All rights reserved.
//

import UIKit
import AlamofireImage

class VideoCasesVC: UIViewController {
    
    @IBOutlet weak var webView : UIWebView!
    let indicator = SpringIndicator()
    let user = User().currentUser()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.webView.isHidden = true
        self.createIndicator()

        let urlString = "http://kusumdoctors.co.ua/videocase/?utm_source=KD_videocase&token=\(user.token)&speciality=\(user.specific)"
        self.webView.loadRequest(URLRequest(url: URL(string: urlString)!))
        self.navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    func createIndicator () {
        indicator.frame.size = CGSize(width: 100, height: 100)
        indicator.center = self.view.center
        indicator.lineColor = PINK
        self.view.addSubview(indicator)
        indicator.start()
    }
}

extension VideoCasesVC : UIWebViewDelegate {
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.webView.isHidden = false
        self.indicator.removeFromSuperview()
    }
}
