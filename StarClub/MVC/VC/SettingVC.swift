//
//  SettingVC.swift
//  StarClub
//
//  Created by macOS on 23.12.17.
//  Copyright © 2017 macOS. All rights reserved.
//

import UIKit
import RealmSwift

class SettingVC: LightViewController {

    @IBOutlet weak var accountDropDown : DropDown!
    @IBOutlet weak var notificationSwitch: UISwitch!
    
    let user = User().currentUser()
    
    var accInfoArr = Array<Account>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getAccounts()
        
        self.accountDropDown.didSelect{(selectedText , index ,id) in
            let acc = self.accInfoArr[index]
            let loaderView = self.createLoaderView()
            RequsetManager.sharedInstance.getUserFromToken(token: acc.token, success: { (_ responseObject: Dictionary<String, Any>) in
                self.menu(success: { (response) in
                    loaderView.removeFromSuperview()
                    let vc = self.navigationController as! CustomeNC
                    vc.updateLebel()
                    self.accountDropDown.placeholder = selectedText
                    self.accountDropDown.textAlignment = .center
                    //DrawerMenuTVC
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let controller = storyboard.instantiateViewController(withIdentifier: "DrawerMenuTVC") as! DrawerMenuTVC
                    controller.viewDidAppear(true)
                    self.updateDrugsModels()
                    self.notificationSwitch.setOn(self.user.push.boolValue, animated: false)
                })
            }, failure: { (_ error: Int?) in
                loaderView.removeFromSuperview()
                RequsetManager.sharedInstance.showError(viewController: self, code: error!)
            })
        }
        notificationSwitch.setOn(user.push.boolValue, animated: false)
    }
    
    
    func menu (success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void) {
        RequsetManager.sharedInstance.isShowMedMenu(success: { (response) in
            let respBool = response ["show"] as? Int ?? 0
            if respBool == 1 {
                UserDefaults.standard.set(1, forKey: "isShow")
            } else {
                UserDefaults.standard.set(0, forKey: "isShow")
            }
            success (response)
        }) { (errorCode) in
            
        }
    }
    
    func updateDrugsModels () {
        let drug = Drug()
        drug.removeAllDrug()
        
        if self.user.specific == 2 {
            drug.addDrugsToGastroDataBase()
        } else if user.specific == 3 || user.specific == 6 || user.specific == 7{
            drug.addDrugsToCardioDataBase()
        } else if user.specific > 2 && user.specific < 6  {
            if user.specific == 5 {
                drug.allDrugs()
                return
            }
            drug.addDrugsToRealmDataBase()
        } else {
            drug.addDrugsToRealmDataBase()
        }
    }
    
    func getAccounts () {
        let loaderView = createLoaderView()
        RequsetManager.sharedInstance.getAccountsFromServer(success: { (_ responseObject: Dictionary<String, Any>) in
            var array = Array<Account>()
            if let artArray = responseObject["data"] as? [[String: Any]] {
                for articleDict in artArray {
                    let acc = Account(response: articleDict)
                    array.append(acc)
                }
            }
            self.accInfoArr = array
            self.addToTextField()
            loaderView.removeFromSuperview()
        }) { (_ error: Int?) in
            loaderView.removeFromSuperview()
            RequsetManager.sharedInstance.showError(viewController: self, code: error!)
        }
    }
    
    private func createLoaderView () -> LoaderView {
        let loaderView = LoaderView()
        loaderView.startFromView(view: self.view)
        return loaderView
    }
    
    func addToTextField () {
        var array = Array<String>()
        for acc in self.accInfoArr {
            array.append(acc.name)
            if user.token == acc.token {
                if acc.name != "" {
                    self.accountDropDown.text = acc.name
                    self.accountDropDown.placeholder = acc.name
                } else {
                    self.accountDropDown.text = acc.phone
                    self.accountDropDown.placeholder = acc.phone
                }
            }
        }
        self.accountDropDown.optionArray = array
    }
    
    @IBAction func changeNotificationState(_ sender: Any) {
        print("\(notificationSwitch.isOn.intValue)")
        RequsetManager.sharedInstance.changePush(state:notificationSwitch.isOn.intValue, success: { (_ responseObject: Dictionary<String, Any>) in
            let newUser = User()
            newUser.firebaseToken = self.user.firebaseToken
            newUser.specific = self.user.specific
            newUser.token = self.user.token
            newUser.name = self.user.name
            newUser.achievement = self.user.achievement
            newUser.push = self.notificationSwitch.isOn.intValue
            newUser.updateUser(newUser: newUser)
        }) { (_ error : Int?) in
            
        }
    }
    
    @IBAction func exit(_ sender: Any) {
        let newUser = User()
        let realm = try! Realm()
        let drugsArray = realm.objects(Drug.self)
        try! realm.write {
            realm.delete(drugsArray)
        }
        newUser.specific = 0
        newUser.firebaseToken = ""
        newUser.token = ""
        newUser.name = ""
        newUser.achievement = 0
        newUser.push = 0
        newUser.updateUser(newUser: newUser)
        if user.token == "" {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "StartVC") as! UINavigationController
            if #available(iOS 13.0, *) {
                controller.modalPresentationStyle = .fullScreen
            }
            self.present(controller, animated: true, completion: nil)
        }
    }
}

extension Int {
    var boolValue: Bool {
        if self == 0 {
            return false
        } else {
            return true
        }
    }
}

extension Bool {
    var intValue: Int {
        if self == false {
            return 0
        } else {
            return 1
        }
    }
}
