//
//  ArticlesTVC.swift
//  StarClub
//
//  Created by macOS on 20.12.17.
//  Copyright © 2017 macOS. All rights reserved.
//

import UIKit

class ArticlesC: UITableViewCell {

    @IBOutlet weak var roundView: UIView!
    @IBOutlet weak var articleTittle: UILabel!
//    @IBOutlet weak var articleHTML: UILabel!
    @IBOutlet weak var articleImageView: UIImageView!
    @IBOutlet weak var articleText: UILabel!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var feedTextView: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        roundView.layer.cornerRadius = CELLBORDERRADIUS
    }
}
