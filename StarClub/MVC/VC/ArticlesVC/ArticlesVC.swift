//
//  ArticlesVC.swift
//  StarClub
//
//  Created by macOS on 20.12.17.
//  Copyright © 2017 macOS. All rights reserved.
//

import UIKit
import AlamofireImage

class ArticlesVC: LightViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var articlesTableView: UITableView!
    @IBOutlet weak var errorLabel: UILabel!
    var articleArray : Array<Article> = Array()
    let indicator = SpringIndicator()

    var sendArticleArray : Array<Article> = Array()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.articlesTableView.isHidden = true
        errorLabel.isHidden = true
        self.getListFromServer()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super .viewWillDisappear(animated)
        self.hideBurger()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showBurger()
    }

    func hideBurger () {
        if self.navigationController is CustomeNC {
            let nc = self.navigationController as! CustomeNC
            nc.hideBurger()
        }
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }

    func showBurger () {
        if self.navigationController is CustomeNC {
            let nc = self.navigationController as! CustomeNC
            nc.showBurger()
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ArticlesDetail" {
            let vc = segue.destination as! ArticlesDetailVC
            vc.articleArray = self.sendArticleArray
        }
    }

    func createIndicator () {
        indicator.frame.size = CGSize(width: 100, height: 100)
        indicator.center = self.view.center
        indicator.lineColor = PINK
        self.view.addSubview(indicator)
        indicator.start()
    }

    func getListFromServer () {
        self.createIndicator()
        RequsetManager.sharedInstance.getArticlesFromServer(success: { (_ responseObject: Dictionary<String, Any>) in
            self.articleArray = Article.mapResponseToArrayObject(response: responseObject)
            if self .articleArray.count > 0 {
                self.articlesTableView.isHidden = false
            } else {
                self.errorLabel.isHidden = false
                self.articlesTableView.isHidden = true
            }
            self.articlesTableView.reloadData()
            self.indicator.removeFromSuperview()
        }) { (_ error: Int?) in
            self.indicator.removeFromSuperview()
            RequsetManager.sharedInstance.showError(viewController: self, code: error!)
        }

    }

    // MARK: - Table view data source
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.sendArticleArray = [self.articleArray[indexPath.row]]
        self.performSegue(withIdentifier: "ArticlesDetail", sender: self)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articleArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cellIdentifier = "ArticlesC"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! ArticlesC
        let article = self.articleArray[indexPath.row]
        cell.articleTittle.text = article.title
//        cell.articleText.text = article.content
        let attributedString = NSAttributedString(html: article.content)
        cell.feedTextView.attributedText = attributedString
        cell.feedTextView.font = UIFont(name: "AvenirNext-Medium", size: 15.0)
        cell.feedTextView.isScrollEnabled = false
        cell.articleImageView.af_setImage(withURL: URL(string: article.image)!, placeholderImage: nil)
        let font = UIFont(name: "AvenirNext-Medium", size: 17.0)
        let height = CGFloat().heightForView(text: cell.articleTittle.text!, font: font!, width: cell.roundView.frame.size.width - 34)
        cell.heightConstraint.constant = height
        cell.articleImageView.contentMode = .scaleAspectFit
        cell.selectionStyle = .none
        return cell
    }
}
