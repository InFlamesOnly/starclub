//
//  ConferencC.swift
//  StarClub
//
//  Created by macOS on 27.12.17.
//  Copyright © 2017 macOS. All rights reserved.
//

import UIKit

class ConferencC: UITableViewCell {

    @IBOutlet weak var roundView: UIView!
    @IBOutlet weak var conferenceTittle: UILabel!
    @IBOutlet weak var conferenceImageView: UIImageView!
    @IBOutlet weak var conferenceText: UILabel!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        roundView.layer.cornerRadius = CELLBORDERRADIUS
    }
}
