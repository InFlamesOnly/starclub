//
//  ConferencVC.swift
//  StarClub
//
//  Created by macOS on 27.12.17.
//  Copyright © 2017 macOS. All rights reserved.
//

import UIKit
import AlamofireImage

class ConferencVC: LightViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var conferenceTableView: UITableView!
    @IBOutlet weak var errorLabel: UILabel!
    var conferencArray : Array<Conference> = Array()
    let indicator = SpringIndicator()
    //AvenirNext-Medium
    override func viewDidLoad() {
        super.viewDidLoad()
        self.conferenceTableView.isHidden = true
        errorLabel.isHidden = true
        self.getListFromServer()
        
//        let font = UIFont(name: "AvenirNext-Medium", size: 17.0)
//
//        var height = heightForView(text: "This is just a load of text", font: font!, width: 100.0)
    }
    
    func createIndicator () {
        indicator.frame.size = CGSize(width: 100, height: 100)
        indicator.center = self.view.center
        indicator.lineColor = PINK
        self.view.addSubview(indicator)
        indicator.start()
    }
    
    func getListFromServer () {
        self.createIndicator()
        RequsetManager.sharedInstance.getConferenceFromServer(success: { (_ responseObject: Dictionary<String, Any>) in
            self.conferencArray = Conference.mapResponseToArrayObject(response: responseObject)
            if self .conferencArray.count > 0 {
                self.conferenceTableView.isHidden = false
            } else {
                self.errorLabel.isHidden = false
                self.conferenceTableView.isHidden = true
            }
            self.conferenceTableView.reloadData()
            self.indicator.removeFromSuperview()
        }) { (_ error: Int?) in
            self.indicator.removeFromSuperview()
            RequsetManager.sharedInstance.showError(viewController: self, code: error!)
        }
    }
    
    // MARK: - Table view data source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return conferencArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "ConferencC"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! ConferencC
        let conferenc = self.conferencArray[indexPath.row]
        cell.conferenceTittle.text = conferenc.title
        cell.conferenceText.text = conferenc.content
        cell.conferenceImageView.af_setImage(withURL: URL(string: conferenc.image)!, placeholderImage: nil)
        let font = UIFont(name: "AvenirNext-Medium", size: 17.0)
        let height = CGFloat().heightForView(text: cell.conferenceTittle.text!, font: font!, width: cell.roundView.frame.size.width - 34)
        cell.heightConstraint.constant = height
        cell.conferenceImageView.contentMode = .scaleAspectFit
        cell.selectionStyle = .none
        return cell
    }
}
