//
//  ArticleState.swift
//  StarClub
//
//  Created by Dima on 23.05.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit

class ArticleState: NSObject {
    
    var article = 0
    
    static let sharedInstance: ArticleState = {
        let instance = ArticleState()
        return instance
    }()
    
}
