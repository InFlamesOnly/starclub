//
//  ArticleAcademyVC.swift
//  StarClub
//
//  Created by Dima on 23.05.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit

class ArticleAcademyVC: LightViewController , UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var articlesTableView: UITableView!
    @IBOutlet weak var errorLabel: UILabel!
    var articleArray : Array<ArticleAcademy> = Array()
    let indicator = SpringIndicator()
    
    var selectedArticle : ArticleAcademy?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.articlesTableView.isHidden = true
        errorLabel.isHidden = true
        if ArticleState.sharedInstance.article != 0 {
            self.getListFromServerWith(id: ArticleState.sharedInstance.article)
        } else {
            self.getListFromServer()
        }
        print("\(ArticleState.sharedInstance.article)")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super .viewWillDisappear(animated)
        self.hideBurger()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showBurger()
    }
    
    func hideBurger () {
        if self.navigationController is CustomeNC {
            let nc = self.navigationController as! CustomeNC
            nc.hideBurger()
        }
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func showBurger () {
        if self.navigationController is CustomeNC {
            let nc = self.navigationController as! CustomeNC
            nc.showBurger()
        }
    }
    
//    if self.navigationController is CustomNavigationController {
//        let nc = self.navigationController as! CustomNavigationController
//        nc.hideBurger()
//    }
//    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ArticlesDetail" {
            let vc = segue.destination as! ArticleAcademyDetailVC
            vc.selectedArticle = self.selectedArticle
        }
    }
    
    func createIndicator () {
        indicator.frame.size = CGSize(width: 100, height: 100)
        indicator.center = self.view.center
        indicator.lineColor = PINK
        self.view.addSubview(indicator)
        indicator.start()
    }
    
    func getListFromServer () {
        self.createIndicator()
        RequsetManager.sharedInstance.getAcademy(success: { (_ responseObject: Dictionary<String, Any>) in
            self.articleArray = ArticleAcademy.mapResponseToArrayObject(response: responseObject)
            if self .articleArray.count > 0 {
                self.articlesTableView.isHidden = false
            } else {
                self.errorLabel.isHidden = false
                self.articlesTableView.isHidden = true
            }
            self.articlesTableView.reloadData()
            self.indicator.removeFromSuperview()
        }) { (_ error: Int?) in
            self.indicator.removeFromSuperview()
            RequsetManager.sharedInstance.showError(viewController: self, code: error!)
        }
    }
    
    func getListFromServerWith (id : Int) {
        self.createIndicator()
        RequsetManager.sharedInstance.getAcademyListWithId(categoryId: ArticleState.sharedInstance.article, success: { (_ responseObject: Dictionary<String, Any>) in
            self.articleArray = ArticleAcademy.mapResponseToArrayObject(response: responseObject)
            if self .articleArray.count > 0 {
                self.articlesTableView.isHidden = false
            } else {
                self.errorLabel.isHidden = false
                self.articlesTableView.isHidden = true
            }
            self.articlesTableView.reloadData()
            self.indicator.removeFromSuperview()
        }) { (_ error: Int?) in
            self.indicator.removeFromSuperview()
            RequsetManager.sharedInstance.showError(viewController: self, code: error!)
        }
    }
    
    // MARK: - Table view data source
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedArticle = self.articleArray[indexPath.row]
        self.performSegue(withIdentifier: "ArticlesDetail", sender: self)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articleArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "ArticlesAcademyC"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! ArticlesAcademyC
        let article = self.articleArray[indexPath.row]
        cell.articleTittle.text = article.title
        cell.shortContent.text = article.shortContent
        
        cell.detButton.tag = indexPath.row
        cell.detButton.addTarget(self, action: #selector(pressButton(_:)), for: .touchUpInside)
//        cell.category.text = "Категория : " + article.category
        cell.selectionStyle = .none
        return cell
    }
    
    @objc func pressButton(_ button: UIButton) {
        self.selectedArticle = self.articleArray[button.tag]
        self.performSegue(withIdentifier: "ArticlesDetail", sender: self)
    }
}
