//
//  ArticlesAcademyC.swift
//  StarClub
//
//  Created by Dima on 23.05.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit

class ArticlesAcademyC: UITableViewCell {

    @IBOutlet weak var roundView: UIView!
    @IBOutlet weak var articleTittle: UILabel!
    @IBOutlet weak var shortContent: UILabel!
    @IBOutlet weak var detButton: UIButton!
//    @IBOutlet weak var category: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        roundView.layer.cornerRadius = CELLBORDERRADIUS
    }

}
