//
//  ArticleAcademyDetailVC.swift
//  StarClub
//
//  Created by Dima on 23.05.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit

class ArticleAcademyDetailVC: LightViewController {
    
    @IBOutlet weak var webView : UIWebView!
    var selectedArticle : ArticleAcademy?
    
    let indicator = SpringIndicator()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.webView.isHidden = true
        self.createIndicator()
        //        self.url = "https://remedy-ua.com/api/v1/getform?token=zCBZ6OlU4ty2mkXC61fVuVUabl8q8qeU12gstVIqxAwdYNeU&list_id=28"
        self.webView.loadRequest(URLRequest(url: URL(string: selectedArticle!.link)!))
        self.navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    func createIndicator () {
        indicator.frame.size = CGSize(width: 100, height: 100)
        indicator.center = self.view.center
        indicator.lineColor = PINK
        self.view.addSubview(indicator)
        indicator.start()
    }
}

extension ArticleAcademyDetailVC : UIWebViewDelegate {
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.webView.isHidden = false
        self.indicator.removeFromSuperview()
    }
}
