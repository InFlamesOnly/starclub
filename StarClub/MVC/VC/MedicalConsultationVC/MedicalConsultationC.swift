//
//  MedicalConsultationC.swift
//  StarClub
//
//  Created by Dima on 23.04.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit

class MedicalConsultationC: UITableViewCell {
    
    @IBOutlet weak var roundView: UIView!
    @IBOutlet weak var isPrivateImage: UIImageView!
    @IBOutlet weak var isPrivateHeight: NSLayoutConstraint!
    @IBOutlet weak var isPrivateWidth: NSLayoutConstraint!
    @IBOutlet weak var quetionTittle: UILabel!
    @IBOutlet weak var quetion: UILabel!
    @IBOutlet weak var answer: UILabel!
    @IBOutlet weak var answerTextView: UITextView!
    @IBOutlet weak var moreButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        roundView.layer.cornerRadius = CELLBORDERRADIUS
    }
}
