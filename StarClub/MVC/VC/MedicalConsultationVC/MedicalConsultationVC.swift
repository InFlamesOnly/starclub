//
//  MedicalConsultationVC.swift
//  StarClub
//
//  Created by Dima on 23.04.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit

class MedicalConsultationVC: LightViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    @IBOutlet weak var questionTableView: UITableView!
    @IBOutlet weak var searchTextField: UITextFieldDelegate!
    let indicator = SpringIndicator()
    var isNewQuestion = true
    var selectedId = 0
    
    var search:String = ""
    var category:String = ""
    
    var legalConsultationArray : Array<LegalConsultation> = Array()
    var searchArray : Array<LegalConsultation> = Array()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if MedicalState.sharedInstance.esAg {
            category = "1"
        } else if MedicalState.sharedInstance.simptAg {
            category = "2"
        } else if MedicalState.sharedInstance.heart {
            category = "3"
        } else if MedicalState.sharedInstance.twoType {
            category = "4"
        } else if MedicalState.sharedInstance.difficultСases {
            category = "5"
        } else if MedicalState.sharedInstance.otherIssues {
            category = "6"
        }
        
        self.getMyListFromServer()
        self.hideKeyboardWhenTappedAround()
        self.questionTableView.isHidden = true
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super .viewWillDisappear(animated)
        self.hideBurger()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showBurger()
    }
    
    func hideBurger () {
        if self.navigationController is CustomeNC {
            let nc = self.navigationController as! CustomeNC
            nc.hideBurger()
        }
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func showBurger () {
        if self.navigationController is CustomeNC {
            let nc = self.navigationController as! CustomeNC
            nc.showBurger()
        }
    }
    
    func createIndicator () {
        indicator.frame.size = CGSize(width: 100, height: 100)
        indicator.center = self.view.center
        indicator.lineColor = PINK
        self.view.addSubview(indicator)
        indicator.start()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string.isEmpty {
            search = String(search.dropLast())
        } else {
            search = textField.text! + string
        }
        
        print(search)
        
        let filteredArray = self.legalConsultationArray.filter() { $0.text.contains(search) || $0.answer.contains(search)}
        
        if filteredArray.count > 0
        {
            searchArray.removeAll(keepingCapacity: true)
            searchArray = filteredArray
        }
        else
        {
            searchArray = self.legalConsultationArray
        }
        self.legalConsultationArray = searchArray
        self.questionTableView.reloadData()
        return true
    }
    
    func getMyListFromServer () {
        self.createIndicator()
        RequsetManager.sharedInstance.getMedConsultation(category : self.category,success: { (_ responseObject: Dictionary<String, Any>) in
            self.legalConsultationArray = LegalConsultation.mapResponseToArrayObject(response: responseObject)
            self.questionTableView.isHidden = false
            self.questionTableView.reloadData()
            self.indicator.removeFromSuperview()
            print("\(self.legalConsultationArray)")
        }) { (_ error: Int?) in
            self.indicator.removeFromSuperview()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "isNewQestion" {
            let vc = segue.destination as! SendMedicalQuetionVC
            vc.isNewQestion = true
            vc.categoryText = self.category
            
        }
        if segue.identifier == "showMore" {
            let vc = segue.destination as! SendMedicalQuetionVC
            vc.selectedId = self.selectedId
            vc.isNewQestion = false
            vc.categoryText = self.category
        }
    }
    
    @IBAction func sendQuestion (_ sender : UIButton) {
        self.createIndicator()
        self.questionTableView.isHidden = true
        RequsetManager.sharedInstance.checkIsSendQuestion(category: self.category, success: { (response) in
            let respBool = response ["success"] as? Bool ?? false
            if respBool {
                self.indicator.removeFromSuperview()
                self.performSegue(withIdentifier: "isNewQestion", sender: self)
            } else {
                let respText = response ["text"] as? String ?? ""
                self.alert(message: respText)
            }
            self.questionTableView.isHidden = false
            
        }) { (errorCode) in
            self.indicator.removeFromSuperview()
        }
    }
    
    @objc func showMore (_ sender : UIButton) {
        let lc = self.legalConsultationArray[sender.tag]
        self.selectedId = lc.consId
        self.createIndicator()
        self.questionTableView.isHidden = true
        RequsetManager.sharedInstance.checkIsSendQuestion(category: self.category, success: { (response) in
            let respBool = response ["success"] as? Bool ?? false
            if respBool {
                self.indicator.removeFromSuperview()
                self.performSegue(withIdentifier: "showMore", sender: self)
            } else {
                let respText = response ["text"] as? String ?? ""
                self.alert(message: respText)
            }
            self.questionTableView.isHidden = false
            
        }) { (errorCode) in
            self.indicator.removeFromSuperview()
        }
        
//        self.performSegue(withIdentifier: "showMore", sender: self)
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let lc = self.legalConsultationArray[indexPath.row]
        self.selectedId = lc.consId
        self.createIndicator()
        self.questionTableView.isHidden = true
        RequsetManager.sharedInstance.checkIsSendQuestion(category: self.category, success: { (response) in
            let respBool = response ["success"] as? Bool ?? false
            if respBool {
                self.indicator.removeFromSuperview()
                self.performSegue(withIdentifier: "showMore", sender: self)
            } else {
                let respText = response ["text"] as? String ?? ""
                self.alert(message: respText)
            }
            self.questionTableView.isHidden = false
            
        }) { (errorCode) in
            self.indicator.removeFromSuperview()
        }
        
//        self.performSegue(withIdentifier: "showMore", sender: self)
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.legalConsultationArray.count
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return  1000
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "MedicalConsultationC"
        let lc = self.legalConsultationArray[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! MedicalConsultationC
        cell.quetion.text = lc.text
        //25 - image wight
        let attributedString = NSAttributedString(html: lc.answer)
        cell.answerTextView.attributedText = attributedString
        cell.answerTextView.font = UIFont(name: "AvenirNext-Medium", size: 11.0)
        cell.answer.text = cell.answerTextView.text
        cell.answerTextView.isScrollEnabled = false
        
        if lc.isPrivate {
            cell.quetionTittle.text = "Личный вопрос (виден только вам)"
        } else {
            cell.quetionTittle.text = "Загальне запитання (видно всім)"
        }
        
        if lc.isAnswered {
            cell.answer.text = lc.answer
        } else {
            cell.answer.text = "Мед.специалист готовит ответ. Спасибо за ожидание."
            cell.answerTextView.text = "Мед.специалист готовит ответ. Спасибо за ожидание."
        }
        
        cell.moreButton.tag = indexPath.row
        cell.moreButton.addTarget(self,action:#selector(showMore), for: .touchUpInside)
        cell.selectionStyle = .none
        return cell
    }
}

