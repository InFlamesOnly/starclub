//
//  CalendarDetailC.swift
//  StarClub
//
//  Created by Hackintosh on 7/5/19.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit

class CalendarDetailC: UITableViewCell {
    
//    "start_date":"2019-06-30",
//    "start_time":"11:33:00",
//    "end_date":"2019-06-30",
//    "end_time":"18:43:00",
//    "location":"\u0422\u0440\u043e\u044f",
    
    @IBOutlet weak var roundView: UIView!
    @IBOutlet weak var dateTittle: UILabel!
    @IBOutlet weak var dateImageView: UIImageView!
    @IBOutlet weak var dateText: UILabel!
    
    @IBOutlet weak var startDateText: UILabel!
    @IBOutlet weak var startDateConstraint: NSLayoutConstraint!
    @IBOutlet weak var startTimeText: UILabel!
    @IBOutlet weak var startTimeConstraint: NSLayoutConstraint!
    @IBOutlet weak var endDateText: UILabel!
    @IBOutlet weak var endDateConstraint: NSLayoutConstraint!
    @IBOutlet weak var endTimeText: UILabel!
    @IBOutlet weak var endTimeConstraint: NSLayoutConstraint!
    @IBOutlet weak var locationText: UILabel!
    @IBOutlet weak var locationTextConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var feedTextView: UITextView!

    override func awakeFromNib() {
        super.awakeFromNib()
        roundView.layer.cornerRadius = CELLBORDERRADIUS
    }
}
