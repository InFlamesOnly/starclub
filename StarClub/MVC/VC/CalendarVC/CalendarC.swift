//
//  CalendarC.swift
//  StarClub
//
//  Created by Hackintosh on 7/3/19.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit

class CalendarC: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textCalendar: UILabel!
    @IBOutlet weak var calendarTextView: UITextView?
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
