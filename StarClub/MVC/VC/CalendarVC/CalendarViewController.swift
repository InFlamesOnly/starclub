//
//  CalendarViewController.swift
//  StarClub
//
//  Created by Dima on 20.06.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit
import FSCalendar

//https://github.com/patchthecode/JTAppleCalendar/wiki/Tutorials

class CalendarViewController: LightViewController, FSCalendarDelegate, FSCalendarDataSource, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var calendar: FSCalendar!
    @IBOutlet weak var eventTableView: UITableView!
    @IBOutlet weak var mounthLabel: UILabel!
    let indicator = SpringIndicator()
    var calendarDates : Array<CalendarDate> = Array()
    var tableViewDates : Array<CalendarDate> = Array()
    
    var currentMounth = 0
    var currentYear = 0
    
    var datesWithEvent = [String]()
    var selectedEvent : CalendarDate?
    
    fileprivate lazy var dateFormatter2: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.calendar.locale = Locale(identifier: "ru_RU")
        self.calendar.isHidden = true
//        self.calendar.scrollEnabled = false
        self.getListFromServer()
        self.currentMounth = Date().month
        self.currentYear = Date().year
        self.mounthLabel.text = self.mounthToString(mounth: self.currentMounth) + " " + "\(self.currentYear)"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super .viewWillDisappear(animated)
        self.hideBurger()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showBurger()
    }
    
    func hideBurger () {
        if self.navigationController is CustomeNC {
            let nc = self.navigationController as! CustomeNC
            nc.hideBurger()
        }
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func showBurger () {
        if self.navigationController is CustomeNC {
            let nc = self.navigationController as! CustomeNC
            nc.showBurger()
        }
    }
    
    func mounthToString (mounth : Int) -> String {
        switch mounth {
        case 1: return "Январь"
        case 2: return "Февраль"
        case 3: return "Март"
        case 4: return "Апрель"
        case 5: return "Май"
        case 6: return "Июнь"
        case 7: return "Июль"
        case 8: return "Август"
        case 9: return "Сентябрь"
        case 10: return "Октябрь"
        case 11: return "Ноябрь"
        case 12: return "Декабрь"
        default:
            return ""
        }
    }
    
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        self.mounthLabel.text = self.mounthToString(mounth: calendar.currentPage.month) + " " + "\(calendar.currentPage.year)"
    }
    
    func createIndicator () {
        indicator.frame.size = CGSize(width: 100, height: 100)
        indicator.center = self.view.center
        indicator.lineColor = PINK
        self.view.addSubview(indicator)
        indicator.start()
    }
    
    func getListFromServer () {
        self.createIndicator()
        RequsetManager.sharedInstance.getCalendarDates(success: { calendars in
            self.calendarDates = CalendarDate.mapResponseToArrayObject(response: calendars)
            self.indicator.removeFromSuperview()
            self.datesWithEvent = self.getAllDatesFromEvent()
            self.calendar.reloadData()
            self.checkEventFromNow()
            self.calendar.isHidden = false
        }) { error in
            self.calendar.isHidden = false
            self.indicator.removeFromSuperview()
        }
    }
    
    @IBAction func nextMounth (sender : UIButton) {
        self.stepCalendarView(index: 1)
        self.currentMounth = self.currentMounth + 1
        if self.currentMounth == 13 {
            self.currentMounth = 1
            self.currentYear = self.currentYear + 1
        }
        self.mounthLabel.text = self.mounthToString(mounth: self.currentMounth) + " " + "\(self.currentYear)"
    }
    
    @IBAction func previousMounth (sender : UIButton) {
        self.stepCalendarView(index: -1)
        self.currentMounth = self.currentMounth - 1
        if self.currentMounth == 0 {
            self.currentMounth = 12
            self.currentYear = self.currentYear - 1
        }
        self.mounthLabel.text = self.mounthToString(mounth: self.currentMounth) + " " + "\(self.currentYear)"
    }
    
    private func stepCalendarView(index: Int) {
        let previousMonth = Calendar.current.date(byAdding: self.calendar.scope.asCalendarComponent(), value: index, to: calendar.currentPage)
        calendar.setCurrentPage(previousMonth!, animated: true)
    }
    
    func checkEventFromNow () {
        let date = Date()
        let dateWithNeedForrmater = dateFormatter2.string(from: date)
        self.filterArrayFromDate(date: dateWithNeedForrmater)
        if self.tableViewDates.count != 0 {
            self.eventTableView.reloadData()
            self.eventTableView.isHidden = false
        } else {
            self.eventTableView.isHidden = true
        }
        self.eventTableView.reloadData()
    }
    
    func getAllDatesFromEvent () -> [String] {
        var array = [String]()
        for dates in self.calendarDates {
            array.append(dates.startDate)
        }
        return array
    }
    
    func filterArrayFromDate (date : String) {
        var array = [CalendarDate]()
        for dates in self.calendarDates {
            if dates.startDate == date {
                array.append(dates)
            }
        }
        self.tableViewDates = array
    }
    
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
        
        let dateString = self.dateFormatter2.string(from: date)
        
        if self.datesWithEvent.contains(dateString) {
            return 1
        }
        
        return 0
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            let vc = segue.destination as! CalendarDetailVC
            vc.selectedEvent = self.selectedEvent
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedEvent = self.tableViewDates[indexPath.row]
        self.performSegue(withIdentifier: "showDetail", sender: self)
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        let dateWithNeedForrmater = dateFormatter2.string(from: date)
        self.filterArrayFromDate(date: dateWithNeedForrmater)
        if self.tableViewDates.count != 0 {
            self.eventTableView.reloadData()
            self.eventTableView.isHidden = false
        } else {
            self.eventTableView.isHidden = true
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tableViewDates.count
    }
    
    //CalendarC
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CalendarC") as! CalendarC
        let event = self.tableViewDates[indexPath.row]
        cell.titleLabel.text = event.title
        cell.textCalendar.text = event.shortContent
        
//        cell.textCalendar.text = event.shortContent
        let attributedString = NSAttributedString(html: event.shortContent)
        cell.calendarTextView?.attributedText = attributedString
        cell.calendarTextView?.font = UIFont(name: "AvenirNext-Medium", size: 15.0)
        cell.textCalendar.text = cell.calendarTextView?.text
        cell.calendarTextView?.isScrollEnabled = false
        cell.calendarTextView?.isUserInteractionEnabled = false
        
//        let attributedString = NSAttributedString(html: article.content)
//        cell.feedTextView.attributedText = attributedString
//        cell.feedTextView.font = UIFont(name: "AvenirNext-Medium", size: 15.0)
//        cell.feedTextView.isScrollEnabled = false
        
        cell.selectionStyle = .none
        return cell
    }
}

extension Date{
    var day:Int {return Calendar.current.component(.day, from:self)}
    var month:Int {return Calendar.current.component(.month, from:self)}
    var year:Int {return Calendar.current.component(.year, from:self)}
}

extension FSCalendarScope {
    func asCalendarComponent() -> Calendar.Component {
        switch (self) {
        case .month: return .month
        case .week: return .weekOfYear
        }
    }
}
