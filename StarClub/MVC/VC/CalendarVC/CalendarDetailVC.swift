//
//  CalendarDetailVC.swift
//  StarClub
//
//  Created by Hackintosh on 7/5/19.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit
import EventKit

class CalendarDetailVC: LightViewController, UITableViewDelegate, UITableViewDataSource {
    
    var selectedEvent : CalendarDate?
    let eventStore : EKEventStore = EKEventStore()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    func addDate () {
        eventStore.requestAccess(to: .event) { (granted, error) in
            if (granted) && (error == nil) {
                
                let event:EKEvent = EKEvent(eventStore: self.eventStore)
                
                event.title = self.selectedEvent?.title
                event.startDate = self.stringToDate(text: self.selectedEvent!.startDate)
                event.endDate = self.stringToDate(text: self.selectedEvent!.endDate)
                event.notes = self.selectedEvent?.shortContent
                event.calendar = self.eventStore.defaultCalendarForNewEvents
                do {
                    try self.eventStore.save(event, span: .thisEvent)
                } catch let error as NSError {
                    print("failed to save event with error : \(error)")
                }
                self.allert()
            }
        }
    }
    
    func allert () {
        let alert = UIAlertController(title: "", message: "Дата додана у календар", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func stringToDate (text : String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.locale =  NSLocale(localeIdentifier: "ru_RU") as Locale
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: text)
        return date ?? Date()
    }
    
    func convertDateFormater(_ date: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale =  NSLocale(localeIdentifier: "ru_RU") as Locale
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "EEEE, MMM d, yyyy"
        return  dateFormatter.string(from: date!)
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    @objc
    func tapFunction(sender:UITapGestureRecognizer) {
        self.addDate()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CalendarDetailC", for: indexPath) as! CalendarDetailC
        cell.dateTittle.text = self.selectedEvent?.title
        cell.dateText.text = self.selectedEvent?.fullContent
        let attributedString = NSAttributedString(html: self.selectedEvent?.fullContent ?? "")
        cell.feedTextView.attributedText = attributedString
        cell.feedTextView.font = UIFont(name: "AvenirNext-Medium", size: 15.0)
        cell.dateText.text = cell.feedTextView.text
        cell.feedTextView.isScrollEnabled = false
        cell.dateImageView.af_setImage(withURL: URL(string: selectedEvent!.image)!, placeholderImage: nil)
        let font = UIFont(name: "AvenirNext-Medium", size: 17.0)
        let height = CGFloat().heightForView(text: cell.dateTittle.text!, font: font!, width: cell.roundView.frame.size.width - 34)
        cell.heightConstraint.constant = height
        cell.dateImageView.contentMode = .scaleAspectFit
        
        cell.startDateText.text = "Дата начала : \(self.convertDateFormater(self.selectedEvent!.startDate))"
        let height1 = CGFloat().heightForView(text: self.selectedEvent!.startDate, font: font!, width: cell.roundView.frame.size.width - 34) + 40
        cell.startDateConstraint.constant = height1
        
        cell.startTimeText.text = "Время начала : \(self.selectedEvent!.startTime)"
        let height2 = CGFloat().heightForView(text: self.selectedEvent!.startTime, font: font!, width: cell.roundView.frame.size.width - 34)
        cell.startTimeConstraint.constant = height2
        
        cell.endDateText.text = "Дата конца : \(self.convertDateFormater(self.selectedEvent!.endDate))"
        let height3 = CGFloat().heightForView(text: self.selectedEvent!.endDate, font: font!, width: cell.roundView.frame.size.width - 34)  + 40
        cell.endDateConstraint.constant = height3
        
        cell.endTimeText.text = "Время конца : \(self.selectedEvent!.endTime)"
        let height4 = CGFloat().heightForView(text: self.selectedEvent!.endTime, font: font!, width: cell.roundView.frame.size.width - 34)
        cell.endTimeConstraint.constant = height4
        
        cell.locationText.text = "Место : \(self.selectedEvent?.location ?? "")"
        let height5 = CGFloat().heightForView(text: self.selectedEvent!.location, font: font!, width: cell.roundView.frame.size.width - 34)
        cell.locationTextConstraint.constant = height5
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(CalendarDetailVC.tapFunction))
        cell.startDateText.isUserInteractionEnabled = true
        cell.startDateText.addGestureRecognizer(tap)
        
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(CalendarDetailVC.tapFunction))
        cell.endDateText.isUserInteractionEnabled = true
        cell.endDateText.addGestureRecognizer(tap2)
        
        let tap3 = UITapGestureRecognizer(target: self, action: #selector(CalendarDetailVC.tapFunction))
        cell.startTimeText.isUserInteractionEnabled = true
        cell.startTimeText.addGestureRecognizer(tap3)
        
        let tap4 = UITapGestureRecognizer(target: self, action: #selector(CalendarDetailVC.tapFunction))
        cell.endTimeText.isUserInteractionEnabled = true
        cell.endTimeText.addGestureRecognizer(tap4)
        
        cell.selectionStyle = .none
        return cell
    }
}
