//
//  CalculatorVC.swift
//  StarClub
//
//  Created by macOS on 05.05.18.
//  Copyright © 2018 macOS. All rights reserved.
//

import UIKit

class CalculatorVC: LightViewController {
    
    @IBOutlet weak var weight: UITextField!
    @IBOutlet weak var hight: UITextField!
    
    @IBOutlet weak var vcTittle: UILabel!
    @IBOutlet weak var resultValue: UILabel!
    @IBOutlet weak var resultText: UILabel!
    @IBOutlet weak var resultTittle: UILabel!
    @IBOutlet weak var firstTextFieldWidthConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let calc = CalculateState.sharedInstance
        if calc.isFirstCalculate {
            self.vcTittle.text = "Індекс маси тіла"
        } else if calc.isSecondCalculate {
            self.vcTittle.text = "Коэффициент атерогенности"
            self.weight.placeholder = "Общий холестерин"
            self.hight.placeholder = "ЛПВП"
        } else if calc.isThirdCalculate {
            self.weight.placeholder = "Тривалість інтервалу (сек)"
            self.vcTittle.text = "Частота серцевих скорочень"
            self.firstTextFieldWidthConstraint.setMultiplier(multiplier: 0.9)
            self.hight.isHidden = true
        } else if calc.isFourCalculate {
            self.vcTittle.text = "Ризик ССЗ за шкалою SCORE"
        }
        self.hideKeyboardWhenTappedAround()
        self.resultValue.isHidden = true
        self.resultText.isHidden = true
        self.resultTittle.isHidden = true
    }
    
    @IBAction func calculate(_ sender: Any) {
        if self.weight.text?.count == 0 && self.hight.text?.count == 0 {
            self.alert(message: "Заповніть всі поля")
            return
        }
        
        if self.weight.text!.contains(".") {
            self.weight.text! = self.weight.text!.replacingOccurrences(of: ".", with: ",", options: .literal, range: nil)
        }
        
        if self.hight.text!.contains(".") {
            self.hight.text! = self.hight.text!.replacingOccurrences(of: ".", with: ",", options: .literal, range: nil)
        }
        
        if self.weight.text?.count == 0 {
            self.alert(message: "Вкажіть масу тіла")
            return
        }
        if self.hight.text?.count == 0 {
            self.alert(message: "Вкажіть ваш зріст")
            return
        }
        
        let numberFormatter = NumberFormatter()
        let weight = numberFormatter.number(from: self.weight.text!)
        let height = numberFormatter.number(from: self.hight.text!)
        
        if weight!.floatValue < 20 || weight!.floatValue > 300 {
            self.alert(message: "Вкажіть правильну масу тіла")
            return
        }
        
        if height!.floatValue < 50 || height!.floatValue > 300 {
            self.alert(message: "Вкажіть правильний зріст")
            return
        }
        
        self.resultValue.isHidden = false
        self.resultText.isHidden = false
        self.resultTittle.isHidden = false
        self.resultValue.text = String(Int(self.calculateRezult()))
        self.resultText.text = self.textFromRezult(result: self.calculateRezult())
        self.view.endEditing(true)
    }
    
    func calculateRezult ()  -> Float {
        view.endEditing(true)
        let numberFormatter = NumberFormatter()
        let weight = numberFormatter.number(from: self.weight.text!)
        let height = numberFormatter.number(from: self.hight.text!)
        
        let floatWeight = weight?.floatValue
        let floatHeight = height?.floatValue
        
        let a = floatHeight!/100 * floatHeight!/100
        let b = floatWeight!/a
    
        return b
    }
    
    func textFromRezult (result : Float)  -> String {
        var value = ""
        if result < 16 {
            value = "Виражений дефіцит маси тіла"
    
        }
        else if result > 16 && result < 18.5 {
            value = "Недостатня (дефіцит) маса тіла"
        }
        else if result > 18.5 && result < 24.99 {
            value = "Норма"
        }
        else if result > 25 && result < 30 {
            value = "Надлишкова маса тіла (предожіреніе)"
        }
        else if result > 30 && result < 35 {
            value = "Ожиріння"
        }
        else if result > 35 && result < 40 {
            value = "Ожиріння різке"
        }
        else if result > 40 {
            value = "Дуже різке ожиріння"
        }
        return value
    }
}

extension Decimal {
    var floatValue:Float {
        return NSDecimalNumber(decimal:self).floatValue
    }
}

extension NSLayoutConstraint {
    
    func setMultiplier(multiplier:CGFloat) -> NSLayoutConstraint {
        
        NSLayoutConstraint.deactivate([self])
        
        let newConstraint = NSLayoutConstraint(
            item: firstItem!,
            attribute: firstAttribute,
            relatedBy: relation,
            toItem: secondItem,
            attribute: secondAttribute,
            multiplier: multiplier,
            constant: constant)
        
        newConstraint.priority = priority
        newConstraint.shouldBeArchived = shouldBeArchived
        newConstraint.identifier = identifier
        
        NSLayoutConstraint.activate([newConstraint])
        return newConstraint
    }
}
