//
//  CheckCompanyTVC.swift
//  StarClub
//
//  Created by macOS on 12.12.17.
//  Copyright © 2017 macOS. All rights reserved.
//

import UIKit
import KYDrawerController

var companyNames = ["StarClub","Ендокринологія"];

class CheckCompanyTVC: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let isGastro = UserDefaults.standard.bool(forKey: "isGastro")
        let isCardio = UserDefaults.standard.bool(forKey: "isCardio")
        if isGastro == true && isCardio == false {
            companyNames = ["Gastro Star Club", "Гастроенторологія"];
            UserDefaults.standard.set(false, forKey: "isGastro")
            UserDefaults.standard.set(false, forKey: "isCardio")
        }
        
        if isGastro == false && isCardio == true {
            companyNames = ["Cardio Star Club", "Кардиологи", "ОЗІРКА"];
            UserDefaults.standard.set(false, forKey: "isGastro")
            UserDefaults.standard.set(false, forKey: "isCardio")
        }
        
        self.tableView.isScrollEnabled = false;
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        self.performSegue(withIdentifier: "showLoginVC", sender: self)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return companyNames.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell : CheckPharmaCell = tableView.dequeueReusableCell(withIdentifier: "CheckPharmaCell", for: indexPath) as! CheckPharmaCell
            cell.name?.text = companyNames[indexPath.row]
            cell.selectionStyle = .none
            return cell
        }
}
