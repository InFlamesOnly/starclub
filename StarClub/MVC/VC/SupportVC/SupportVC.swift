//
//  SupportVC.swift
//  StarClub
//
//  Created by macOS on 19.12.17.
//  Copyright © 2017 macOS. All rights reserved.
//

import UIKit

class SupportVC: LightViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var supportTableView : UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        supportTableView.isHidden = true
    }
    
    @objc func callNumber() {
        if let url = URL(string: "tel://\(SUPPORTPHONENUMBER)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    // MARK: - Table view data source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 194
            
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cellIdentifier = "SupportC"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! SupportC
            cell.telephoneButton.tag = indexPath.row
            cell.telephoneButton.addTarget(self,action:#selector(callNumber), for: .touchUpInside)
            cell.selectionStyle = .none
            return cell
        } else {
            let cellIdentifier = "QuetionC"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! QuetionC
            cell.selectionStyle = .none
            return cell
        }
    }
    
}
