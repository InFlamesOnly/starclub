//
//  SupportC.swift
//  StarClub
//
//  Created by macOS on 21.12.17.
//  Copyright © 2017 macOS. All rights reserved.
//

import UIKit

class SupportC: UITableViewCell {
    
    @IBOutlet weak var telephoneButton : UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        telephoneButton.layer.cornerRadius = telephoneButton.frame.size.height / 2
    }
}
