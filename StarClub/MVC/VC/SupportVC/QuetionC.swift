//
//  QuetionC.swift
//  StarClub
//
//  Created by macOS on 21.12.17.
//  Copyright © 2017 macOS. All rights reserved.
//

import UIKit

class QuetionC: UITableViewCell {
    
    @IBOutlet weak var qutionTittle : UILabel!
    @IBOutlet weak var qutionAnswer : UILabel!
    @IBOutlet weak var roundBackgroundView : UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        roundBackgroundView.layer.cornerRadius = CELLBORDERRADIUS
    }
}
