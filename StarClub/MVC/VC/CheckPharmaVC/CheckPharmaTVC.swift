//
//  CheckPharmaTVC.swift
//  StarClub
//
//  Created by macOS on 12.12.17.
//  Copyright © 2017 macOS. All rights reserved.
//

import UIKit

let pharmaNames = ["Ендокринологія","Гастроенторологія","Кардіологія","Педіатрія","Сімейні лікарі"];

class CheckPharmaTVC: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.isScrollEnabled = false;
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        if indexPath.row == 0 {
//            let controller = storyboard.instantiateViewController(withIdentifier: "CheckCompanyVC") as! CheckCompanyVC
//            self.navigationController?.pushViewController(controller, animated: true)
//        } else if indexPath.row == 1 {
//            let controller = storyboard.instantiateViewController(withIdentifier: "CheckCompanyVC") as! CheckCompanyVC
//            UserDefaults.standard.set(true, forKey: "isGastro")
//            UserDefaults.standard.set(false, forKey: "isCardio") //Bool
//            self.navigationController?.pushViewController(controller, animated: true)
//        } else if indexPath.row == 2 {
//            let controller = storyboard.instantiateViewController(withIdentifier: "CheckCompanyVC") as! CheckCompanyVC
//            UserDefaults.standard.set(false, forKey: "isGastro")
//            UserDefaults.standard.set(true, forKey: "isCardio") //Bool
//            self.navigationController?.pushViewController(controller, animated: true)
//        }
//        else {
            self.performSegue(withIdentifier: "showLoginVC", sender: self)
//        }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pharmaNames.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : CheckPharmaCell = tableView.dequeueReusableCell(withIdentifier: "CheckPharmaCell", for: indexPath) as! CheckPharmaCell
        cell.name?.text = pharmaNames[indexPath.row]
        cell.selectionStyle = .none
        return cell
    }

}
