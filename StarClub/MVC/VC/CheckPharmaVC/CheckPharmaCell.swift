//
//  CheckPharmaCell.swift
//  StarClub
//
//  Created by macOS on 12.12.17.
//  Copyright © 2017 macOS. All rights reserved.
//

import UIKit

class CheckPharmaCell: UITableViewCell {
    
    @IBOutlet weak var bckView: UIView!
    @IBOutlet weak var name: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        bckView?.layer.cornerRadius = 25
        bckView?.clipsToBounds = true
        bckView?.layer.borderWidth = 1.0
        bckView?.layer.borderColor = LIGHTGREY.cgColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
