//
//  PatientListVC.swift
//  StarClub
//
//  Created by macOS on 11.03.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit

class PatientListVC: LightViewController {
    
    @IBOutlet weak var patientsTableView: UITableView!
    @IBOutlet weak var errorLabel: UILabel!
    
    var patientsArray : Array<Patient> = Array()
    let indicator = SpringIndicator()
    
    let user = User().currentUser()
    
    var patient : Patient?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.patientsTableView.isHidden = true
        self.errorLabel.isHidden = true
        self.getPatients()
    }
    
    func createIndicator () {
        indicator.frame.size = CGSize(width: 100, height: 100)
        indicator.center = self.view.center
        indicator.lineColor = PINK
        self.view.addSubview(indicator)
        indicator.start()
    }
    
    func getPatients () {
        self.createIndicator()
        RequsetManager.sharedInstance.getPatientList(success: { (patientList) in
            self.patientsTableView.isHidden = false
            self.patientsArray = Patient.mapResponseToArrayObject(response: patientList)
            if self.patientsArray.count == 0 {
                self.patientsTableView.isHidden = true
                self.errorLabel.isHidden = false
            }
            self.patientsTableView.reloadData()
            self.indicator.removeFromSuperview()
        }) { (errorCode) in
            self.patientsTableView.isHidden = false
            self.indicator.removeFromSuperview()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showVisits" {
            let vc = segue.destination as! VisitsVC
            vc.patient = self.patient!
        }
    }

}



extension PatientListVC : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.patient = self.patientsArray[indexPath.row]
        self.performSegue(withIdentifier: "showVisits", sender: self)
    }
}

extension PatientListVC : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.patientsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PatientCell", for: indexPath) as! PatientCell
        let patient = self.patientsArray[indexPath.row]
        //cell.visitNumberLabel.text
        cell.visitNumberLabel.text = "Пациент № \(patient.patientId)"
        cell.nameLabel.text = patient.name
        cell.ageLabel.text = patient.age
        //cell.visitCountLabel.text
        cell.visitCountLabel.text = "Визитов: \(patient.count)"
        cell.selectionStyle = .none
        return cell
    }
}
