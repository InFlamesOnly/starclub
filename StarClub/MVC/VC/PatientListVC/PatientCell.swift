//
//  PatientCell.swift
//  StarClub
//
//  Created by macOS on 11.03.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit

class PatientCell: UITableViewCell {
    
    @IBOutlet weak var roundView: UIView!
    @IBOutlet weak var visitNumberLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var visitCountLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        roundView.layer.cornerRadius = CELLBORDERRADIUS
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
