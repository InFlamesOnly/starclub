//
//  ArticlesDetailVC.swift
//  StarClub
//
//  Created by macOS on 06.02.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit

class ArticlesDetailVC: LightViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var articlesTableView: UITableView!
    var articleArray : Array<Article> = Array()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let user = User().currentUser()
        let vc = CustomeNC()
        //TODO
//        if user.specific != 7 {
//            let roundView = vc.createRoundView()
//            self.navigationItem.rightBarButtonItem = roundView
//        }
            self.navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articleArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "ArticlesC"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! ArticlesC
        let article = self.articleArray[indexPath.row]
        cell.articleTittle.text = article.title
        cell.articleText.text = article.content
        let attributedString = NSAttributedString(html: article.content)
        cell.feedTextView.attributedText = attributedString
        cell.feedTextView.font = UIFont(name: "AvenirNext-Medium", size: 15.0)
        cell.articleText.text = cell.feedTextView.text
        cell.feedTextView.isScrollEnabled = false
        cell.articleImageView.af_setImage(withURL: URL(string: article.image)!, placeholderImage: nil)
        let font = UIFont(name: "AvenirNext-Medium", size: 17.0)
        let height = CGFloat().heightForView(text: cell.articleTittle.text!, font: font!, width: cell.roundView.frame.size.width - 34)
        cell.heightConstraint.constant = height
        cell.articleImageView.contentMode = .scaleAspectFit
        cell.selectionStyle = .none
        return cell
    }

}
