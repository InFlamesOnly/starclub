//
//  TreatmentProtocolsVC.swift
//  StarClub
//
//  Created by macOS on 6/29/20.
//  Copyright © 2020 macOS. All rights reserved.
//

import UIKit

class TreatmentProtocolsVC: UIViewController, UIWebViewDelegate {
    
    @IBOutlet weak var webView : UIWebView!
    let indicator = SpringIndicator()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.createIndicator()
        self.webView.loadRequest(URLRequest(url: URL(string: "http://kusumdoctors.co.ua/protocols?utm_source=KD_protocol&utm_medium=open_first_page&utm_campaign=\(User().currentUser().specific)&token=\(User().currentUser().token)")!))
        self.webView.delegate = self
        self.webView.isHidden = true
        self.webView.scalesPageToFit = true
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.indicator.removeFromSuperview()
        self.webView.isHidden = false
    }
    
    func createIndicator () {
        indicator.frame.size = CGSize(width: 100, height: 100)
        indicator.center = self.view.center
        indicator.lineColor = PINK
        self.view.addSubview(indicator)
        indicator.start()
    }

}
