//
//  VisitVC.swift
//  StarClub
//
//  Created by macOS on 11.03.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit

class VisitVC: LightViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
}

extension VisitVC : UITableViewDelegate {
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        self.performSegue(withIdentifier: "showVisit", sender: self)
//    }
}

extension VisitVC : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VisitCell", for: indexPath) as! VisitCell
        cell.selectionStyle = .none
        return cell
    }
}
