//
//  VisitCell.swift
//  StarClub
//
//  Created by macOS on 11.03.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit

class VisitCell: UITableViewCell {
    
    @IBOutlet weak var roundView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var genderAndDateLabel: UILabel!
    @IBOutlet weak var visitsDateLabel: UILabel!
    @IBOutlet weak var visitNumberLabel: UILabel!
//    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var heightLabel: UILabel!
    @IBOutlet weak var IMVLabel: UILabel!
    
    @IBOutlet weak var CATLabel: UILabel!
    @IBOutlet weak var DATLabel: UILabel!
    
    @IBOutlet weak var glucoseLabel: UILabel!
    
    @IBOutlet weak var HSAllLabel: UILabel!
    @IBOutlet weak var HSLPNPLabel: UILabel!
    
    @IBOutlet weak var triglyceridesLabel: UILabel!
    
    @IBOutlet weak var mainDiagnosisLabel: UILabel!
    @IBOutlet weak var concomitantDiagnosisLabel: UILabel!
    
    @IBOutlet weak var treatmentDiagnosisLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        roundView.layer.cornerRadius = CELLBORDERRADIUS
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
