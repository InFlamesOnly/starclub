//
//  SendMedicalQuetionVC.swift
//  StarClub
//
//  Created by Dima on 23.04.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit

class SendMedicalQuetionVC: LightViewController, UITableViewDelegate, UITableViewDataSource, UITextViewDelegate {
    
    @IBOutlet weak var questionTableView: UITableView!
    var isNewQestion = false
    var isMyQuestion = false
    var selectedId = 0
    let indicator = SpringIndicator()
    var textViewText = ""
    
    var categoryText = ""
    
    var legalConsultationArray : Array<LegalConsultation> = Array()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        self.questionTableView.isHidden = true
        
        if isNewQestion {
            self.questionTableView.isHidden = false
            print("isNew")
        } else {
            self.getListFromServer()
            print("isNotNew")
        }
        
        let user = User().currentUser()
        let vc = CustomeNC()
        if user.specific != 7 {
            //TODO
            //            let roundView = vc.createRoundView()
            //            self.navigationItem.rightBarButtonItem = roundView
        }
        self.navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.selectedId = 0
    }
    
    func createIndicator () {
        indicator.frame.size = CGSize(width: 100, height: 100)
        indicator.center = self.view.center
        indicator.lineColor = PINK
        self.view.addSubview(indicator)
        indicator.start()
    }
    
    func getListFromServer () {
        self.createIndicator()
    RequsetManager.sharedInstance.getMedConsultationFromId(lcId:String(self.selectedId), success: { (_ responseObject: Dictionary<String, Any>) in
            self.legalConsultationArray = LegalConsultation.mapResponseToArrayObject(response: responseObject)
            let lc = self.legalConsultationArray[0]
            self.isMyQuestion = lc.isMyQuestion
            self.questionTableView.isHidden = false
            self.questionTableView.reloadData()
            self.indicator.removeFromSuperview()
        
            print("\(self.legalConsultationArray)")
        }) { (_ error: Int?) in
            self.indicator.removeFromSuperview()
        }
    }
    
    @IBAction func send(_ sender: Any) {
        self.createIndicator()
        var indexPath = IndexPath()
        if legalConsultationArray.count == 0 {
            indexPath = IndexPath(row: 0, section: 0)
        } else {
            let indexPathRow = self.questionTableView.numberOfRows(inSection: self.questionTableView.numberOfSections - 1)
            indexPath = IndexPath(row: indexPathRow - 1, section: 0)
        }
        
        let cell: SendMedicalQuetionTextC = self.questionTableView.cellForRow(at: indexPath) as! SendMedicalQuetionTextC
        cell.sendButton.isHidden = true
        RequsetManager.sharedInstance.sendMedQestion(category:self.categoryText, qestionId: self.selectedId, text:self.textViewText , success: { (_ responseObject: Dictionary<String, Any>) in
            cell.sendButton.isHidden = false
            self.alertWithAction(message: "Спасибо за Ваше обращение. Срок ответа – 3 рабочих дня!", title: "")
            self.indicator.removeFromSuperview()
        }) { (_ error: Int?) in
            cell.sendButton.isHidden = false
            self.indicator.removeFromSuperview()
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if (textView.text == "Я ввожу текст вопроса…") {
            textView.text = ""
            //optional
        }
        textView.becomeFirstResponder()
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        self.textViewText = textView.text
        if (textView.text == "") {
            textView.text = "Я ввожу текст вопроса…"
        }
        textView.resignFirstResponder()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if legalConsultationArray.count == 0 {
            return 1
        } else if self.isMyQuestion {
            return legalConsultationArray.count  + 1
        } else if !self.isMyQuestion {
            return legalConsultationArray.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return  1000
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "SendMedicalQuetionC"
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! SendMedicalQuetionC
        if indexPath.row == legalConsultationArray.count {
            let cellIdentifier = "SendMedicalQuetionTextC"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! SendMedicalQuetionTextC
            //            cell.sendQuestionTittle.text = "Введите ваш вопрос"
            if self.isNewQestion {
                _ = Timer.scheduledTimer(withTimeInterval: 0.7, repeats: false) { (timer) in
                    cell.sendText.becomeFirstResponder()
                }
            }
            else {
                //                if !self.isMyQuestion {
                //                    cell.sendButton.isEnabled = false
                //                } else {
                //                    cell.sendButton.isEnabled = true
                //                }
            }
            cell.selectionStyle = .none
            return cell
        }
        
        //        if !isNewQestion {
        //            cell.sendQuestionTittle.text = "Личный вопрос"
        //        }
        
        
        let lc = self.legalConsultationArray[indexPath.row]
        
        
        if lc.isPrivate {
            cell.sendQuestionTittle.text = "Личный вопрос (виден только вам)"
        } else {
            cell.sendQuestionTittle.text = "Загальне запитання (видно всім)"
        }
        
        cell.quetion.text = lc.text
        let attributedString = NSAttributedString(html: lc.answer)
        cell.answerTextView.attributedText = attributedString
        cell.answerTextView.font = UIFont(name: "AvenirNext-Medium", size: 11.0)
        cell.answer.text = cell.answerTextView.text
        cell.answerTextView.isScrollEnabled = false
        
        if lc.isAnswered {
            cell.answer.text = lc.answer
        } else {
            cell.answer.text = "Мед.специалист готовит ответ. Спасибо за ожидание."
            cell.answerTextView.text = "Мед.специалист готовит ответ. Спасибо за ожидание."
        }
        
        cell.selectionStyle = .none
        return cell
    }
}
