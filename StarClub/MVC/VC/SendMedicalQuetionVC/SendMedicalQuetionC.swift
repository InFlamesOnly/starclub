//
//  SendMedicalQuetionC.swift
//  StarClub
//
//  Created by Dima on 23.04.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit

class SendMedicalQuetionC: UITableViewCell {
    
    @IBOutlet weak var sendQuestionTittle: UILabel!
    @IBOutlet weak var quetion: UILabel!
    @IBOutlet weak var answer: UILabel!
    @IBOutlet weak var roundView: UIView!
    @IBOutlet weak var answerTextView: UITextView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        roundView.layer.cornerRadius = CELLBORDERRADIUS
    }
}
