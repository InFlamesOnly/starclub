//
//  SendMedicalQuetionTextC.swift
//  StarClub
//
//  Created by Dima on 23.04.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit

class SendMedicalQuetionTextC: UITableViewCell {
    
    @IBOutlet weak var sendQuestionTittle: UILabel!
    @IBOutlet weak var roundView: UIView!
    @IBOutlet weak var sendText: UITextView!
    @IBOutlet weak var sendButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        roundView.layer.cornerRadius = CELLBORDERRADIUS
        self.customisationButton()
    }
    
    func customisationButton () {
        sendButton.layer.borderColor = PINK.cgColor
        sendButton.layer.cornerRadius = 10
        sendButton.layer.borderWidth = 1
        sendButton.layer.cornerRadius = sendButton.frame.size.height/2
    }
}
