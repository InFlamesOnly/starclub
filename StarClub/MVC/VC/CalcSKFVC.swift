//
//  CalcSKFVC.swift
//  StarClub
//
//  Created by Dima on 12.04.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit

class CalcSKFVC: LightViewController {
    
    @IBOutlet weak var genderDropDown : DropDown!
    @IBOutlet weak var ageTextField : UITextField!
    @IBOutlet weak var weightTextField : UITextField!
    @IBOutlet weak var creatinTextField : UITextField!
    @IBOutlet weak var mochTextField : UITextField!
    @IBOutlet weak var albTextField : UITextField!
    
    @IBOutlet weak var rezultLabel : UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        self.rezultLabel.isHidden = true
        
        self.genderDropDown.optionArray = ["Чоловіча", "Жіноча"]
        
        self.genderDropDown.didSelect{(selectedText , index ,id) in
            self.genderDropDown.placeholder = selectedText
            self.genderDropDown.textAlignment = .center
        }
    }
    
    @IBAction func calculate(_ sender: Any) {
        var gender = 0
        if  genderDropDown.placeholder == "Жіноча" || genderDropDown.text == "Жіноча" {
            gender = 1
        }
        
        if self.ageTextField.text?.count == 0 && self.weightTextField.text?.count == 0 && self.creatinTextField.text?.count == 0 && self.mochTextField.text?.count == 0 && self.albTextField.text?.count == 0 {
            self.alert(message: "Заповніть усі поля")
            return
        }
        
        if self.ageTextField.text?.count == 0 {
            self.alert(message: "Вкажіть ваш вік")
            return
        }
        
        if self.weightTextField.text?.count == 0 {
            self.alert(message: "Вкажіть вашу вагу")
            return
        }
        
        if self.creatinTextField.text?.count == 0 {
            self.alert(message: "Вкажіть креатинін")
            return
        }
        
        if self.mochTextField.text?.count == 0 {
            self.alert(message: "Вкажіть сечовину")
            return
        }
        
        if self.albTextField.text?.count == 0 {
            self.alert(message: "Вкажіть альбумін")
            return
        }

        self.rezultLabel.isHidden = false
        self.rezultLabel.text = self.calculate(gender: gender, age: Int(self.ageTextField.text!)!, weight: Double(self.weightTextField.text!)!, creatin: Double(self.creatinTextField.text!)!, moch: Double(self.mochTextField.text!)!, alb: Double(self.albTextField.text!)!)
    }

    func calculate (gender : Int, age : Int, weight : Double, creatin : Double, moch : Double, alb : Double) -> String {
        var returnedString = ""
        if gender == 0 {//m
            let res = (Double((140 - age)) * weight * 1.2) / creatin
            let roundRezClCr = "\(Int(res) )" + " " + "мл/хв" //res Клиренс креатин
            returnedString = "КК (формула Кокрофта - Голта) : " + roundRezClCr + "\n"
            let a = creatin * 0.0113
            let b = moch * 6
            let c = alb * 0.1

            let rezMDR = 170 * pow(a, -0.999) * pow(Double(age), -0.176) * pow(b, -0.170) * pow(c, 0.318)
            let roundRezMDR = "\(Int(rezMDR) )" + " " + "мл/хв/1.73 м²"
            returnedString = returnedString + "Швидкість клубочкової фільтрації (MDRD) : " + roundRezMDR + "\n"
            
            if creatin <= 80 {
                let d = creatin * 0.0113/0.9
                let resCKD = Double(141) * pow(0.993, Double(age)) * pow(d, -0.412)
                let roundRezCKD = "\(Int(resCKD) )" + " " + "мл/хв/1.73 м²"
                returnedString = returnedString + "Швидкість клубочкової фільтрації (CKD-EPI) :" + roundRezCKD
            } else {
                let d = creatin * 0.0113/0.9
                let resCKD = Double(141) * pow(0.993, Double(age)) * pow(d, -1.21)
                let roundRezCKD = "\(Int(resCKD) )" + " " + "мл/хв/1.73 м²"
                returnedString = returnedString + "Швидкість клубочкової фільтрації (CKD-EPI) : " + roundRezCKD
            }
        } else {
            let res = (Double((140 - age)) * weight * 1.02) / creatin
            let roundRezClCr = "\(Int(res) )" + " " + "мл/хв" //res Клиренс креатин
            returnedString = "КК (формула Кокрофта - Голта) : " + roundRezClCr + "\n"
            let a = creatin * 0.0113
            let b = moch * 6
            let c = alb * 0.1
            
            let rezMDR = 170 * pow(a, -0.999) * pow(Double(age), -0.176) * pow(b, -0.170) * pow(c, 0.318) * 0.762
            let roundRezMDR = "\(Int(rezMDR) )" + " " + "мл/хв/1.73 м²"
            returnedString = returnedString + "Швидкість клубочкової фільтрації (MDRD) : " + roundRezMDR + "\n"
            if creatin <= 62 {
                let d = creatin * 0.0113/0.7
                let resCKD = Double(144) * pow(0.993, Double(age)) * pow(d, -0.328)
                let roundRezCKD = "\(Int(resCKD) )" + " " + "мл/хв/1.73 м²"
                returnedString = returnedString + "Швидкість клубочкової фільтрації (CKD-EPI) : " + roundRezCKD
            } else {
                let d = creatin * 0.0113/0.7
                let resCKD = Double(144) * pow(0.993, Double(age)) * pow(d, -1.21)
                let roundRezCKD = "\(Int(resCKD) )" + " " + "мл/хв/1.73 м²"
                returnedString = returnedString + "Швидкість клубочкової фільтрації (CKD-EPI) : " + roundRezCKD
            }
        }
        return returnedString
    }
}
