//
//  CustomeNC.swift
//  StarClub
//
//  Created by macOS on 12.12.17.
//  Copyright © 2017 macOS. All rights reserved.
//

import UIKit
import KYDrawerController

protocol CustomeNCDelegate : class {
    func update()
    func back ()
}

class CustomeNC: UINavigationController {
    weak var ncDelegate: CustomeNCDelegate?
    var contLabel : UILabel!
    var notificationCount = User().currentUser().achievement
    var titleText = startNavigationText//main VC loaded with this name
    let user = User().currentUser()
    
    
    let burgerButton = UIButton()
    let updateButton = UIButton()
    let backButton = UIButton()
//    let helperButton = UIButton()
    
    var achivmentItem = UIBarButtonItem()
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.showAchivment()
//        self.hideAchivment()
////        self.hideOrShowAchivments()
//        self.view.addSubview(navigationBar)
        self.crateBurgerButton()
        self.crateUpdateButton()
        self.crateBackButton()
//        self.crateHelperButton()
        self.addTittleToNavigationBar()
        self.navigationBar.barTintColor = PINK
        self.updateButton.isHidden = true
        self.backButton.isHidden = self.updateButton.isHidden
        
        if #available(iOS 15, *) {
            // MARK: Navigation bar appearance
            let navigationBarAppearance = UINavigationBarAppearance()
            navigationBarAppearance.configureWithOpaqueBackground()
            navigationBarAppearance.titleTextAttributes = [
                NSAttributedString.Key.foregroundColor : UIColor.white
            ]
            navigationBarAppearance.backgroundColor = PINK
            UINavigationBar.appearance().standardAppearance = navigationBarAppearance
            UINavigationBar.appearance().compactAppearance = navigationBarAppearance
            UINavigationBar.appearance().scrollEdgeAppearance = navigationBarAppearance
            
            // MARK: Tab bar appearance
            let tabBarAppearance = UITabBarAppearance()
            tabBarAppearance.configureWithOpaqueBackground()
            tabBarAppearance.backgroundColor = UIColor.blue
            UITabBar.appearance().scrollEdgeAppearance = tabBarAppearance
            UITabBar.appearance().standardAppearance = tabBarAppearance
        }

//        self.helperButton.isHidden = true
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
//            imageView.image = UIImage(named: const2)
        } else {
            print("Portrait")
//            imageView.image = UIImage(named: const)
        }
    }
    
    //kusum_helper
    
    func crateBurgerButton () {
        let icon = UIImage(named: "Burger")
        burgerButton.addTarget(self, action: #selector(showMenu), for: .touchUpInside)
        burgerButton.imageView?.contentMode = .scaleAspectFill
        burgerButton.setImage(icon, for: .normal)
        self.navigationBar.addSubview(burgerButton)
        burgerButton.clipsToBounds = true
        burgerButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            burgerButton.centerYAnchor.constraint(equalTo: navigationBar.centerYAnchor),
            burgerButton.leftAnchor.constraint(equalTo: navigationBar.leftAnchor, constant: 16),
//            burgerButton.bottomAnchor.constraint(equalTo: navigationBar.bottomAnchor, constant: 0)
//            ,
            burgerButton.heightAnchor.constraint(equalToConstant: 24),
            burgerButton.widthAnchor.constraint(equalToConstant: 24),
        ])
        burgerButton.contentMode = .scaleAspectFit
    }
    
    func crateUpdateButton () {
        let icon = UIImage(named: "kusum_update")
        updateButton.addTarget(self, action: #selector(update), for: .touchUpInside)
        updateButton.imageView?.contentMode = .scaleAspectFill
        updateButton.setImage(icon, for: .normal)
        self.navigationBar.addSubview(updateButton)
        updateButton.clipsToBounds = true
        updateButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            updateButton.bottomAnchor.constraint(equalTo: navigationBar.bottomAnchor, constant: -8),
            updateButton.rightAnchor.constraint(equalTo: navigationBar.rightAnchor, constant: -16),
            updateButton.heightAnchor.constraint(equalToConstant: 38),
            updateButton.widthAnchor.constraint(equalToConstant: 83),
        ])
        updateButton.contentMode = .scaleAspectFit
        updateButton.adjustsImageWhenHighlighted = false
    }
    
    func crateBackButton () {
        let icon = UIImage(named: "arrow_white")
        backButton.addTarget(self, action: #selector(back), for: .touchUpInside)
        backButton.imageView?.contentMode = .scaleAspectFill
        backButton.setImage(icon, for: .normal)
        self.navigationBar.addSubview(backButton)
        backButton.clipsToBounds = true
        backButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            backButton.bottomAnchor.constraint(equalTo: navigationBar.bottomAnchor, constant: -8),
            backButton.rightAnchor.constraint(equalTo: self.updateButton.leftAnchor, constant: -10),
            backButton.heightAnchor.constraint(equalToConstant: 25),
            backButton.widthAnchor.constraint(equalToConstant: 25),
        ])
        backButton.contentMode = .scaleAspectFit
        backButton.adjustsImageWhenHighlighted = false
    }
    //LeftArrow
    
//    func crateHelperButton () {
//        let icon = UIImage(named: "kusum_helper")
//        helperButton.addTarget(self, action: #selector(showHelperViewController), for: .touchUpInside)
//        helperButton.imageView?.contentMode = .scaleAspectFill
//        helperButton.setImage(icon, for: .normal)
//        self.navigationBar.addSubview(helperButton)
//        helperButton.clipsToBounds = true
//        helperButton.translatesAutoresizingMaskIntoConstraints = false
//        NSLayoutConstraint.activate([
//            helperButton.bottomAnchor.constraint(equalTo: navigationBar.bottomAnchor, constant: -8),
//            helperButton.rightAnchor.constraint(equalTo: navigationBar.rightAnchor, constant: -16),
//            helperButton.heightAnchor.constraint(equalToConstant: 38),
//            helperButton.widthAnchor.constraint(equalToConstant: 83),
//        ])
//        helperButton.contentMode = .scaleAspectFit
//        helperButton.adjustsImageWhenHighlighted = false
//    }
    
    func hideBurger () {
        self.burgerButton.isHidden = true
    }
    
    func showBurger () {
        self.burgerButton.isHidden = false
    }
    
    func hideOrShowAchivments () {
//        if user.specific == 7 {
//            self.hideAchivment()
//        } else {
//            self.showAchivment()
//        }
    }
    
    func hideAchivment () {
        self.achivmentItem = self.createRoundView()
        let navigationItem = UINavigationItem()
        let burgerIcon = self.createBurgerIcon()
            
        navigationItem.leftBarButtonItem = burgerIcon
        navigationItem.rightBarButtonItems = []
            
        navigationBar.items = [navigationItem]
    }
    
    func showAchivment () {
        self.achivmentItem = self.createRoundView()
        let navigationItem = UINavigationItem()
        let burgerIcon = self.createBurgerIcon()
        
        navigationItem.leftBarButtonItem = burgerIcon
        navigationItem.rightBarButtonItems = [self.achivmentItem]
        
        navigationBar.items = [navigationItem]
    }
    
    private func createBurgerIcon () -> UIBarButtonItem {
        let icon = UIImage(named: "Burger")
        let iconSize = CGRect(origin: CGPoint.zero, size: CGSize(width: 32, height: 27))
        let iconButton = UIButton(frame: iconSize)
        iconButton.setBackgroundImage(icon, for: .normal)
        let barButton = UIBarButtonItem(customView: iconButton)
        iconButton.addTarget(self, action: #selector(showMenu), for: .touchUpInside)
        return barButton
    }
    
    func createRoundView () -> UIBarButtonItem {
        let countText = String(notificationCount)
        let font = UIFont(name: "Helvetica", size: 17)
        let width = self.heightForView(text: countText, font: font!, width: 20)
        let roundView = UIView(frame: CGRect(x: 0, y: 0, width: width + 10, height: 30))
        roundView.backgroundColor = UIColor.white
        roundView.layer.cornerRadius = roundView.frame.size.height / 2
        let notCount = UILabel()
        notCount.frame = CGRect(origin: CGPoint.zero, size: CGSize(width: width, height: 20))
        notCount.textColor = PINK
        notCount.text = countText
        notCount.textAlignment = .center
        notCount.center = roundView.center
        roundView.addSubview(notCount)
            
        notCount.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(userDidTapLabel(tapGestureRecognizer:)))
        notCount.addGestureRecognizer(tapGesture)
            
        let roundNav = UIBarButtonItem(customView: roundView)
        contLabel = notCount
        return roundNav
    }
    
    @objc func userDidTapLabel(tapGestureRecognizer: UITapGestureRecognizer) {
        print("tap")
//        if user.specific != 2 {
            let drawerController = navigationController?.viewControllers[1] as? KYDrawerController
            var vc = CustomeNC()
            vc = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AchivmentVC") as? CustomeNC)!
            vc.titleText = "Достижения"
            drawerController?.mainViewController = vc
            drawerController?.setDrawerState(KYDrawerController.DrawerState.closed, animated: true)
//        }
    }
    
    private func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.sizeToFit()
        
        return label.frame.height
    }
    
    func updateLebel() {
//        contLabel.text = String(user.achievement)
    }
    
    /*
     self.title = "Tittle"
     let attrs = [
     NSAttributedStringKey.foregroundColor: UIColor.red,
     NSAttributedStringKey.font: UIFont(name: "Georgia-Bold", size: 24)!
     ]
     
     self.navigationBar.titleTextAttributes = attrs
     NOT WORKING
     */
    func addTittleToNavigationBar () {
        let label = UILabel()
//        label.frame = CGRect(origin: CGPoint.zero, size: CGSize(width: 180, height: 20))
        label.textColor = UIColor.white
        label.textAlignment = .center
//        label.center = self.navigationBar.center
        label.text = titleText
        self.navigationBar.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            label.centerXAnchor.constraint(equalTo: navigationBar.centerXAnchor),
            label.centerYAnchor.constraint(equalTo: navigationBar.centerYAnchor),
//            label.centerXAnchor.constraint(equalTo: navigationBar.centerXAnchor),
//            label.rightAnchor.constraint(equalTo: self.updateButton.leftAnchor, constant: 8),
//            label.rightAnchor.constraint(equalTo: self.helperButton.leftAnchor, constant: 8),
//            label.leftAnchor.constraint(equalTo: self.burgerButton.rightAnchor, constant: 8),
            label.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width - 46 * 2),
            
//                        label.widthAnchor.constraint(equalToConstant: 180),
        ])
        print(label)
    }
    
    @objc func update() {
        self.ncDelegate?.update()
    }
    
    @objc func back() {
        self.ncDelegate?.back()
    }
    
    @objc func showHelperViewController() {
        if let rootViewController = UIApplication.topViewController() {
            if let drawer = rootViewController as? KYDrawerController {
                if let nc = drawer.drawerViewController as? UINavigationController {
                    if let myDrawer = nc.viewControllers.first as? DraverMenuVC {
                        myDrawer.drawerMenu?.findDrawerFromName(name: "Консиліум допомагає")
                    }
                }
            }
        }
    }
    
    @objc func showMenu() {
        let drawerController = self.parent as? KYDrawerController
        drawerController?.drawerWidth = self.view.frame.width - 32
        drawerController?.setDrawerState(KYDrawerController.DrawerState.opened, animated: true)
    }
    
}

@IBDesignable extension UINavigationController {
    @IBInspectable var barTintColor: UIColor? {
        set {
            guard let uiColor = newValue else { return }
            navigationBar.barTintColor = uiColor
        }
        get {
            guard let color = navigationBar.barTintColor else { return nil }
            return color
        }
    }
}
