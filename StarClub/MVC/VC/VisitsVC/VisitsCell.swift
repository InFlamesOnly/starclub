//
//  VisitsCell.swift
//  StarClub
//
//  Created by macOS on 11.03.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit

class VisitsCell: UITableViewCell {
    
    @IBOutlet weak var roundView: UIView!
    @IBOutlet weak var visitNumberLabel: UILabel!
    @IBOutlet weak var visitDateLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        roundView.layer.cornerRadius = CELLBORDERRADIUS
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
