//
//  VisitsVC.swift
//  StarClub
//
//  Created by macOS on 11.03.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit

class VisitsVC: LightViewController {
    
    var patient = Patient()
    
    @IBOutlet weak var visitsTableView: UITableView!
    @IBOutlet weak var errorLabel: UILabel!
    
    var patientsArray : Array<Patient> = Array()
    let indicator = SpringIndicator()
    
    let user = User().currentUser()

    override func viewDidLoad() {
        super.viewDidLoad()

//        self.visitsTableView.isHidden = true
//        self.errorLabel.isHidden = true
        self.getVisits()
        
    }
    
    func getVisits () {
//        self.createIndicator()
        RequsetManager.sharedInstance.getVisits(patientId: self.patient.patientId, success: { (visitsList) in
            
        }) { (errorCode) in
            
        }
//        RequsetManager.sharedInstance.getPatientList(success: { (patientList) in
//            self.patientsTableView.isHidden = false
//            self.patientsArray = Patient.mapResponseToArrayObject(response: patientList)
//            if self.patientsArray.count == 0 {
//                self.patientsTableView.isHidden = true
//                self.errorLabel.isHidden = false
//            }
//            self.patientsTableView.reloadData()
//            self.indicator.removeFromSuperview()
//        }) { (errorCode) in
//            self.patientsTableView.isHidden = false
//            self.indicator.removeFromSuperview()
//        }
    }
    
    func createIndicator () {
        indicator.frame.size = CGSize(width: 100, height: 100)
        indicator.center = self.view.center
        indicator.lineColor = PINK
        self.view.addSubview(indicator)
        indicator.start()
    }
}

extension VisitsVC : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "showVisit", sender: self)
    }
}

extension VisitsVC : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VisitsCell", for: indexPath) as! VisitsCell
        cell.selectionStyle = .none
        return cell
    }
}
