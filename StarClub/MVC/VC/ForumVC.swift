//
//  File.swift
//  StarClub
//
//  Created by macOS on 12/9/20.
//  Copyright © 2020 macOS. All rights reserved.
//

import Foundation
import UIKit

class ForumVC: UIViewController {
    
    @IBOutlet weak var webView : UIWebView!
    let indicator = SpringIndicator()
    let user = User().currentUser()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.webView.isHidden = true
        self.createIndicator()
        let urlString = "http://kusumdoctors.co.ua/forum/3.php?utm_source=KD_forum&token=\(user.token)&speciality=\(user.specific)"
        self.webView.loadRequest(URLRequest(url: URL(string: urlString)!))
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        if self.navigationController is CustomeNC {
            let nc = self.navigationController as! CustomeNC
            nc.ncDelegate = self
            nc.updateButton.isHidden = false
            nc.backButton.isHidden = nc.updateButton.isHidden
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super .viewWillDisappear(animated)
        if self.navigationController is CustomeNC {
            let nc = self.navigationController as! CustomeNC
            nc.ncDelegate = self
            nc.updateButton.isHidden = true
            nc.backButton.isHidden = nc.updateButton.isHidden
        }
    }
    
    func createIndicator () {
        indicator.frame.size = CGSize(width: 100, height: 100)
        indicator.center = self.view.center
        indicator.lineColor = PINK
        self.view.addSubview(indicator)
        indicator.start()
    }
}

extension ForumVC : CustomeNCDelegate {
    func update() {
        self.webView.isHidden = true
        self.createIndicator()
        self.webView.reload()
    }
    
    func back () {
        self.webView.goBack()
    }
}

extension ForumVC : UIWebViewDelegate {
    func webViewDidFinishLoad(_ webView: UIWebView) {
            self.webView.isHidden = false
            self.indicator.removeFromSuperview()
    }
}
