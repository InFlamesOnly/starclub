//
//  ViewController.swift
//  StarClub
//
//  Created by macOS on 12.12.17.
//  Copyright © 2017 macOS. All rights reserved.
//

import UIKit
import FirebaseMessaging
import KYDrawerController

var startNavigationText = ""

class LoginVC: LightViewController, UITextFieldDelegate, ForgotePasswordSwipeViewDelegate,LoginSwipeViewDelegate,CodeSwipeViewDelegate,ViewPagerDataSource {
    
    @IBOutlet weak var viewPager: ViewPager!
    
    var loginView : LoginSwipeView!
    var forgotPasswordView : ForgotePasswordSwipeView!
    var codeView : CodeSwipeView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        self.initionalSwipeViews()
//        viewPager.scrollToPage(index: 1)
    }
    private func createLoaderView () -> LoaderView {
        let loaderView = LoaderView()
        loaderView.startFromView(view: self.view)
        return loaderView
    }
    
    private func startApp () {
        let user = User().currentUser()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "KYDrawerController") as! KYDrawerController
        if user.specific == 2 {
            let drug = Drug()
            drug.addDrugsToGastroDataBase()
            startNavigationText = "Стрічка"
            self.navigationController?.pushViewController(controller, animated: true)
        } else if user.specific == 3 || user.specific == 6 {
            let drug = Drug()
            drug.addDrugsToCardioDataBase()
            startNavigationText = "Стрічка"
            self.navigationController?.pushViewController(controller, animated: true)
        } else if user.specific > 2 && user.specific < 6 {
            if user.specific == 5 {
                let drug = Drug()
                drug.allDrugs()
                controller.mainSegueIdentifier = "mainUR"
                startNavigationText = "Консультація"
                self.navigationController?.pushViewController(controller, animated: true)
                return
            }
            let drug = Drug()
            drug.addDrugsToRealmDataBase()
            controller.mainSegueIdentifier = "mainUR"
            startNavigationText = "Консультація"
            self.navigationController?.pushViewController(controller, animated: true)
        } else if user.specific == 7 {
            controller.mainSegueIdentifier = "mainPatient"
            startNavigationText = "Карточка пациента"
            self.navigationController?.pushViewController(controller, animated: true)
        } else {
            let drug = Drug()
            drug.addDrugsToRealmDataBase()
            startNavigationText = "Стрічка"
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    //PatientVC
    
    //MARK: - API request
    private func getUserFromServer () {
        let loginIsCorrect = self.checkLoginFromCorrect(textField: loginView.loginTextField, view: loginView.loginBorderView)
        let passwordIsCorrect = self.checkPasswordFromCorrect(textField: loginView.passwordTextField, view: loginView.passwordBorderView)
        if loginIsCorrect &&  passwordIsCorrect {
            let loaderView = createLoaderView()
            let number = String(loginView.loginTextField.text!.filter { !" ()-".contains($0) })
            RequsetManager.sharedInstance.authUserFromServer(login: number, password: loginView.passwordTextField.text!, success: { (_ responseObject: Dictionary<String, Any>) in
                loaderView.removeFromSuperview()
                self.startApp()
            }) { (_ error: Int?) in
                loaderView.removeFromSuperview()
                RequsetManager.sharedInstance.showError(viewController: self, code: error!)
                }
        }
    }
    
    private func rememberPassword () {
        let loginIsCorrect = self.checkLoginFromCorrect(textField: forgotPasswordView.loginTextField, view: forgotPasswordView.loginBorderView)
        if loginIsCorrect {
            let token = Messaging.messaging().fcmToken
            let loaderView = createLoaderView()
            let number = String(forgotPasswordView.loginTextField.text!.filter { !" ()-".contains($0) })
            RequsetManager.sharedInstance.forgotPasswordFromServer(login: number, firebaseToken: token!, success: { (_ responseObject: Dictionary<String, Any>) in
                print("SUCCES REMEMBER PASSWORD")
                loaderView.removeFromSuperview()
                self.viewPager.scrollToPage(index: 3)
            }, failure: { (_ error: Int?) in
                loaderView.removeFromSuperview()
                RequsetManager.sharedInstance.showError(viewController: self, code: error!)
            })
        }
    }
    
    private func changePassword () {
        let newPasswordIsEquals = self.equalingPassword()
        if newPasswordIsEquals {
            let loaderView = createLoaderView()
            let number = String(forgotPasswordView.loginTextField.text!.filter { !" ()-".contains($0) })
            RequsetManager.sharedInstance.changePassword(login: number, code: Int(codeView.codeTextField.text!)!, newPassword: codeView.newPasswordTextField.text!, success: { (_ responseObject: Dictionary<String, Any>) in
                loaderView.removeFromSuperview()
                self.viewPager.scrollToPage(index: 1)
            }, failure: { (_ error: Int?) in
                loaderView.removeFromSuperview()
                RequsetManager.sharedInstance.showError(viewController: self, code: error!)
            })
        }
    }
    
    //MARK: - Test text fields from correct
//    - (BOOL)textFieldShouldReturn:(UITextField *)textField {
//    [textField resignFirstResponder];
//    return NO;
//    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("return tap")
        return true
    }
    
    private func equalingPassword () -> Bool {
        var isCorrect = false
        if codeView.newPasswordTextField.text == codeView.acceptPasswordTextField.text {
            isCorrect = true
            codeView.newPasswordBorderView.fieldsIsCorrect()
            codeView.acceptPasswordBorderView.fieldsIsCorrect()
        } else {
            isCorrect = false
            codeView.newPasswordBorderView.errorFields()
            codeView.acceptPasswordBorderView.errorFields()
        }
        return isCorrect
    }
    
    private func checkLoginFromCorrect (textField : UITextField , view : RoundViewWithBorder) -> Bool {
        var isCorrect = false
        if (textField.text?.count)! < 19 {
            view.errorFields()
            isCorrect = false
        } else {
            view.fieldsIsCorrect()
            isCorrect = true
        }
        return isCorrect
    }
    
    private func checkPasswordFromCorrect (textField : UITextField , view : RoundViewWithBorder) -> Bool {
        var isCorrect = false
        if (textField.text?.count)! <= 4 {
            view.errorFields()
            isCorrect = false
        } else {
            view.fieldsIsCorrect()
            isCorrect = true
        }
        return isCorrect
    }
    
    //MARK: - Actions
    //MARK: - SwipeViewDelegate
    func goToCodeSwipeView() {
        self.rememberPassword()
    }
    
    func backToLoginSwipeView() {
        viewPager.scrollToPage(index: 1)
    }
    func backToForgotPasswordSwipeView() {
        viewPager.scrollToPage(index: 2)
    }
    
    func goToNewPasswordSwipeView() {
        self.changePassword()
    }
    
    func loginUser() {
        self.getUserFromServer()
    }
    
    func goToForgotPasswordSwipeView() {
        forgotPasswordView.loginTextField.text = loginView.loginTextField.text
        viewPager.scrollToPage(index: 2)
    }
    
    //MARK: - ViewPagerDataSource
    func numberOfItems(viewPager: ViewPager) -> Int {
        return 3
    }
    
    func viewAtIndex(viewPager: ViewPager, index: Int, view: UIView?) -> UIView {
        var view = UIView()
        if index == 0 {
            view = loginView
        } else if index == 1 {
            view = forgotPasswordView
        } else if index == 2 {
            view = codeView
        }
        return view
    }
    
    private func initionalSwipeViews () {
        loginView = (self.loadViewFromBundel(name: "LoginSwipeView") as? LoginSwipeView)!
        forgotPasswordView = (self.loadViewFromBundel(name: "ForgotePasswordSwipeView") as? ForgotePasswordSwipeView)!
        codeView = (self.loadViewFromBundel(name: "CodeSwipeView") as? CodeSwipeView)!
        viewPager.dataSource = self
    }
    
    private func loadViewFromBundel(name : String) -> UIView {
        var view = UIView ()
        switch name {
        case "ForgotePasswordSwipeView":
            if let customView = Bundle.main.loadNibNamed(name, owner: self, options: nil)?.first as? ForgotePasswordSwipeView {
                customView.frame = CGRect(x: 0, y: 0, width: viewPager.frame.size.width, height: viewPager.frame.size.height)
                customView.delegate = self
                view = customView
                break
            }
        case "LoginSwipeView":
            if let customView = Bundle.main.loadNibNamed(name, owner: self, options: nil)?.first as? LoginSwipeView {
                customView.frame = CGRect(x: 0, y: 0, width: viewPager.frame.size.width, height: viewPager.frame.size.height)
                customView.delegate = self
//                customView.loginTextField.delegate = self
//                customView.passwordTextField.delegate = self
                view = customView
                break
            }
        case "CodeSwipeView":
            if let customView = Bundle.main.loadNibNamed(name, owner: self, options: nil)?.first as? CodeSwipeView {
                customView.frame = CGRect(x: 0, y: 0, width: viewPager.frame.size.width, height: viewPager.frame.size.height)
                customView.delegate = self
                view = customView
                break
            }
        default:
            break
        }
        return view
    }
}
