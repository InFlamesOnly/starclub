//
//  CalculatorVCSCORE.swift
//  StarClub
//
//  Created by macOS on 13.11.2018.
//  Copyright © 2018 macOS. All rights reserved.
//

import UIKit

class CalculatorVCSCORE: LightViewController, UITextFieldDelegate {
    
    @IBOutlet weak var genderDropDown : DropDown!
    @IBOutlet weak var ageTextField : UITextField!
    @IBOutlet weak var adLevelTextField : UITextField!
    @IBOutlet weak var smokeDropDown : DropDown!
    @IBOutlet weak var plazmTextField : UITextField!
    
    @IBOutlet weak var resultTittle: UILabel!
    @IBOutlet weak var rezultLabel : UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.rezultLabel.isHidden = true
        self.resultTittle.isHidden = true
        
        self.genderDropDown.optionArray = ["Чоловіча", "Жіноча"]
        self.smokeDropDown.optionArray = ["Так", "Ні"]
        
        self.genderDropDown.didSelect{(selectedText , index ,id) in
            self.genderDropDown.placeholder = selectedText
            self.genderDropDown.textAlignment = .center
        }
        
        self.smokeDropDown.didSelect{(selectedText , index ,id) in
            self.smokeDropDown.placeholder = selectedText
            self.smokeDropDown.textAlignment = .center
        }
    }
    
    
    @IBAction func calculate(_ sender: Any) {
        var smoke = 0
        var gender = 0
        if  genderDropDown.placeholder == "Жіноча" || genderDropDown.text == "Жіноча" {
            gender = 1
        }
        
        if  smokeDropDown.placeholder == "Так" || smokeDropDown.text == "Так"{
            smoke = 1
        }
        
        
        if self.ageTextField.text?.count == 0 && self.adLevelTextField.text?.count == 0 && self.plazmTextField.text?.count == 0 {
            self.alert(message: "Заповніть всі поля")
            return
        }
        
        if self.ageTextField.text?.count == 0 {
            self.alert(message: "Вкажіть ваш вік")
            return
        }

        if self.adLevelTextField.text?.count == 0 {
            self.alert(message: "Вкажіть рівень систолічного артеріального тиску")
            return
        } else {
            if Double(self.adLevelTextField.text!)! < 100 || Double(self.adLevelTextField.text!)! > 180 {
                self.alert(message: "Рівень систолічного артеріального тиску повинен бути в межах від 100 до 180 мм рт.ст.!")
                return
            }
        }
        
        if self.plazmTextField.text?.count == 0 {
            self.alert(message: "Вкажіть холістерін плазми")
            return
        } else {
            if Double(self.plazmTextField.text!)! < 3 || Double(self.plazmTextField.text!)! > 8 {
                self.alert(message: "Рівень холестерину повинен бути в межах від 3 до 8 ммоль / л!")
                return
            }
        }
        
        let rezult = self.calculate(gender: gender, smoke: smoke, age: Int(self.ageTextField.text!)!, adLevel: Double(self.adLevelTextField.text!)!, plazma: Double(self.plazmTextField.text!)!)
        
        self.rezultLabel.isHidden = false
        self.resultTittle.isHidden = false
        
        if rezult < 1 {
            self.rezultLabel.text = "\(Int(rezult)) % Низький ризик"
        } else if rezult > 1 && rezult < 5 {
            self.rezultLabel.text = "\(Int(rezult)) % Середній ризик"
        } else if rezult > 5 && rezult < 10 {
            self.rezultLabel.text = "\(Int(rezult)) % Високий ризик"
        } else if rezult > 10 {
            self.rezultLabel.text = "\(Int(rezult)) % Дуже високий ризик"
        }
        self.view.endEditing(true)
    }
    
//    Уровень суммарного СС риск по шкале SCORE:
//    менее 1% - низким.
//    от >1 до 5% - средний или умеренно повышенный.
//    от >5% до 10% - высокий.
//    >10% - очень высокий.
    
    func calculate (gender : Int, smoke : Int, age : Int, adLevel : Double, plazma : Double) -> Double {
        
        var alpha = 0.0
        var p = 0.0
        
        if gender == 1 {
            alpha = -28.7
            p = 6.23
        } else {
            alpha = -21.0
            p = 4.62
        }
        
        let cs0 = exp(-exp(alpha)*pow(Double(age) - 20.0, p))
        let cs10 = exp(-exp(alpha)*pow(Double(age) - 10.0, p))
        
        if gender == 1 {
            alpha = -30.0
            p = 6.42;
        } else {
            alpha = -25.7
            p = 5.47
        }
        
        let ncs0 = exp(-exp(alpha)*pow(Double(age) - 20.0, p))
        let ncs10 = exp(-exp(alpha)*pow(Double(age) - 10.0, p))
        
        
        var bchol = 0.24
        var bsbp = 0.018
        var bsm = Double(smoke) * 0.71
        let wc = bchol * (Double(plazma)-6.0) + bsbp * (Double(adLevel)-120.0) + bsm
        
        bchol = 0.02
        bsbp = 0.022
        bsm = Double(smoke)*0.63
        let wnc = bchol * (Double(plazma)-6.0) + bsbp * (Double(adLevel)-120.0) + bsm
        
        let cs = pow(cs0, exp(wc))
        var cs1 = pow(cs10, exp(wc))

        let ncs = pow(ncs0, exp(wnc))
        var ncs1 = pow(ncs10, exp(wnc))
        
        cs1 = cs1/cs
        ncs1 = ncs1/ncs
        
        let r = 1.0-cs1
        let r1 = 1.0-ncs1
        
        let result = (100.0*(r+r1))// ---- вот это результат

        return result
    }
}

extension Double {
    func roundTo(places: Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
