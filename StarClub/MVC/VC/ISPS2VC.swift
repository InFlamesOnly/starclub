//
//  ISPS2VC.swift
//  StarClub
//
//  Created by macOS on 2/7/20.
//  Copyright © 2020 macOS. All rights reserved.
//

import UIKit

class ISPS2VC: UIViewController, UIWebViewDelegate {
    
    @IBOutlet weak var webView : UIWebView!
    let indicator = SpringIndicator()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.createIndicator()
        self.webView.isHidden = true
        self.webView.delegate = self
        self.webView.loadRequest(URLRequest(url: URL(string: "http://kusumdoctors.co.ua/?page_id=784")!))
    }
    
    func createIndicator () {
        indicator.frame.size = CGSize(width: 100, height: 100)
        indicator.center = self.view.center
        indicator.lineColor = PINK
        self.view.addSubview(indicator)
        indicator.start()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.indicator.removeFromSuperview()
        self.webView.isHidden = false
    }
}
