//
//  PatientVC.swift
//  StarClub
//
//  Created by macOS on 01.02.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit
import Photos

@available(iOS 10.2, *)
class PatientVC: LightViewController {
    
    @IBOutlet fileprivate var captureButton: UIButton!
    @IBOutlet fileprivate var capturePreviewView: UIView!
    @IBOutlet fileprivate var acceptView: UIView!
    @IBOutlet fileprivate var acceptImageView: UIImageView!
    
    @IBOutlet fileprivate var acceptLabel: UILabel!
    
    var uploadedImage = UIImage()
    
    let cameraController = CameraController()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureCameraController()
        self.styleCaptureButton()
        self.capturePreviewView.isHidden = true
        self.acceptView.isHidden = true
        self.acceptLabel.isHidden = true
    }
    
    func configureCameraController() {
        cameraController.prepare {(error) in
            if let error = error {
                print(error)
            }
            
            try? self.cameraController.displayPreview(on: self.capturePreviewView)
        }
    }
    
    @IBAction func captureImage(_ sender: UIButton) {
        cameraController.captureImage {(image, error) in
            guard let image = image else {
                print(error ?? "Image capture error")
                return
            }
            self.acceptView.isHidden = false
            self.acceptImageView.image = image
            self.uploadedImage = image
        }
    }
    
    @IBAction func showCameraView(_ sender: UIButton) {
        self.capturePreviewView.isHidden = false
    }
    
    @IBAction func hideCameraView(_ sender: UIButton) {
        self.capturePreviewView.isHidden = true
        self.acceptView.isHidden = true
    }
    
    @IBAction func sendPhotoFromServer(_ sender: UIButton) {
        self.sendPhoto()
        print("send photo")
    }
    
    func sendPhoto() {
        let loaderView = createLoaderView()
        RequsetManager.sharedInstance.sendPhoto(photo: self.uploadedImage, success: { (success) in
            self.capturePreviewView.isHidden = true
            self.acceptView.isHidden = true
            self.acceptLabel.isHidden = false
            loaderView.removeFromSuperview()
        }) { (errorCode) in
            loaderView.removeFromSuperview()
        }
    }
    
    func styleCaptureButton() {
        captureButton.layer.borderColor = UIColor.black.cgColor
        captureButton.layer.borderWidth = 2
        
        captureButton.layer.cornerRadius = min(captureButton.frame.width, captureButton.frame.height) / 2
    }
    
    private func createLoaderView () -> LoaderView {
        let loaderView = LoaderView()
        loaderView.startFromView(view: self.view)
        return loaderView
    }
}
