//
//  DrawerMenuTVC.swift
//  StarClub
//
//  Created by macOS on 12.12.17.
//  Copyright © 2017 macOS. All rights reserved.
//

import UIKit
import KYDrawerController
import RealmSwift
//1 Лента
//2 Календарь
//3 Препараты
//4 Статьи
//5 Конферении
//6 ICPC-2 Классификация ПМД
//7 Юридическая консульця популярные вопросы
//8 Юр конс все вопросы
//9 мед консультация все вопросы по эссенкиальной АГ
//10 Вопросы по симотрической АГ
//11 Вопросы по СД 2 типа
//12 Консилиум по сложным клиническим случаям
//13 Другие вопросы
//14 Калькулятор ИМТ
//15 Риск ССЗ по шкале Скор
//16 Скорость клубчковой филтрации
//17 Библиотека
//18 Видео кейсы
//19 Консилиум

enum DrawerMenuType {
    case tape //Лента
    case calendar //Календарь
    case drugs //Препараты
    case articles //Статьи
    case conferences //Конференции
    case ICPC_PMD //ICPC -2 Классификация ПМД
    case legalAdvicePopularQuestions //Популярные вопросы
    case legalAllQuestions //Все вопросы
    case medicalConsultationAllQuestions //Вопросы по эссенциальной АГ
    case questionsOnSymotricAH //Вопросы по симптоматической АГ
    case typeDiabetesQuestions //Вопросы по СД 2 типа
    case consultationOnComplexClinicalCases //Консилиум по сложным клиническим случаям
    case otherIssues //Другие вопросы
    case BMICalculator //Индекс массы тела
    case CVDriskOnTheScaleScale //Риска ССЗ по шкале SCORE
    case glomerularFiltrationRate //Скорость клубочковой фильтрации
    case library //Библиотека
    case videoCases //Видео кейсы
    case council //Консилиум
    
    case forum //Консилиум
}

class DrawerMenuTVC: UITableViewController {

    //TODO
    var menuNames : [DrawerMenuObject] = []
//    var menuNames = [""]
    
    @IBOutlet var menuTableView: UITableView!
    
    var isULConsOpened = false
    var isMedConsOpened = false
    var isCalcOpened = false
    var isKusumAcademyOpened = false
    
    let validTexts: Set<String> = [
        "Популярні запитання",
        "Усі запитання",
        "Індекс маси тіла",
        "Ризик ССЗ за шкалою SCORE",
        "Клиренс креатинина",
        "Швидкість клубочкової фільтрації",
        "Запитання щодо гіпертонічної хвороби",
        "Вопросы по симптоматической АГ",
        "Запитання щодо порушень ритму серця",
        "Вопросы по СД 2 типа",
        "Консиліум щодо складних клінічних випадків",
        "Другие вопросы",
        "Дозування Укрлів® табл."
    ]
    
    let user = User().currentUser()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.reloadMenu()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.doctorActivity()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.reloadMenu()
    }
    
    func doctorActivity () {
        RequsetManager.sharedInstance.doctorActivity(success: { (resp) in
            
        }) { (error) in
            
        }
    }
    
//    Лента
//    Видео кейсы
//    Опросы
//    Препараты Кусум
//    Протоколы лечения
//    Юридическая консультация
//    Медицинская консультация
//    Календарь
//    ICPC-2
//    Библиотека
//    Калькуляторы
//    Поддержка
//    Настройки

    func reloadMenu () {

        let drawerMenuObj = DrawerMenuObject()
        if user.specific > 2 && user.specific < 6 {
            if user.specific == 3  {
                self.menuNames = [
//                    drawerMenuObj.mainKusumHelper(),
                    drawerMenuObj.feed(),
                    drawerMenuObj.questionnaire(),
                    drawerMenuObj.forum(),
                    drawerMenuObj.videoCases(),
                    drawerMenuObj.articles(),
                    drawerMenuObj.treatmentProtocols(),
                    drawerMenuObj.ISPS2(),
                    drawerMenuObj.calculators(),
                    drawerMenuObj.imt(),
                    drawerMenuObj.score(),
                    drawerMenuObj.skf(),
                    drawerMenuObj.ukrliv(),
                    drawerMenuObj.drugs(),
                    drawerMenuObj.legalConsultation(),
                    drawerMenuObj.popularLegalConsultation(),
                    drawerMenuObj.allLegalConsultation(),
                    drawerMenuObj.calendar(),
                    drawerMenuObj.library(),
                    drawerMenuObj.conference(),
                    drawerMenuObj.academy(),
                    drawerMenuObj.сonsilium(),
                    drawerMenuObj.support(),
                    drawerMenuObj.settings(),
                ]
            } else if user.specific == 5 {
                self.menuNames = [
//                    drawerMenuObj.mainKusumHelper(),
                    drawerMenuObj.feed(),
                    drawerMenuObj.questionnaire(),
                    drawerMenuObj.forum(),
                    drawerMenuObj.videoCases(),
                    drawerMenuObj.articles(),
                    drawerMenuObj.treatmentProtocols(),
                    drawerMenuObj.ISPS2(),
                    drawerMenuObj.calculators(),
                    drawerMenuObj.imt(),
                    drawerMenuObj.score(),
                    drawerMenuObj.skf(),
                    drawerMenuObj.ukrliv(),
                    drawerMenuObj.drugs(),
                    drawerMenuObj.legalConsultation(),
                    drawerMenuObj.popularLegalConsultation(),
                    drawerMenuObj.allLegalConsultation(),
                    drawerMenuObj.calendar(),
                    drawerMenuObj.library(),
                    drawerMenuObj.conference(),
                    drawerMenuObj.academy(),
                    drawerMenuObj.сonsilium(),
                    drawerMenuObj.support(),
                    drawerMenuObj.settings(),
                ]
            }  else {
                self.menuNames = [
//                    drawerMenuObj.mainKusumHelper(),
                    drawerMenuObj.feed(),
                    drawerMenuObj.questionnaire(),
                    drawerMenuObj.forum(),
                    drawerMenuObj.videoCases(),
                    drawerMenuObj.articles(),
                    drawerMenuObj.treatmentProtocols(),
                    drawerMenuObj.ISPS2(),
                    drawerMenuObj.calculators(),
                    drawerMenuObj.imt(),
                    drawerMenuObj.score(),
                    drawerMenuObj.skf(),
                    drawerMenuObj.ukrliv(),
                    drawerMenuObj.legalConsultation(),
                    drawerMenuObj.popularLegalConsultation(),
                    drawerMenuObj.allLegalConsultation(),
                    drawerMenuObj.calendar(),
                    drawerMenuObj.library(),
                    drawerMenuObj.conference(),
                    drawerMenuObj.academy(),
                    drawerMenuObj.сonsilium(),
                    drawerMenuObj.support(),
                    drawerMenuObj.settings(),
                ]
            }
        } else if user.specific == 2 {
            self.menuNames = [
//                drawerMenuObj.mainKusumHelper(),
                drawerMenuObj.feed(),
                drawerMenuObj.questionnaire(),
                drawerMenuObj.forum(),
                drawerMenuObj.videoCases(),
                drawerMenuObj.articles(),
                drawerMenuObj.treatmentProtocols(),
                drawerMenuObj.ISPS2(),
                drawerMenuObj.calculators(),
                drawerMenuObj.imt(),
                drawerMenuObj.score(),
                drawerMenuObj.skf(),
                drawerMenuObj.ukrliv(),
                drawerMenuObj.drugs(),
                
                drawerMenuObj.legalConsultation(),
                drawerMenuObj.popularLegalConsultation(),
                drawerMenuObj.allLegalConsultation(),
                drawerMenuObj.calendar(),
                drawerMenuObj.library(),
                drawerMenuObj.conference(),
                drawerMenuObj.academy(),
                drawerMenuObj.сonsilium(),
                drawerMenuObj.support(),
                drawerMenuObj.settings(),
            ]
        } else if user.specific == 6 {
            self.menuNames = [
//                drawerMenuObj.feed(),
//                drawerMenuObj.mainKusumHelper(),
                drawerMenuObj.questionnaire(),
                drawerMenuObj.forum(),
                drawerMenuObj.videoCases(),
                drawerMenuObj.articles(),
                drawerMenuObj.treatmentProtocols(),
                drawerMenuObj.ISPS2(),
                drawerMenuObj.calculators(),
                drawerMenuObj.imt(),
                drawerMenuObj.score(),
                drawerMenuObj.skf(),
                drawerMenuObj.ukrliv(),
                drawerMenuObj.drugs(),
                
                drawerMenuObj.legalConsultation(),
                drawerMenuObj.popularLegalConsultation(),
                drawerMenuObj.allLegalConsultation(),
                drawerMenuObj.calendar(),
                drawerMenuObj.library(),
                drawerMenuObj.conference(),
                drawerMenuObj.academy(),
                drawerMenuObj.сonsilium(),
                drawerMenuObj.support(),
                drawerMenuObj.settings(),
            ]
        } else if user.specific == 7 {
            self.menuNames = [
//                drawerMenuObj.mainKusumHelper(),
                 drawerMenuObj.feed(),
                drawerMenuObj.questionnaire(),
                drawerMenuObj.forum(),
                drawerMenuObj.videoCases(),
                drawerMenuObj.articles(),
                drawerMenuObj.treatmentProtocols(),
                drawerMenuObj.ISPS2(),
                drawerMenuObj.calculators(),
                drawerMenuObj.imt(),
                drawerMenuObj.score(),
                drawerMenuObj.skf(),
                drawerMenuObj.ukrliv(),
                drawerMenuObj.drugs(),
                
                drawerMenuObj.legalConsultation(),
                drawerMenuObj.popularLegalConsultation(),
                drawerMenuObj.allLegalConsultation(),
                drawerMenuObj.calendar(),
                drawerMenuObj.library(),
                drawerMenuObj.conference(),
                drawerMenuObj.patientCart(),
                drawerMenuObj.academy(),
                drawerMenuObj.сonsilium(),
                drawerMenuObj.support(),
                drawerMenuObj.settings(),
            ]
        } else {
            if user.specific == 77 {
                self.menuNames = [
                drawerMenuObj.forum()
                ]
            } else {
            self.menuNames = [
//                drawerMenuObj.mainKusumHelper(),
                drawerMenuObj.feed(),
                drawerMenuObj.questionnaire(),
                drawerMenuObj.forum(),
                drawerMenuObj.videoCases(),
                drawerMenuObj.articles(),
                drawerMenuObj.treatmentProtocols(),
                drawerMenuObj.ISPS2(),
                drawerMenuObj.calculators(),
                drawerMenuObj.imt(),
                drawerMenuObj.score(),
                drawerMenuObj.skf(),
                drawerMenuObj.ukrliv(),
                drawerMenuObj.drugs(),
                
                drawerMenuObj.legalConsultation(),
                drawerMenuObj.popularLegalConsultation(),
                drawerMenuObj.allLegalConsultation(),
                drawerMenuObj.calendar(),
                drawerMenuObj.library(),
                drawerMenuObj.conference(),
                drawerMenuObj.academy(),
                drawerMenuObj.сonsilium(),
                drawerMenuObj.conference(),
                drawerMenuObj.academy(),
                drawerMenuObj.сonsilium(),
                drawerMenuObj.support(),
                drawerMenuObj.settings(),
            ]
            }
        }
        
        let isShowMed = UserDefaults.standard.integer(forKey: "isShow")
        
        if user.specific != 77 {
            if isShowMed == 1 {
                let index = self.menuNames.index(where: { $0.name == "Усі запитання" })
                self.menuNames.insert(drawerMenuObj.medCons(), at: index! + 1)
    //            self.menuNames.insert(drawerMenuObj.esAg(), at: index! + 2)
                self.menuNames.insert(drawerMenuObj.simptAg(), at: index! + 2)
                self.menuNames.insert(drawerMenuObj.heart(), at: index! + 3)
                self.menuNames.insert(drawerMenuObj.twoType(), at: index! + 4)
                self.menuNames.insert(drawerMenuObj.difficultСases(), at: index! + 5)
                self.menuNames.insert(drawerMenuObj.otherIssues(), at: index! + 6)
            }
        }

        self.tableView.reloadData()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.selectDrawerItem(itemCount: indexPath.row)
    }
    
    func selectDrawerItem (itemCount : Int) {
        let drawerController = navigationController?.parent as? KYDrawerController
        var vc = CustomeNC()
        let drawerObj = self.menuNames[itemCount]
        
        if drawerObj.vcStoryboard != "" {
            vc = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: drawerObj.vcStoryboard) as? CustomeNC)!
            vc.titleText = drawerObj.name
        }

        self.checkLegalConsultation(obj: drawerObj)
        self.checkMedicalConsultation(obj: drawerObj)
        self.checkAcademy(obj: drawerObj)
        
        if drawerObj.name == "Юридична консультація" {
            self.isULConsOpened = !self.isULConsOpened
            self.reloadRows()
        } else if drawerObj.name == "Медична консультація" {
            self.isMedConsOpened = !self.isMedConsOpened
            self.reloadRows()
        } else if drawerObj.name == "Калькулятори" {
            self.isCalcOpened = !self.isCalcOpened
            self.reloadRows()
        } else if drawerObj.name == "" {
            
        }
        
        if drawerObj.vcStoryboard != "" {
            drawerController?.mainViewController = vc
            drawerController?.setDrawerState(KYDrawerController.DrawerState.closed, animated: true)
        }
    }
    
    func reloadRows () {
        self.tableView.beginUpdates()
        self.tableView.reloadRows(at:[], with: UITableView.RowAnimation.none)
        self.tableView.endUpdates()
    }
    
    func findDrawerFromName (name : String) {
        let drawerController = navigationController?.parent as? KYDrawerController
        var vc = CustomeNC()
        
        if let item = self.menuNames.first(where: {$0.name == name}) {
           if item.vcStoryboard != "" {
               vc = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: item.vcStoryboard) as? CustomeNC)!
               vc.titleText = item.name
           }

           self.checkLegalConsultation(obj: item)
           self.checkMedicalConsultation(obj: item)
           self.checkAcademy(obj: item)

           if item.vcStoryboard != "" {
               drawerController?.mainViewController = vc
               drawerController?.setDrawerState(KYDrawerController.DrawerState.closed, animated: true)
           }
        }
//
//        if drawerObj.vcStoryboard != "" {
//            vc = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: drawerObj.vcStoryboard) as? CustomeNC)!
//            vc.titleText = drawerObj.name
//        }
//
//        self.checkLegalConsultation(obj: drawerObj)
//        self.checkMedicalConsultation(obj: drawerObj)
//        self.checkAcademy(obj: drawerObj)
//
//        if drawerObj.vcStoryboard != "" {
//            drawerController?.mainViewController = vc
//            drawerController?.setDrawerState(KYDrawerController.DrawerState.closed, animated: true)
//        }
    }
    
    func checkAcademy (obj : DrawerMenuObject) {
//        if obj.name == "Бібліотека" {
//            ArticleState.sharedInstance.article = 5
//        }
//        else if obj.name == "Відеокейси" {
//            ArticleState.sharedInstance.article = 6
//        }
        if obj.name == "Консиліум" {
            ArticleState.sharedInstance.article = 7
        } else if obj.name == "Консиліум академія" {
            ArticleState.sharedInstance.article = 0
        }
    }
    
    func checkLegalConsultation (obj : DrawerMenuObject) {
        if obj.name == "Популярні запитання" {
            LegalState.sharedInstance.isAll = false
        } else if obj.name == "Усі запитання" {
            LegalState.sharedInstance.isAll = true
        }
    }
    
    func checkMedicalConsultation (obj : DrawerMenuObject) {
        if obj.name == "Запитання щодо гіпертонічної хвороби" {
            MedicalState.sharedInstance.isEs()
        } else if obj.name == "Вопросы по симптоматической АГ" {
            MedicalState.sharedInstance.isSimptAg()
        } else if obj.name == "Запитання щодо порушень ритму серця" {
            MedicalState.sharedInstance.isHeart()
        } else if obj.name == "Вопросы по СД 2 типа" {
            MedicalState.sharedInstance.isTwoType()
        } else if obj.name == "Консиліум щодо складних клінічних випадків" {
            MedicalState.sharedInstance.isDifficultСases()
        } else if obj.name == "Другие вопросы" {
            MedicalState.sharedInstance.isOtherIssues()
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let contentString = self.menuNames[indexPath.row].name
        
        if !self.isULConsOpened {
            if contentString == "Популярні запитання" ||
                contentString == "Усі запитання" {
                return 0
            }
        } else {
            if contentString == "Популярні запитання" ||
                contentString == "Усі запитання" {
                return UITableViewAutomaticDimension
            }
        }
        
        if !self.isMedConsOpened {
            if contentString == "Запитання щодо гіпертонічної хвороби" ||
                contentString == "Вопросы по симптоматической АГ" ||
                contentString == "Запитання щодо порушень ритму серця" ||
                contentString == "Вопросы по СД 2 типа" ||
                contentString == "Консиліум щодо складних клінічних випадків" ||
                contentString == "Інші питання" {
                return 0
            }
        } else {
            if contentString == "Запитання щодо гіпертонічної хвороби" ||
                contentString == "Вопросы по симптоматической АГ" ||
                contentString == "Запитання щодо порушень ритму серця" ||
                contentString == "Вопросы по СД 2 типа" ||
                contentString == "Консиліум щодо складних клінічних випадків" ||
                contentString == "Інші питання" {
                return UITableViewAutomaticDimension
            }
        }

        if !self.isCalcOpened {
            if contentString == "Індекс маси тіла" ||
                contentString == "Ризик ССЗ за шкалою SCORE" ||
                contentString == "Швидкість клубочкової фільтрації" ||
                contentString == "Клиренс креатинина" ||
                contentString == "Дозування Укрлів® табл." {
                return 0
            }
        } else {
            if contentString == "Індекс маси тіла" ||
                contentString == "Ризик ССЗ за шкалою SCORE" ||
                contentString == "Швидкість клубочкової фільтрації" ||
                contentString == "Клиренс креатинина" ||
                contentString == "Дозування Укрлів® табл." {
                return UITableViewAutomaticDimension
            }
        }
        
        if contentString == "Достижения" {
            return 0
        } else {
            return UITableViewAutomaticDimension
        }
    }
    
//    if obj.name == "Запитання щодо гіпертонічної хвороби" {
//        MedicalState.sharedInstance.isEs()
//    } else if obj.name == "Вопросы по симптоматической АГ" {
//        MedicalState.sharedInstance.isSimptAg()
//    } else if obj.name == "Запитання щодо порушень ритму серця" {
//        MedicalState.sharedInstance.isHeart()
//    } else if obj.name == "Вопросы по СД 2 типа" {
//        MedicalState.sharedInstance.isTwoType()
//    } else if obj.name == "Консиліум щодо складних клінічних випадків" {
//        MedicalState.sharedInstance.isDifficultСases()
//    } else if obj.name == "Другие вопросы" {
//        MedicalState.sharedInstance.isOtherIssues()
//    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuNames.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "MenuCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! MenuCell
        cell.name?.text = menuNames[indexPath.row].name
        cell.countView?.isHidden = true
        
        if validTexts.contains(cell.name.text ?? "") {
            cell.name.text = String(format: "- %@", cell.name.text!)
            cell.textConstraint.constant = 35
        } else {
            cell.textConstraint.constant = 20
        }

        cell.selectionStyle = .none
        return cell
    }
}
