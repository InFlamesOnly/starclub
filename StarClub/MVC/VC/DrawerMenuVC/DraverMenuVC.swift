//
//  DraverMenuVC.swift
//  StarClub
//
//  Created by macOS on 12.12.17.
//  Copyright © 2017 macOS. All rights reserved.
//

import UIKit
import KYDrawerController


class DraverMenuVC: LightViewController {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    
    var drawerMenu: DrawerMenuTVC?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let user = User().currentUser()
        nameLabel.text = user.name
        
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    @IBAction func closeDrawer(_ sender: Any) {
        let drawerController = navigationController?.parent as? KYDrawerController
        drawerController?.setDrawerState(KYDrawerController.DrawerState.closed, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "drawerView" {
            if let vc = segue.destination as? DrawerMenuTVC {
                self.drawerMenu = vc
            }
        }
    }
    
    //drawerView
}
