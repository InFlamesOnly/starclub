//
//  DrawerMenuObject.swift
//  StarClub
//
//  Created by Dima on 26.04.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import Foundation

class DrawerMenuObject {
    var name = ""
    var vcStoryboard = ""
    
    convenience init (name : String, vcStoryboard : String) {
        self.init()
        self.vcStoryboard = vcStoryboard
        self.name = name
    }
    //протоколы лечения
    
    //Кусум допомагає
    
    func mainKusumHelper () -> DrawerMenuObject {
        return DrawerMenuObject(name: "Консиліум допомагає", vcStoryboard: "MainKusumHelperVC")
    }

    func treatmentProtocols () -> DrawerMenuObject {
        return DrawerMenuObject(name: "Протоколи лікування", vcStoryboard: "TreatmentProtocolsVC")
    }
    
    func patientCart () -> DrawerMenuObject {
        return DrawerMenuObject(name: "Карточка пациента", vcStoryboard: "PatientVC")
    }
    
    func feed () -> DrawerMenuObject {
        return DrawerMenuObject(name: "Стрічка", vcStoryboard: "FeedVC")
    }
    
    func achivment () -> DrawerMenuObject {
        return DrawerMenuObject(name: "Достижения", vcStoryboard: "AchivmentVC")
    }
    
    func articles () -> DrawerMenuObject {
        return DrawerMenuObject(name: "Статті", vcStoryboard: "ArticlesVC")
    }
    
    func drugs () -> DrawerMenuObject {
        return DrawerMenuObject(name: "Консиліум препарати", vcStoryboard: "DrugsVC")
    }
    
    func conference () -> DrawerMenuObject {
        return DrawerMenuObject(name: "Конференції", vcStoryboard: "ConferencVC")
    }
    
    func legalConsultation () -> DrawerMenuObject {
        return DrawerMenuObject(name: "Юридична консультація", vcStoryboard: "")
    }
    
    func popularLegalConsultation () -> DrawerMenuObject {
        return DrawerMenuObject(name: "Популярні запитання", vcStoryboard: "LegalConsultationVC")
    }
    
    func allLegalConsultation () -> DrawerMenuObject {
        return DrawerMenuObject(name: "Усі запитання", vcStoryboard: "LegalConsultationVC")
    }
    
    func calculators () -> DrawerMenuObject {
        return DrawerMenuObject(name: "Калькулятори", vcStoryboard: "")
    }
    
    func imt () -> DrawerMenuObject {
        return DrawerMenuObject(name: "Індекс маси тіла", vcStoryboard: "CalculatorVC")
    }
    
    func score () -> DrawerMenuObject {
        return DrawerMenuObject(name: "Ризик ССЗ за шкалою SCORE", vcStoryboard: "CalculatorVCSCORE")
    }
    
    func skf () -> DrawerMenuObject {
        return DrawerMenuObject(name: "Швидкість клубочкової фільтрації", vcStoryboard: "CalculatorSKF")
    }
    
    func ukrliv () -> DrawerMenuObject {
        return DrawerMenuObject(name: "Дозування Укрлів® табл.", vcStoryboard: "CalculatorUKRLIV")
    }
    
    func support () -> DrawerMenuObject {
        return DrawerMenuObject(name: "Підтримка", vcStoryboard: "SupportVC")
    }
    
    func settings () -> DrawerMenuObject {
        return DrawerMenuObject(name: "Налаштування", vcStoryboard: "SettingVC")
    }
    
    func esAg () -> DrawerMenuObject {
        return DrawerMenuObject(name: "Запитання щодо гіпертонічної хвороби", vcStoryboard: "MedicalConsultationVC")
    }
    
    func medCons () -> DrawerMenuObject {
        return DrawerMenuObject(name: "Медична консультація", vcStoryboard: "")
    }
    
    func simptAg () -> DrawerMenuObject {
        return DrawerMenuObject(name: "Вопросы по симптоматической АГ", vcStoryboard: "MedicalConsultationVC")
    }
    
    func heart () -> DrawerMenuObject {
        return DrawerMenuObject(name: "Запитання щодо порушень ритму серця", vcStoryboard: "MedicalConsultationVC")
    }
    
    func twoType () -> DrawerMenuObject {
        return DrawerMenuObject(name: "Вопросы по СД 2 типа", vcStoryboard: "MedicalConsultationVC")
    }
    
    func difficultСases () -> DrawerMenuObject {
        return DrawerMenuObject(name: "Консиліум щодо складних клінічних випадків", vcStoryboard: "MedicalConsultationVC")
    }
    
    func otherIssues () -> DrawerMenuObject {
        return DrawerMenuObject(name: "Інші питання", vcStoryboard: "MedicalConsultationVC")
    }
    
    func academy () -> DrawerMenuObject {
        return DrawerMenuObject(name: "Консиліум академія", vcStoryboard: "ArticlesAcademyVC")
    }
    
    func library () -> DrawerMenuObject {
        return DrawerMenuObject(name: "Бібліотека", vcStoryboard: "ArticlesAcademyVC")
    }
    
    func сonsilium () -> DrawerMenuObject {
        return DrawerMenuObject(name: "Консиліум", vcStoryboard: "ArticlesAcademyVC")
    }
    
    func videoCases () -> DrawerMenuObject {
        return DrawerMenuObject(name: "Відеокейси", vcStoryboard: "VideoCasesVC")
    }
    
    func calendar () -> DrawerMenuObject {
        return DrawerMenuObject(name: "Календар заходів", vcStoryboard: "CalendarVC")
    }
    //ICPC -2 Классификация ПМД ISPS2
    func ISPS2 () -> DrawerMenuObject {
        return DrawerMenuObject(name: "ICPC-2 Класифікація ПМД", vcStoryboard: "ISPS2VC")
    }
    
    func questionnaire () -> DrawerMenuObject {
        return DrawerMenuObject(name: "Опитування", vcStoryboard: "QuestionnaireVC")
    }
    
    func forum () -> DrawerMenuObject {
        return DrawerMenuObject(name: "Форум", vcStoryboard: "ForumVC")
    }
    
    
    //QuestionnaireVC
}
