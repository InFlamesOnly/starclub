//
//  AchivmentVC.swift
//  StarClub
//
//  Created by macOS on 22.12.17.
//  Copyright © 2017 macOS. All rights reserved.
//

import UIKit
import AlamofireImage

class AchivmentVC: LightViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var achivmentTableView : UITableView!
    var achivmentArray : Array<Achivment> = Array()
    var achivment : Achivment!
    let indicator = SpringIndicator()
    let user = User().currentUser()

    override func viewDidLoad() {
        super.viewDidLoad()
        achivmentTableView.isHidden = true
        self.getAchivmentFromServer()
    }
    
    func createIndicator () {
        indicator.frame.size = CGSize(width: 100, height: 100)
        indicator.center = self.view.center
        indicator.lineColor = PINK
        self.view.addSubview(indicator)
        indicator.start()
    }
    
    func getAchivmentFromServer() {
        self.createIndicator()
        RequsetManager.sharedInstance.getAchivmentsFromServer(success: { (_ responseObject : Dictionary <String,Any>) in
            let ach = Achivment()
            ach.getAchivmentFromServerResponse(response: responseObject)
            self.achivment = ach
            self.achivmentTableView.isHidden = false
            self.achivmentTableView.reloadData()
            self.indicator.removeFromSuperview()
        }) { (_ error : Int?) in
            self.indicator.removeFromSuperview()
            RequsetManager.sharedInstance.showError(viewController: self, code: error!)
        }
    }
    
    func calculateProgress (max : Int, current : Int) -> Double {
        var progress = Double()
        if max != 0 || current != 0 {
             progress = Double(current)/Double(max)
        } else {
            progress = 0
        }
        return progress
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (achivment != nil) {
            return achivment.mounth.count + 1
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cellIdentifier = "AchivmentC"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! AchivmentC
            cell.progressView.animateTo(progress: CGFloat(self.calculateProgress(max: achivment.mounthMax, current: achivment.mounthSum)))
            cell.progressLabel.text = "Шкала достижения"
//            cell.progressLabel.text = String(format:"Шкала достижения за %@",achivment.currentMounthName)
            if user.specific == 2 {
                cell.progressTittle.text = "Всего фонариков собрано"
            } else if user.specific == 3 || user.specific == 6 {
                cell.progressTittle.text = "Всего красочных штрихов собрано"
            } else {
                cell.progressTittle.text = "Всего пицц собрано"
            }
            cell.countLabel.text = String(achivment.sum)
            cell.flowerImage.af_setImage(withURL: URL(string: achivment.imageURL)!, placeholderImage: nil)
            cell.flowerImage.contentMode = .scaleAspectFill
            //sum ()
            cell.selectionStyle = .none
            return cell
        } else {
            let mounth = achivment.mounth[indexPath.row - 1]
            let cellIdentifier = "MounthC"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! MounthC
            cell.mountName.text = mounth.name
            cell.mountValue.text = String(mounth.bonus)
            if mounth.time == 0 {
               cell.mountBackround.backgroundColor = LIGHTGREEN
                cell.mountValue.isHidden = false
            }
            if mounth.time == 2 {
                cell.mountBackround.backgroundColor = LIGHTPINK
                cell.mountValue.isHidden = true
            }
            cell.selectionStyle = .none
            return cell
        }
    }

}
