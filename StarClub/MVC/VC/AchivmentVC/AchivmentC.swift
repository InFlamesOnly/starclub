//
//  AchivmentC.swift
//  StarClub
//
//  Created by macOS on 22.12.17.
//  Copyright © 2017 macOS. All rights reserved.
//

import UIKit
import GTProgressBar

class AchivmentC: UITableViewCell {
    
    @IBOutlet weak var flowerImage : UIImageView!
    @IBOutlet weak var countBackground : UIView!
    @IBOutlet weak var countLabel : UILabel!
    @IBOutlet weak var progressLabel : UILabel!
    @IBOutlet weak var progressTittle : UILabel!
    @IBOutlet weak var progressView : GTProgressBar!

    override func awakeFromNib() {
        super.awakeFromNib()
        progressView.displayLabel = false
        progressView.layer.cornerRadius = progressView.frame.size.height/2
        countBackground.layer.cornerRadius = countBackground.frame.size.height/2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
