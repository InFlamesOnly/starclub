//
//  MounthC.swift
//  StarClub
//
//  Created by macOS on 22.12.17.
//  Copyright © 2017 macOS. All rights reserved.
//

import UIKit

class MounthC: UITableViewCell {
    
    @IBOutlet weak var mountBackround : UIView!
    @IBOutlet weak var mountName : UILabel!
    @IBOutlet weak var mountValue : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        mountBackround.layer.cornerRadius = mountBackround.frame.size.height/2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
