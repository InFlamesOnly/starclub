//
//  LegalConsultationVC.swift
//  StarClub
//
//  Created by macOS on 20.02.18.
//  Copyright © 2018 macOS. All rights reserved.
//

import UIKit

class LegalConsultationVC: LightViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    @IBOutlet weak var questionTableView: UITableView!
    @IBOutlet weak var searchTextField: UITextFieldDelegate!
    let indicator = SpringIndicator()
    var isNewQuestion = true
    var selectedId = 0
    
    var search:String = ""
    
    var legalConsultationArray : Array<LegalConsultation> = Array()
    var searchArray : Array<LegalConsultation> = Array()

    override func viewDidLoad() {
        super.viewDidLoad()
        let ulState = LegalState.sharedInstance
        if ulState.isAll {
            self.getListFromServer()
            print("all")
        } else {
            self.getMyListFromServer()
            print("not all")
        }
        self.hideKeyboardWhenTappedAround()
        self.questionTableView.isHidden = true

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super .viewWillDisappear(animated)
        self.hideBurger()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showBurger()
    }
    
    func hideBurger () {
        if self.navigationController is CustomeNC {
            let nc = self.navigationController as! CustomeNC
            nc.hideBurger()
        }
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func showBurger () {
        if self.navigationController is CustomeNC {
            let nc = self.navigationController as! CustomeNC
            nc.showBurger()
        }
    }
    
    func createIndicator () {
        indicator.frame.size = CGSize(width: 100, height: 100)
        indicator.center = self.view.center
        indicator.lineColor = PINK
        self.view.addSubview(indicator)
        indicator.start()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string.isEmpty {
            search = String(search.dropLast())
        } else {
            search = textField.text! + string
        }
        
        print(search)
        
        let filteredArray = self.legalConsultationArray.filter() { $0.text.contains(search) || $0.answer.contains(search)}
        
        if filteredArray.count > 0
        {
            searchArray.removeAll(keepingCapacity: true)
            searchArray = filteredArray
        }
        else
        {
            searchArray = self.legalConsultationArray
        }
        self.legalConsultationArray = searchArray
        self.questionTableView.reloadData()
        return true
    }
    
    func getListFromServer () {
        self.createIndicator()
        RequsetManager.sharedInstance.getLegalConsultation(success: { (_ responseObject: Dictionary<String, Any>) in
            self.legalConsultationArray = LegalConsultation.mapResponseToArrayObject(response: responseObject)
            self.questionTableView.isHidden = false
            self.questionTableView.reloadData()
            self.indicator.removeFromSuperview()
            print("\(self.legalConsultationArray)")
        }) { (_ error: Int?) in
            self.indicator.removeFromSuperview()
        }
    }
    
    func getMyListFromServer () {
        self.createIndicator()
        RequsetManager.sharedInstance.getMyLegalConsultation(success: { (_ responseObject: Dictionary<String, Any>) in
            self.legalConsultationArray = LegalConsultation.mapResponseToArrayObject(response: responseObject)
            self.questionTableView.isHidden = false
            self.questionTableView.reloadData()
            self.indicator.removeFromSuperview()
            print("\(self.legalConsultationArray)")
        }) { (_ error: Int?) in
            self.indicator.removeFromSuperview()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "isNewQestion" {
            let vc = segue.destination as! SendQuetionVC
            vc.isNewQestion = true
            
        }
        if segue.identifier == "showMore" {
            let vc = segue.destination as! SendQuetionVC
            vc.selectedId = self.selectedId
            vc.isNewQestion = false
        }
    }
    
    @objc func showMore (_ sender : UIButton) {
        let lc = self.legalConsultationArray[sender.tag]
        self.selectedId = lc.consId
        self.performSegue(withIdentifier: "showMore", sender: self)
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let lc = self.legalConsultationArray[indexPath.row]
        self.selectedId = lc.consId
        self.performSegue(withIdentifier: "showMore", sender: self)
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.legalConsultationArray.count
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return  1000
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "LegalConsultationC"
        let lc = self.legalConsultationArray[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! LegalConsultationC
        cell.quetion.text = lc.text
        //25 - image wight
        let attributedString = NSAttributedString(html: lc.answer)
        cell.answerTextView.attributedText = attributedString
        cell.answerTextView.font = UIFont(name: "AvenirNext-Medium", size: 11.0)
        cell.answer.text = cell.answerTextView.text
        cell.answerTextView.isScrollEnabled = false
        
        if lc.isPrivate {
            cell.quetionTittle.text = "Особисте питання (видно тільки вам)"
        } else {
            cell.quetionTittle.text = "Загальне запитання (видно всім)"
        }
        
        if lc.isAnswered {
            cell.answer.text = lc.answer
        } else {
            cell.answer.text = "Юрист готує відповідь. Дякуємо за очікування."
            cell.answerTextView.text = "Юрист готує відповідь. Дякуємо за очікування."
        }
        
        cell.moreButton.tag = indexPath.row
        cell.moreButton.addTarget(self,action:#selector(showMore), for: .touchUpInside)
        cell.selectionStyle = .none
        return cell
    }
}
