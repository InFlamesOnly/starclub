//
//  LightViewController.swift
//  StarClub
//
//  Created by macOS on 10/1/19.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit

class LightViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }
    }

}
