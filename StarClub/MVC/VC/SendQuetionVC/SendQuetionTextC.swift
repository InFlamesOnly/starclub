//
//  SendQuetionTextC.swift
//  StarClub
//
//  Created by macOS on 20.02.18.
//  Copyright © 2018 macOS. All rights reserved.
//

import UIKit

class SendQuetionTextC: UITableViewCell {
    
    @IBOutlet weak var sendQuestionTittle: UILabel!
    @IBOutlet weak var roundView: UIView!
    @IBOutlet weak var sendText: UITextView!
    @IBOutlet weak var sendButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        roundView.layer.cornerRadius = CELLBORDERRADIUS
        self.customisationButton()
    }
    
    func customisationButton () {
        sendButton.layer.borderColor = PINK.cgColor
        sendButton.layer.cornerRadius = 10
        sendButton.layer.borderWidth = 1
        sendButton.layer.cornerRadius = sendButton.frame.size.height/2
    }
}
