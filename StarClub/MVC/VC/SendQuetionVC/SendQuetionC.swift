//
//  SendQuetionC.swift
//  StarClub
//
//  Created by macOS on 20.02.18.
//  Copyright © 2018 macOS. All rights reserved.
//

import UIKit

class SendQuetionC: UITableViewCell {
    
    @IBOutlet weak var sendQuestionTittle: UILabel!
    @IBOutlet weak var quetion: UILabel!
    @IBOutlet weak var answer: UILabel!
    @IBOutlet weak var roundView: UIView!
    @IBOutlet weak var answerTextView: UITextView!


    override func awakeFromNib() {
        super.awakeFromNib()
        roundView.layer.cornerRadius = CELLBORDERRADIUS
    }
}
