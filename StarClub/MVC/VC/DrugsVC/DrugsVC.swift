//
//  DrugsVC.swift
//  StarClub
//
//  Created by macOS on 20.12.17.
//  Copyright © 2017 macOS. All rights reserved.
//

import UIKit
import RealmSwift
//import Realm
//import Realm

//class DrugsVC: LightViewController, UITableViewDelegate, UITableViewDataSource, UIWebViewDelegate {
//
//    @IBOutlet weak var drugsTableView: UITableView!
//    @IBOutlet weak var drugsWebView: UIWebView!
//    @IBOutlet weak var drugsBckView: UIView!
//
//    let user = User().currentUser()
//
//    var drugsArray : Array<Drug>!
//    let indicator = SpringIndicator()
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
////        if let drugView = Bundle.main.loadNibNamed("DrugView", owner: self, options: nil)?.first as? DrugView {
////            self.view.addSubview(drugView)
////            self.drugsWebView = drugView
////            drugView.drugInstructonWebView.delegate = self
////            drugView.drugInstructonWebView.scrollView.showsHorizontalScrollIndicator = false
////        }
//
//        let realm = try! Realm()
//        let drugsArray = realm.objects(Drug.self)
//        self.drugsArray = Array(drugsArray)
//        self.drugsTableView.isHidden = false
//        self.drugsBckView.isHidden = true
//    }
//
//    func createIndicator () {
//        indicator.frame.size = CGSize(width: 100, height: 100)
//        indicator.center = self.view.center
//        indicator.lineColor = PINK
//        self.view.addSubview(indicator)
//        indicator.start()
//    }
////     file:///var/containers/Bundle/Application/750CF79A-4ACE-428C-8EFE-04BC94335F10/StarClub.app/21.html
////    file:///var/containers/Bundle/Application/750CF79A-4ACE-428C-8EFE-04BC94335F10/StarClub.app/1.html
//
//    @objc func openWebView (_ sender : UIButton) {
////        self.drugsTableView.isHidden = true
//        self.drugsBckView.isHidden = false
//        if user.specific == 2 {
//            let localfilePath = Bundle.main.url(forResource: String(sender.tag + 221), withExtension: "html");
//            let myRequest = NSURLRequest(url: localfilePath!)
//            self.drugsWebView.loadRequest(myRequest as URLRequest)
//        } else if user.specific == 3 || user.specific == 6 {
//            let localfilePath = Bundle.main.url(forResource: String(sender.tag + 331), withExtension: "html");
//            let myRequest = NSURLRequest(url: localfilePath!)
//            self.drugsWebView.loadRequest(myRequest as URLRequest)
//        } else {
//            if user.specific == 5 {
//                let localfilePath = Bundle.main.url(forResource: String(sender.tag + 1), withExtension: "html");
//                let myRequest = NSURLRequest(url: localfilePath!)
//                self.drugsWebView.loadRequest(myRequest as URLRequest)
//            } else {
//                let localfilePath = Bundle.main.url(forResource: String(sender.tag + 1), withExtension: "html");
//                let myRequest = NSURLRequest(url: localfilePath!)
//                self.drugsWebView.loadRequest(myRequest as URLRequest)
//            }
//
//        }
//    }
//
//    @IBAction func close(_ sender: Any) {
//        self.drugsWebView.loadRequest(URLRequest.init(url: URL.init(string: "about:blank")!))
//        self.drugsBckView.isHidden = true
//
//    }
//
//    // MARK: - WebViewDelegate
//    func webViewDidFinishLoad(_ webView: UIWebView) {
//        webView.scrollView.contentSize = CGSize(width: webView.frame.size.width, height: webView.scrollView.contentSize.height)
//        self.indicator.removeFromSuperview()
//    }
//
//    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
//
//        return false
//    }
//
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        if (scrollView.contentOffset.x > 0) {
//            scrollView.contentOffset = CGPoint(x: 0, y: scrollView.contentOffset.y)
//        }
//    }
//
//    // MARK: - Table view data source
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return drugsArray.count
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cellIdentifier = "DrugsC"
//        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! DrugsC
//        let drug = drugsArray[indexPath.row]
//        cell.moreButton.tag = indexPath.row
//        cell.moreButton.addTarget(self,action:#selector(openWebView), for: .touchUpInside)
//        cell.pharmacyName.text = drug.name
//        cell.flowerImage.image = UIImage(named : drug.flowerImage)
//        cell.pharmacyImage.image = UIImage(named : drug.drugsImage)
//        if self.user.specific == 5 {
//            cell.flowerImage.isHidden = true
//            cell.flowerBackgroundView.isHidden = true
//        }
//        cell.selectionStyle = .none
//        return cell
//    }
//}

class DrugsVC: UIViewController {
    
    @IBOutlet weak var webView : UIWebView!
    let indicator = SpringIndicator()
    let user = User().currentUser()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.webView.isHidden = true
        self.createIndicator()
        let urlString = "https://kusumdoctors.co.ua/preparati/?utm_source=KD_preparat&token=\(user.token)&specialuty=\(user.specific)"
        self.webView.loadRequest(URLRequest(url: URL(string: urlString)!))
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        if self.navigationController is CustomeNC {
            let nc = self.navigationController as! CustomeNC
            nc.ncDelegate = self
            nc.updateButton.isHidden = false
            nc.backButton.isHidden = nc.updateButton.isHidden
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super .viewWillDisappear(animated)
        if self.navigationController is CustomeNC {
            let nc = self.navigationController as! CustomeNC
            nc.ncDelegate = self
            nc.updateButton.isHidden = true
            nc.backButton.isHidden = nc.updateButton.isHidden
        }
    }
    
    func createIndicator () {
        indicator.frame.size = CGSize(width: 100, height: 100)
        indicator.center = self.view.center
        indicator.lineColor = PINK
        self.view.addSubview(indicator)
        indicator.start()
    }
}

extension DrugsVC : CustomeNCDelegate {
    func update() {
        self.webView.isHidden = true
        self.createIndicator()
        self.webView.reload()
    }
    
    func back () {
        self.webView.goBack()
    }
}

extension DrugsVC : UIWebViewDelegate {
    func webViewDidFinishLoad(_ webView: UIWebView) {
            self.webView.isHidden = false
            self.indicator.removeFromSuperview()
    }
}
