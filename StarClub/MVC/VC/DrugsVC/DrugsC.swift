//
//  DrugsC.swift
//  StarClub
//
//  Created by macOS on 20.12.17.
//  Copyright © 2017 macOS. All rights reserved.
//

import UIKit

class DrugsC: UITableViewCell {

    @IBOutlet weak var flowerImage: UIImageView!
    @IBOutlet weak var flowerBackgroundView: UIView!
    @IBOutlet weak var roundBackgrounView: UIView!
    @IBOutlet weak var pharmacyImage: UIImageView!
    @IBOutlet weak var pharmacyName: UILabel!
    @IBOutlet weak var moreButton: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        roundBackgrounView.layer.cornerRadius = CELLBORDERRADIUS
        flowerBackgroundView.layer.cornerRadius = flowerBackgroundView.frame.size.height / 2
    }
}
