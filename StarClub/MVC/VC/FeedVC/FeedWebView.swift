//
//  FeedWebView.swift
//  StarClub
//
//  Created by macOS on 11/14/20.
//  Copyright © 2020 macOS. All rights reserved.
//

import Foundation
import UIKit

class FeedWebView: UIViewController {
    
    @IBOutlet weak var webView : UIWebView!
    let indicator = SpringIndicator()
    let user = User().currentUser()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.webView.isHidden = true
        self.createIndicator()
        let urlString = "http://kusumdoctors.co.ua/category/lenta/?utm_source=KD_stritchka&token=\(user.token)&speciality=\(user.specific)"
        self.webView.loadRequest(URLRequest(url: URL(string: urlString)!))
        self.navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    func createIndicator () {
        indicator.frame.size = CGSize(width: 100, height: 100)
        indicator.center = self.view.center
        indicator.lineColor = PINK
        self.view.addSubview(indicator)
        indicator.start()
    }
}

extension FeedWebView : UIWebViewDelegate {
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.webView.isHidden = false
        self.indicator.removeFromSuperview()
    }
}
