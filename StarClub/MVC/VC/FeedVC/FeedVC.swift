//
//  FeedVC.swift
//  StarClub
//
//  Created by macOS on 21.12.17.
//  Copyright © 2017 macOS. All rights reserved.
//

import UIKit
import AlamofireImage
import KYDrawerController
import Toast_Swift


class FeedVC: LightViewController, UITableViewDelegate, UITableViewDataSource, FeedCDelegate {
     
    @IBOutlet weak var feedTableView: UITableView!
    @IBOutlet weak var errorLabel: UILabel!
    var articleArray : Array<Article> = Array()
    var testArray : Array<Test> = Array()
    let indicator = SpringIndicator()
    let user = User().currentUser()

    override func viewDidLoad() {
        super.viewDidLoad()
        feedTableView.isHidden = true
        errorLabel.isHidden = true
        self.getListFromServer()
        feedTableView.rowHeight = UITableViewAutomaticDimension
        feedTableView.estimatedRowHeight = 10000
        feedTableView.showsHorizontalScrollIndicator = false
        feedTableView.showsVerticalScrollIndicator = false
    }
    
    func addHoliday () {
        if let hv = Bundle.main.loadNibNamed("HolidayView", owner: self, options: nil)?.first as? HolidayView {
            hv.frame = CGRect(x: 0, y: 64, width: self.view.frame.size.width, height: self.view.frame.size.height - 64)
            self.view.addSubview(hv)
        }
    }
    
    func checkHoliday () {
        let defaults = UserDefaults.standard
        if defaults.bool(forKey: "holiday") == true {
            self.addHoliday()
            defaults.set(false, forKey: "holiday")
            defaults.synchronize()
        }
    }
    
    func createIndicator () {
        indicator.frame.size = CGSize(width: 100, height: 100)
        indicator.center = self.view.center
        indicator.lineColor = PINK
        self.view.addSubview(indicator)
        indicator.start()
    }
    
    func getListFromServer () {
        self.createIndicator()
        RequsetManager.sharedInstance.getList(success: { (_ responseObject: Dictionary<String, Any>) in
            self.checkHoliday()
            self.articleArray = Article.mapResponseToArrayObject(response: responseObject)
            self.testArray = Test.mapResponseToArrayObject(response: responseObject)
            if self .articleArray.count > 0 {
                self.feedTableView.isHidden = false
                self.errorLabel.isHidden = true
            } else {
                self.feedTableView.isHidden = true
                self.errorLabel.isHidden = false
            }
            self.feedTableView.reloadData()
            self.indicator.removeFromSuperview()
        }) { (_ error: Int?) in
            self.indicator.removeFromSuperview()
            RequsetManager.sharedInstance.showError(viewController: self, code: error!)
        }
    }
    
    // MARK: - FeedCDelegate
    func tapToOpros(cell: Int, article: Article, answer : String) {
        let visibleCell = feedTableView.visibleCells.first as! FeedC
        let indexPath = feedTableView.indexPath(for: visibleCell)
        visibleCell.showTrueAnswer(answer: answer)
        self.feedTableView.reloadRows(at: [indexPath!], with: .none)
        self.feedTableView.isUserInteractionEnabled = false
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.feedTableView.isUserInteractionEnabled = true
            print(answer)
            RequsetManager.sharedInstance.writeAnswerWithTextFromServer(text: answer, articleId: article.articleId, bonus: article.bonus, success: { (_ responseObject : Dictionary <String, Any>) in
                self.feedTableView.beginUpdates()
                self.articleArray.remove(at: (indexPath?.row)!)
                self.testArray.remove(at: (indexPath?.row)!)
                self.feedTableView.deleteRows(at: [indexPath!], with: .fade)
                self.feedTableView.endUpdates()
                let bonus = responseObject["bonuses"] as! Int
                let newUser = User()
                let user = User().currentUser()
                newUser.achievement = bonus
                newUser.firebaseToken = user.firebaseToken
                newUser.token = user.token
                newUser.name = user.name
                newUser.push = user.push
                newUser.specific = user.specific
                newUser.updateUser(newUser: newUser)
                let vc = self.navigationController as! CustomeNC
                vc.updateLebel()
                self.checkArrayToEmtpy()
                self.view.makeToast("Спасибо за Ваш ответ")
            }, failure: { (_ error : Int?) in
                
            })
        }
    }
    
    func tapToTrueAnswer(cell: Int) {
        let visibleCell = feedTableView.visibleCells.first as! FeedC
        let indexPath = feedTableView.indexPath(for: visibleCell)
        self.feedTableView.reloadRows(at: [indexPath!], with: .none)
        visibleCell.userSetTrueAnswer(success: { (_ responseObject : Dictionary <String, Any>) in
            self.feedTableView.beginUpdates()
            self.articleArray.remove(at: (indexPath?.row)!)
            self.testArray.remove(at: (indexPath?.row)!)
            self.feedTableView.deleteRows(at: [indexPath!], with: .fade)
            self.feedTableView.endUpdates()
            let bonus = responseObject["bonuses"] as! Int
            let newUser = User()
            let user = User().currentUser()
            newUser.achievement = bonus
            newUser.firebaseToken = user.firebaseToken
            newUser.token = user.token
            newUser.name = user.name
            newUser.push = user.push
            newUser.specific = user.specific
            newUser.updateUser(newUser: newUser)
            let vc = self.navigationController as! CustomeNC
            vc.updateLebel()
            self.checkArrayToEmtpy()
            self.view.makeToast("Вы выбрали правильный ответ")
        }) { (_ error : Int?) in
            
        }
    }
    
    func allert () {
        let alert = UIAlertController(title: "Ошибка", message: "Ответ должен иметь не менее 3х символов", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in

        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func sendAnswerText(article: Article) {
        let visibleCell = feedTableView.visibleCells.first as! FeedC
        let answerCell = visibleCell.answersTableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! FeedQuestionWithAnswerC
        let indexPath = feedTableView.indexPath(for: visibleCell)
        self.feedTableView.reloadRows(at: [indexPath!], with: .none)
        
        if answerCell.answerTextField.text!.count < 3 {
            self.allert()
        } else {
            visibleCell.userSetTextAnswer(text: answerCell.answerTextField.text ?? "", success: { response in
                
                self.feedTableView.beginUpdates()
                self.articleArray.remove(at: (indexPath?.row)!)
                self.testArray.remove(at: (indexPath?.row)!)
                self.feedTableView.deleteRows(at: [indexPath!], with: .fade)
                self.feedTableView.endUpdates()
                let bonus = response["bonuses"] as! Int
                let newUser = User()
                let user = User().currentUser()
                newUser.achievement = bonus
                newUser.firebaseToken = user.firebaseToken
                newUser.token = user.token
                newUser.name = user.name
                newUser.push = user.push
                newUser.specific = user.specific
                newUser.updateUser(newUser: newUser)
                let vc = self.navigationController as! CustomeNC
                //            vc.hideOrShowAchivments()
                vc.updateLebel()
                self.checkArrayToEmtpy()
                
                self.view.makeToast("Спасибо за Ваш ответ")
                
            }) { error in
                
            }
        }
    }
    
    func checkArrayToEmtpy () {
        if articleArray.isEmpty {
            self.feedTableView.isHidden = true
            self.errorLabel.isHidden = false
        }
    }
    
    func tapToFalseAnswer(cell: Int) {
        let visibleCell = feedTableView.visibleCells.first as! FeedC
        let indexPath = feedTableView.indexPath(for: visibleCell)
        let answIndexPath = IndexPath(row: cell - 2, section: 0)
        visibleCell.answersTableView.reloadRows(at: [answIndexPath], with: .none)
        feedTableView.reloadRows(at: [indexPath!], with: .none)
    }

    // MARK: - Table view data source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articleArray.count
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return  1000
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //DrugGastroImage5
        let article = articleArray[indexPath.row]
        let cellIdentifier = "FeedC"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! FeedC
        //
//        article.isAnsweredCell = true
        if article.isAnsweredCell {
            cell.isShowAnswerTextView = true
            cell.isNeedShowAnswer = false
        } else {
            cell.isShowAnswerTextView = false
        }
        let test = testArray[indexPath.row]
//        cell.feedImageView.af_setImage(withURL: URL(string: article.image)!)
        cell.feedImageView.af_setImage(withURL: URL(string: article.image)!, placeholderImage: nil)
        cell.feedImageView.contentMode = .scaleAspectFit
        cell.article = article
        cell.answersTableView.tag = indexPath.row
        let font = UIFont(name: "AvenirNext-Medium", size: 15.0)
        let attributedString = NSAttributedString(html: article.content)
        cell.feedTittle.text = article.title
        
        cell.feedTextView.attributedText = attributedString
        cell.feedTextView.font = font
        cell.feedText.text = cell.feedTextView.text
        cell.feedTextView.isScrollEnabled = false

        if test.title == ""  {
            cell.heightAnswersViewConstraint.constant = 0
            cell.isNeedShowAnswer = false
            return cell
        }
        
        if user.specific == 2 {
            cell.prizeImageView.image = UIImage(named: "DrugGastroImage5")
        }  else if user.specific == 3 || user.specific == 6 {
            cell.prizeImageView.image = UIImage(named: "Cardio")
        } else {
            cell.prizeImageView.image = UIImage(named: "Pizza2") //Flower3
        }
    
        cell.test = test
        cell.delegate = self
        cell.tappedCell = indexPath.row
        cell.prizeLabel.text = String(format:"%@ %@ за ответ",String(article.bonus),article.written)
        cell.prizeCountLabel.text = String(article.bonus)

        let height = CGFloat().heightForView(text: cell.feedTittle.text!, font: font!, width: cell.roundView.frame.size.width - 34)
        cell.heightConstraint.constant = height + 20
        cell.selectionStyle = .none
        var answerHeight = 0 as CGFloat
        cell.isNeedShowAnswer = false
        for text in test.answers {
            let answer = text
            let height = CGFloat().heightForView(text: answer.value, font: font!, width: cell.roundView.frame.size.width)
            answerHeight = answerHeight + height + 30
            if text.isTrue {
                if article.isAnsweredCell {
                    cell.isShowAnswerTextView = true
                    cell.isNeedShowAnswer = false
                } else {
                    cell.isNeedShowAnswer = true
                    cell.isShowAnswerTextView = false
                }
//                cell.isNeedShowAnswer = true
            }
        }
        let quetionHeight = CGFloat().heightForView(text: test.title, font: UIFont(name: "AvenirNext-Medium", size: 17.0)!, width: UIScreen.main.bounds.width - 50) + 40
        
        if !cell.isNeedShowAnswer {
            if cell.isShowAnswerTextView {
                cell.heightAnswersViewConstraint.constant = quetionHeight + 38
            } else {
                cell.heightAnswersViewConstraint.constant = quetionHeight - 12
            }
        } else {
            cell.heightAnswersViewConstraint.constant = quetionHeight + answerHeight
        }
        
        cell.answersTableView.reloadData()
        return cell
    }
}

