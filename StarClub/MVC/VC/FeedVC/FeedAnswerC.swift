//
//  FeedAnswerC.swift
//  StarClub
//
//  Created by macOS on 21.12.17.
//  Copyright © 2017 macOS. All rights reserved.
//

import UIKit

enum AnswerBorderStyle {
    case Black
    case Pink
    case Green
    
    var value: UIColor {
        switch self {
        case .Black: return BLACK
        case .Pink: return PINK
        case .Green: return GREEN
        }
    }
}

class FeedAnswerC: UITableViewCell {
    
    @IBOutlet weak var backgroundAnswer: UIView!
    @IBOutlet weak var answer: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundAnswer.layer.cornerRadius = 17
    }
}
