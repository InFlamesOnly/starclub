//
//  FeedC.swift
//  StarClub
//
//  Created by macOS on 21.12.17.
//  Copyright © 2017 macOS. All rights reserved.
//

import UIKit

protocol FeedCDelegate : NSObjectProtocol {
    func tapToTrueAnswer(cell : Int)
    func tapToFalseAnswer(cell : Int)
    func tapToOpros(cell : Int, article : Article, answer : String)
    func sendAnswerText(article : Article)
//    func showWebViewWithURL(url : URL)
}

class FeedC: UITableViewCell, UITableViewDataSource, UITableViewDelegate, UITextViewDelegate {
    
    @IBOutlet weak var roundView: UIView!
    @IBOutlet weak var feedTittle: UILabel!
    @IBOutlet weak var feedImageView: UIImageView!
    @IBOutlet weak var prizeImageView: UIImageView!
    @IBOutlet weak var feedText: UILabel!
    @IBOutlet weak var feedTextView: UITextView!
    @IBOutlet weak var answersTableView: UITableView!
    @IBOutlet weak var prizeLabel: UILabel!
    @IBOutlet weak var prizeCountLabel: UILabel!
    @IBOutlet weak var prizeCountView: UIView!
    @IBOutlet weak var heightAnswersViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightTextViewConstraint: NSLayoutConstraint!
    
    var tappedCell: Int!
    var test: Test!
    var article: Article!
    var user = User().currentUser()
    var delegate : FeedCDelegate?
    
    var savedAnswer = ""
    
    var isNeedShowAnswer: Bool!
    var isShowAnswerTextView: Bool!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        roundView.layer.cornerRadius = CELLBORDERRADIUS
        prizeCountView.layer.cornerRadius = prizeCountView.frame.size.height/2
//        self.countHeightQutionCell()
        answersTableView.rowHeight = UITableViewAutomaticDimension
        answersTableView.estimatedRowHeight = 1000
        answersTableView.isScrollEnabled = false
    }
    
    private func countHeightQutionCell (cell:FeedQuetionC) {
        let cell = answersTableView.dequeueReusableCell(withIdentifier: "FeedQuetionC", for: IndexPath(index: 0)) as! FeedQuetionC

        let labelWidth = cell.quetion.frame.width
        let maxLabelSize = CGSize(width: labelWidth, height: CGFloat.greatestFiniteMagnitude)
        let actualLabelSize = cell.quetion.text!.boundingRect(with: maxLabelSize, options: [.usesLineFragmentOrigin], attributes: [NSAttributedStringKey.font: cell.quetion.font], context: nil)
        let labelHeight = actualLabelSize.height
        let count = test.answers.count
        heightAnswersViewConstraint.constant = 56 * CGFloat(count) + labelHeight - 20
    }
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
//        self.delegate?.showWebViewWithURL(url : URL)
        return true
    }
    
    func showTrueAnswer (answer : String) {
        for obj in self.test.answers {
            if obj.value == answer {
                if obj.isTrue {
                    obj.color = 3
                    return
                } else {
                    obj.color = 2
                    for obj in self.test.answers {
                        if obj.isTrue {
                            obj.color = 3
                            return
                        }
                    }
                }
            } else {
                obj.color = 1
            }
        }
    }
    
    func returnStringHeight(font: UIFont, text: String) -> CGFloat {
        let fontAttributes = [NSAttributedStringKey.font: font]
        let size = (text as NSString).size(withAttributes: fontAttributes)
        let height = size.height
        return height
    }
    
    func userSetTrueAnswer (success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        RequsetManager.sharedInstance.writeAnswerFromServer(articleId: article.articleId, bonus: article.bonus, success: { (_ responseObject : Dictionary <String, Any>) in
            success (responseObject)
        }, failure: { (_ error : Int?) in
            failure(error)
        })
    }
    
    func userSetTextAnswer (text : String, success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        RequsetManager.sharedInstance.writeAnswerWithTextFromServer(text: text, articleId: article.articleId, bonus: article.bonus, success: { (_ responseObject : Dictionary <String, Any>) in
            success (responseObject)
        }, failure: { (_ error : Int?) in
            failure(error)
        })
    }
    
//    func webViewDidFinishLoad(_ webView: UIWebView) {
//        webView.scrollView.contentSize = CGSize(width: webView.frame.size.width, height: webView.scrollView.contentSize.height)
//        self.indicator.removeFromSuperview()
//    }
    
    // MARK: - Table view data source
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row > 0 {
            let answer = test.answers[indexPath.row - 1]
            if self.article.opros > 0 {
                self.savedAnswer = answer.value
                self.delegate?.tapToOpros(cell: self.tappedCell, article: self.article, answer : answer.value)
                return
            }
            if answer.isTrue {
                answer.color = 3
                self.delegate?.tapToTrueAnswer(cell: self.tappedCell)
            } else {
                answer.color = 2
                self.delegate?.tapToFalseAnswer(cell: self.tappedCell)
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.isShowAnswerTextView && !self.isNeedShowAnswer {
            return 1
        }
        if self.isNeedShowAnswer {
            return test.answers.count + 1
        }
        return 1
    }
    
    @objc func pressButton(_ button: UIButton) {
        self.delegate?.sendAnswerText(article: self.article)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            if self.isShowAnswerTextView {
                let cellIdentifier = "FeedQuestionWithAnswerC"
                let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! FeedQuestionWithAnswerC
                cell.sendButton.tag = indexPath.row
                cell.sendButton.addTarget(self, action: #selector(pressButton(_:)), for: .touchUpInside)
                cell.quetion.text = test.title
                cell.selectionStyle = .none
                return cell
            }
            let cellIdentifier = "FeedQuetionC"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! FeedQuetionC
            cell.quetion.text = test.title
            cell.selectionStyle = .none
            return cell
        } else {
            let answer = test.answers[indexPath.row - 1]
            let color = answer.color
            let cellIdentifier = "FeedAnswerC"
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! FeedAnswerC
            cell.answer.text = answer.value
            
            switch color {
            case 1 : cell.backgroundAnswer.layer.borderColor = AnswerBorderStyle.Black.value.cgColor
            case 2 : cell.backgroundAnswer.layer.borderColor = AnswerBorderStyle.Pink.value.cgColor
            case 3 : cell.backgroundAnswer.layer.borderColor = AnswerBorderStyle.Green.value.cgColor
            default:
                break
            }
            cell.backgroundAnswer.layer.borderWidth = 1
            cell.selectionStyle = .none
            return cell
        }
    }
}

extension String {
    func height(for width: CGFloat, font: UIFont) -> CGFloat {
        let maxSize = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
        let actualSize = self.boundingRect(with: maxSize, options: [.usesLineFragmentOrigin], attributes: [NSAttributedStringKey.font: font], context: nil)
        return actualSize.height
    }
}
