//
//  FeedQuestionWithAnswerC.swift
//  StarClub
//
//  Created by Hackintosh on 7/10/19.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit

class FeedQuestionWithAnswerC: UITableViewCell {
    
    @IBOutlet weak var quetion: UILabel!
    @IBOutlet weak var answerTextField: UITextField!
    @IBOutlet weak var sendButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
