//
//  VisitDetailVC.swift
//  StarClub
//
//  Created by Dima on 19.03.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit

class VisitDetailVC: LightViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

}

extension VisitDetailVC : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "showVisit", sender: self)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 118
        case 1:
            return 97
        case 2:
            return 82
        case 3:
            return 48
        case 4:
            return 97
        case 5:
            return 97
        case 6:
            return 97
        case 7:
            return 42
        case 8:
            return 119
        case 9:
            return 42
        case 10:
            return 42
        case 11:
            return 42
        default:
            return 0
        }
    }
}

extension VisitDetailVC : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 12
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "VisitDetailCell") as! VisitDetailCell
        if indexPath.row > 0 {
            cell = tableView.dequeueReusableCell(withIdentifier: "VisitDetailCell" + "\(indexPath.row)", for: indexPath) as! VisitDetailCell
        }
        cell.selectionStyle = .none
        return cell
    }
}
