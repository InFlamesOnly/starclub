//
//  SplashScreenVC.swift
//  StarClub
//
//  Created by macOS on 13.12.17.
//  Copyright © 2017 macOS. All rights reserved.
//

import UIKit
import KYDrawerController

class SplashScreenVC: LightViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        _ = Timer.scheduledTimer(withTimeInterval: 2.0, repeats: false) { (timer) in
            self.checkFirstScreen()
            self.checkVersion()
        }
    }
    
    func checkVersion () {
        _ = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: false) { (timer) in
            RequsetManager.sharedInstance.checkVersion(success: { (resp) in
                self.sendVersion()
                let version = resp["version"] as! Dictionary<String, Any>
                let ios : String = version["ios"] as! String
                let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
                if ios != appVersion {
//                    self.setAlertFromNeedUpdate()
                }
            }, failure: { errorCode in
                
            })
        }
    }
    
    func sendVersion () {
        RequsetManager.sharedInstance.sendAppVersion(success: { (resp) in
            
        }) { errorCode in
            
        }
    }
    
    func setAlertFromNeedUpdate () {
        if let rootViewController = UIApplication.topViewController() {
            let alert = UIAlertController(title: "Ошибка", message: "Вам необходимо обновить ваше приложение", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
//                if let url = URL(string: "itms-apps://itunes.apple.com/us/app/kusum-doctors/id1329955308?mt=8"),
//                    UIApplication.shared.canOpenURL(url){
//                    UIApplication.shared.openURL(url)
//                }
            }))
            
            rootViewController.present(alert, animated: true, completion: nil)
        }
    }
    
    private func checkFirstScreen () {
        let user = User().currentUser()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "KYDrawerController") as! KYDrawerController
        if user.token.isEmpty {
            self.performSegue(withIdentifier: "showStartScreen", sender: self)
        } else {
            
            if user.specific == 6 {
                controller.mainSegueIdentifier = "mainUR"
                startNavigationText = "Консультація"
            } else {
                controller.mainSegueIdentifier = "main"
                startNavigationText = "Стрічка"
            }
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}
