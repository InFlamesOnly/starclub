//
//  CalculatorVCCreatin.swift
//  StarClub
//
//  Created by macOS on 16.11.2018.
//  Copyright © 2018 macOS. All rights reserved.
//

import UIKit

class CalculatorVCCreatin: LightViewController {
    
    @IBOutlet weak var genderDropDown : DropDown!
    @IBOutlet weak var ageTextField : UITextField!
    @IBOutlet weak var weightTextField : UITextField!
    @IBOutlet weak var plazmaTextField : UITextField!
    
    @IBOutlet weak var rezultLabel : UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.rezultLabel.isHidden = true
        
        self.genderDropDown.optionArray = ["Чоловіча", "Жіноча"]
        
        self.genderDropDown.didSelect{(selectedText , index ,id) in
            self.genderDropDown.placeholder = selectedText
            self.genderDropDown.textAlignment = .center
        }
    }
    
    @IBAction func calculate(_ sender: Any) {
        var gender = 0
        if  genderDropDown.placeholder == "Жіноча" || genderDropDown.text == "Жіноча" {
            gender = 1
        }
        
        if self.ageTextField.text?.count == 0 && self.weightTextField.text?.count == 0 && self.plazmaTextField.text?.count == 0 {
            self.alert(message: "Заповніть усі поля")
            return
        }
        
        if self.ageTextField.text?.count == 0 {
            self.alert(message: "Вкажіть ваш вік")
            return
        }
        
        if self.weightTextField.text?.count == 0 {
            self.alert(message: "Вкажіть вашу вагу")
            return
        }
        
        if self.plazmaTextField.text?.count == 0 {
            self.alert(message: "Вкажіть креатинін плазми")
            return
        }
        
        let rezult = self.calculate(gender: gender, age: Int(self.ageTextField.text!)!, weight: Double(self.weightTextField.text!)!, plazma: Double(self.plazmaTextField.text!)!)
        
        self.rezultLabel.isHidden = false
        
        self.rezultLabel.text = self.textFromRezult(result: rezult)
    }
    
    func textFromRezult (result : Double)  -> String {
        var value = ""
        if result < 15 {
            value = "Результат : \(result.roundTo(places: 2)) Термінальна ниркова недостатність"
        }
        else if result > 15 && result < 29 {
            value = "Результат : \(result.roundTo(places: 2)) Виражене зниження СКФ"
        }
        else if result > 30 && result < 59 {
            value = "Результат : \(result.roundTo(places: 2)) Помірне зниження СКФ"
        }
        else if result > 60 && result < 89 {
            value = "Результат : \(result.roundTo(places: 2)) Ознаки пошкодження нирок з початковим зниженням СКФ"
        }
        else if result > 90 {
            value = "Результат : \(result.roundTo(places: 2)) Ознаки ушкодження нирок з нормальною або підвищеною СКФ"
        }
        return value
    }
    
    func calculate (gender : Int, age : Int, weight : Double, plazma : Double) -> Double {
        var rezult = 0.0
        if gender == 1 {
            //j
            rezult = (((140.0 - Double(age)) * weight)/(72*plazma)) * 0.85
        } else {
            //m
            rezult = ((140.0 - Double(age)) * weight)/(72*plazma)
        }
        
        return rezult
    }
}

