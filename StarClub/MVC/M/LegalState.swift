//
//  LegalState.swift
//  StarClub
//
//  Created by macOS on 20.06.2018.
//  Copyright © 2018 macOS. All rights reserved.
//

import UIKit

class LegalState: NSObject {

    var isAll = false
    
    static let sharedInstance: LegalState = {
        let instance = LegalState()
        return instance
    }()
    
}
