//
//  RequsetManager.swift
//  StarClub
//
//  Created by macOS on 17.12.17.
//  Copyright © 2017 macOS. All rights reserved.
//

import UIKit
import Alamofire
import RealmSwift
import FirebaseMessaging

private enum ErrorMessegess {
    case UserNotFound
    case PasswordIncorect
    case PushCodeIncorect
    case NoInternet
    case NoServer
    
    var text: String {
        switch self {
        case .NoInternet: return "Немає доступу до Інтернету"
        case .UserNotFound: return "Невірно введений логін"
        case .PasswordIncorect: return "Невірно введений пароль"
        case .PushCodeIncorect: return "Невірний код відновлення пароля"
        case .NoServer: return "Спробуйте пізніше, сервер не відповідає"
        }
    }
}
//k2.uds.kiev.ua
//private let kAPIServer = "https://k2.uds.kiev.ua/api/v3/"
private let kAPIServer = "http://kusumdoctors.co.ua/api/v3/"
private let headers = ["Content-Type": "application/x-www-form-urlencoded"]

//MARK: - API methods
private let kSendQestions = String(format:"%@send_question",kAPIServer)
private let kMedSendQestions = String(format:"%@send_question_med",kAPIServer)
private let kWriteAnswer = String(format:"%@write_answer",kAPIServer)
private let kAuth = String(format:"%@auth",kAPIServer)
private let kForgotPassword = String(format:"%@forgot_password",kAPIServer)
private let kAuthFirebaseToken = String(format:"%@auth_firebase_token",kAPIServer)
private let kChangePassword = String(format:"%@change_password",kAPIServer)
private let kChangePush = String(format:"%@settings",kAPIServer)
private let kCheckStatusToken = String(format:"%@check_token",kAPIServer)

private let kGetPatientList = String(format:"%@get_pacients",kAPIServer)
private let kGetVisitsList = String(format:"%@get_visit_lists",kAPIServer)
private let kSendPatient = String(format:"%@add_pacient",kAPIServer)

private let kCalendarDate = String(format:"%@get_all_calendar",kAPIServer)

private let kIsShowMenu = String(format:"%@show_med_menu",kAPIServer)

private let kAcademy = String(format:"%@get_all_academy",kAPIServer)

private let kPhotoUpload = "http://kusumdoctors.co.ua/upl.php"

private let kCheckVersion = "http://kusumdoctors.co.ua/version.json"
private let kDoctorActivity = "http://kusumdoctors.co.ua/api/v3/update_doctor"
private let kVersion = "http://kusumdoctors.co.ua/api/v3/send_version"

class RequsetManager: NSObject {
    
    var user = User().currentUser()
    // MARK: Local Variable
    //
    //    var emptyStringArray : [String] = []
    
    // MARK: - Shared Instance
    static let sharedInstance: RequsetManager = {
        let instance = RequsetManager()
        // setup code
        return instance
    }()
    
    // MARK: - Initialization Method
    override init() {
        super.init()
    }
    
    //MARK: - Publick Method
    //MARK: - API
    
    func getAppInfo() -> String {
        let dictionary = Bundle.main.infoDictionary!
        let version = dictionary["CFBundleShortVersionString"] as! String
        let build = dictionary["CFBundleVersion"] as! String
        return version + "(" + build + ")"
    }
    
    func sendAppVersion (success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        let parameters = ["IOS version" : self.getAppInfo()]
        self.postRequest(request: kVersion, parameters: parameters, success: { (_ responseObject: Any) in
//            success(responseObject as! Dictionary<String, Any>)
        }) { (_ error: Int?) in
            guard let error = error else { return }
            failure(self.checkErrorCode(code: error))
        }
    }
    
    func doctorActivity (success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        let parameters = ["token" : user.token]
        self.postRequest(request: kDoctorActivity, parameters: parameters, success: { (_ responseObject: Any) in
            success(responseObject as! Dictionary<String, Any>)
        }) { (_ error: Int?) in
            guard let error = error else { return }
            failure(self.checkErrorCode(code: error))
        }
    }
    
    func getCalendarDates (success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        //
        let request = String(format:"%@?token=%@",kCalendarDate,user.token)
        self.getRequest(request: request, parameters: [:], success: { (_ responseObject: Any) in
            success(responseObject as! Dictionary<String, Any>)
        }) { (_ error: Int?) in
            guard let error = error else { return }
            failure(self.checkErrorCode(code: error))
        }
    }
    
    func checkVersion (success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        self.getRequest(request: kCheckVersion, parameters: [:], success: { response in
            success (response)
        }) { errorCode in
            guard let error = errorCode else { return }
            failure(self.checkErrorCode(code: error))
        }
    }
    
    func getAcademyListWithId (categoryId : Int, success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        //
        let request = String(format:"%@?token=%@&category=\(categoryId)",kAcademy,user.token)
        self.getRequest(request: request, parameters: [:], success: { (_ responseObject: Any) in
            success(responseObject as! Dictionary<String, Any>)
        }) { (_ error: Int?) in
            guard let error = error else { return }
            failure(self.checkErrorCode(code: error))
        }
    }
    
    func getAcademy (success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        //
        let request = String(format:"%@?token=%@",kAcademy,user.token)
        self.getRequest(request: request, parameters: [:], success: { (_ responseObject: Any) in
            success(responseObject as! Dictionary<String, Any>)
        }) { (_ error: Int?) in
            guard let error = error else { return }
            failure(self.checkErrorCode(code: error))
        }
    }
    
    func isShowMedMenu (success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        let request = String(format:"%@?token=%@",kIsShowMenu,user.token)
        self.getRequest(request: request, parameters: [:], success: { (_ responseObject: Any) in
            success(responseObject as! Dictionary<String, Any>)
        }) { (_ error: Int?) in
            guard let error = error else { return }
            failure(self.checkErrorCode(code: error))
        }
    }
    
    func sendPatient (patient : Patient, success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        let parameters = ["p_name" : patient.name,
                          "p_sex" : patient.sex,
                          "p_age" : patient.age] as [String : Any]
        self.postRequest(request: kSendPatient, parameters: parameters, success: { (_ responseObject: Any) in
            success(responseObject as! Dictionary<String, Any>)
        }) { (_ error: Int?) in
            guard let error = error else { return }
            failure(self.checkErrorCode(code: error))
        }
    }
    
    func getPatientList (success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        //
        let request = String(format:"%@?token=%@",kGetPatientList,user.token)
        self.getRequest(request: request, parameters: [:], success: { (_ responseObject: Any) in
            
            success(responseObject as! Dictionary<String, Any>)
        }) { (_ error: Int?) in
            guard let error = error else { return }
            failure(self.checkErrorCode(code: error))
        }
    }
    
    func getVisits (patientId : Int, success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        //
        let request = String(format:"%@?token=%@&pacient_id=%@",kGetVisitsList,user.token,String(patientId))
        self.getRequest(request: request, parameters: [:], success: { (_ responseObject: Any) in
            
            success(responseObject as! Dictionary<String, Any>)
        }) { (_ error: Int?) in
            guard let error = error else { return }
            failure(self.checkErrorCode(code: error))
        }
    }
    
    func authUserFromServer (login : String, password : String, success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
//        let parameters = ["login" : login, "password" : password]
        let auth = String(format:"%@auth?login=%@&password=%@",kAPIServer,login,password)
        self.getRequest(request: auth, parameters: [:], success: { (_ responseObject: Any) in
            self.updateUserFromRealm(response: responseObject as! [String : Any])
            self.authFirebaseToken()
            success(responseObject as! Dictionary<String, Any>)
        }) { (_ error: Int?) in
            guard let error = error else { return }
            failure(self.checkErrorCode(code: error))
        }
    }
    
    //TODO получить данные от юзера по токену
    func getUserFromToken (token : String, success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        //
        let auth = String(format:"%@authtoken?token=%@",kAPIServer,token)
        self.getRequest(request: auth, parameters: [:], success: { (_ responseObject: Any) in
            self.updateUserFromRealm(response: responseObject as! [String : Any])
            self.authFirebaseToken()
            success(responseObject as! Dictionary<String, Any>)
        }) { (_ error: Int?) in
            guard let error = error else { return }
            failure(self.checkErrorCode(code: error))
        }
    }
    
    func getLegalConsultation (success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        let getList = String(format:"%@legal_consultation?token=%@",kAPIServer,user.token)
        self.getRequest(request: getList, parameters: [:], success: { (_ responseObject: Any) in
            success(responseObject as! Dictionary<String, Any>)
        }) { (_ error: Int?) in
            guard let error = error else { return }
            failure(self.checkErrorCode(code: error))
        }
    }
    
    
    func getMyLegalConsultation (success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        let getList = String(format:"%@legal_consultation?token=%@&popular=1",kAPIServer,user.token)
        self.getRequest(request: getList, parameters: [:], success: { (_ responseObject: Any) in
            success(responseObject as! Dictionary<String, Any>)
        }) { (_ error: Int?) in
            guard let error = error else { return }
            failure(self.checkErrorCode(code: error))
        }
    }
    
    func getLegalConsultationFromId (lcId : String ,success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        let getList = String(format:"%@legal_consultation?token=%@&id=%@",kAPIServer,user.token, lcId)
        self.getRequest(request: getList, parameters: [:], success: { (_ responseObject: Any) in
            success(responseObject as! Dictionary<String, Any>)
        }) { (_ error: Int?) in
            guard let error = error else { return }
            failure(self.checkErrorCode(code: error))
        }
    }
    
    /////////
    
    
    func checkIsSendQuestion (category : String, success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        let getList = String(format:"%@show_med_consultation_form?token=%@&category=%@",kAPIServer,user.token, category)
        self.getRequest(request: getList, parameters: [:], success: { (_ responseObject: Any) in
            success(responseObject as! Dictionary<String, Any>)
        }) { (_ error: Int?) in
            guard let error = error else { return }
            failure(self.checkErrorCode(code: error))
        }
    }
    
    func getMedConsultation (category : String, success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        let getList = String(format:"%@med_consultation?token=%@&category=%@",kAPIServer,user.token, category)
        self.getRequest(request: getList, parameters: [:], success: { (_ responseObject: Any) in
            success(responseObject as! Dictionary<String, Any>)
        }) { (_ error: Int?) in
            guard let error = error else { return }
            failure(self.checkErrorCode(code: error))
        }
    }
    
    //https://kusumdoctors.co.ua/api/v1/show_med_consultation_full?token=1444444&question_id=1
    
    func getMedConsultationFromId (lcId : String ,success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        let getList = String(format:"%@show_med_consultation_full?token=%@&question_id=%@",kAPIServer,user.token, lcId)
        self.getRequest(request: getList, parameters: [:], success: { (_ responseObject: Any) in
            success(responseObject as! Dictionary<String, Any>)
        }) { (_ error: Int?) in
            guard let error = error else { return }
            failure(self.checkErrorCode(code: error))
        }
    }

    
    func getList (success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
//        let getList = "http://kusumdoctors.co.ua/api/v1/get_list?new=1&token=123452340523-452345"
        let getList = String(format:"%@get_list?token=%@&new=1",kAPIServer,user.token)
        self.getRequest(request: getList, parameters: [:], success: { (_ responseObject: Any) in
            success(responseObject as! Dictionary<String, Any>)
        }) { (_ error: Int?) in
            guard let error = error else { return }
            failure(self.checkErrorCode(code: error))
        }
    }

    func getQuestionnaireList (success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        //        let getList = "http://kusumdoctors.co.ua/api/v1/get_list?new=1&token=123452340523-452345"
        let getList = String(format:"%@get_opros?token=%@&plat=andr",kAPIServer,user.token)
        self.getRequest(request: getList, parameters: [:], success: { (_ responseObject: Any) in
            success(responseObject as! Dictionary<String, Any>)
        }) { (_ error: Int?) in
            guard let error = error else { return }
            failure(self.checkErrorCode(code: error))
        }
    }
    
    func forgotPasswordFromServer (login : String, firebaseToken : String, success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        let parameters = ["login" : login, "firebase_token" : firebaseToken]
        self.putRequest(request: kForgotPassword, parameters: parameters, success: { (_ responseObject: Any) in
            success(responseObject as! Dictionary<String, Any>)
        }) { (_ error: Int?) in
            guard let error = error else { return }
            failure(self.checkErrorCode(code: error))
        }
    }
    
    func authFirebaseToken (token : String, firebaseToken : String, success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        let parameters = ["token" : token,
                          "firebase_token" : firebaseToken]
        self.putRequest(request: kAuthFirebaseToken, parameters: parameters, success: { (_ responseObject: Any) in
            success(responseObject as! Dictionary<String, Any>)
        }) { (_ error: Int?) in
            guard let error = error else { return }
            failure(self.checkErrorCode(code: error))
        }
    }
    
    func writeAnswerFromServer (articleId : Int, bonus : Int, success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        let parameters = ["article_id" : articleId,
                          "token" : user.token,
                          "bonus" : bonus] as [String : Any]
        self.postRequest(request: kWriteAnswer, parameters: parameters, success: { (_ responseObject: Any) in
            success(responseObject as! Dictionary<String, Any>)
        }) { (_ error: Int?) in
            guard let error = error else { return }
            failure(self.checkErrorCode(code: error))
        }
    }
    
    func writeAnswerWithTextFromServer (text : String ,articleId : Int, bonus : Int, success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        let parameters = ["answer" : text,
                          "article_id" : articleId,
                          "token" : user.token,
                          "bonus" : bonus] as [String : Any]
        self.postRequest(request: kWriteAnswer, parameters: parameters, success: { (_ responseObject: Any) in
            success(responseObject as! Dictionary<String, Any>)
        }) { (_ error: Int?) in
            guard let error = error else { return }
            failure(self.checkErrorCode(code: error))
        }
    }
    
    func sendPhoto (photo : UIImage ,success: @escaping (_ isOk : Bool) -> Void, failure: @escaping (_ isOk : Bool) -> Void) {
        
        let image = photo
        let imgData = UIImageJPEGRepresentation(image, 0.2)!
        
        let parameters = ["token": user.token]
        
        AF.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(imgData, withName: "uploaded_file",fileName: "file.jpg", mimeType: "image/jpg")
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            } //Optional for extra parameters
        },
                         to:kPhotoUpload)
        { (result) in
            success(true)
        }
    }
    
//    func sendPhoto (photo : UIImage ,success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
//        let parameters = ["uploaded_file" : photo,
//                          "token" : user.token] as [String : Any]
//        self.postRequest(request: kPhotoUpload, parameters: parameters, success: { (_ responseObject: Any) in
//            success(responseObject as! Dictionary<String, Any>)
//        }) { (_ error: Int?) in
//            failure(self.checkErrorCode(code: error!))
//        }
//    }
    
    
    
    func sendQestion (qestionId : Int, text : String, success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        
        var parameters = ["question" : text,
                          "token" : user.token,] as [String : Any]
        
        if qestionId != 0 {
            parameters.updateValue(qestionId, forKey: "question_id")
        }
        self.postRequest(request: kSendQestions, parameters: parameters, success: { (_ responseObject: Any) in
            success(responseObject as! Dictionary<String, Any>)
        }) { (_ error: Int?) in
            guard let error = error else { return }
            failure(self.checkErrorCode(code: error))
        }
    }
    
    func sendMedQestion (category : String, qestionId : Int, text : String, success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        
        var parameters = ["question" : text,
                          "token" : user.token,
                          "category" : category] as [String : Any]
        
        if qestionId != 0 {
            parameters.updateValue(qestionId, forKey: "question_id")
        }
        self.postRequest(request: kMedSendQestions, parameters: parameters, success: { (_ responseObject: Any) in
            success(responseObject as! Dictionary<String, Any>)
        }) { (_ error: Int?) in
            guard let error = error else { return }
            failure(self.checkErrorCode(code: error))
        }
    }
    
    func checkUserToken (success : @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        let userToken = String(format:"%@check_token?token=%@",kAPIServer,user.token)
        self.getRequest(request: userToken, parameters: [:], success: { (_ responseObject: Any) in
            success(responseObject as! Dictionary<String, Any>)
        }) { (_ error: Int?) in
            guard let error = error else { return }
            failure(self.checkErrorCode(code: error))
        }
    }
    
    func getAchivmentsFromServer (success : @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        let achivment = String(format:"%@get_achievements?token=%@",kAPIServer,user.token)
        self.getRequest(request: achivment, parameters: [:], success: { (_ responseObject: Any) in
            success(responseObject as! Dictionary<String, Any>)
        }) { (_ error: Int?) in
            guard let error = error else { return }
            failure(self.checkErrorCode(code: error))
        }
    }
    
    func getArticlesFromServer(success : @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        let getList = String(format:"%@get_list?token=%@",kAPIServer,user.token)
        self.getRequest(request: getList, parameters: [:], success: { (_ responseObject: Any) in
            success(responseObject as! Dictionary<String, Any>)
        }) { (_ error: Int?) in
            guard let error = error else { return }
            failure(self.checkErrorCode(code: error))
        }
    }
    
    func getConferenceFromServer(success : @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        let getList = String(format:"%@get_conference?token=%@",kAPIServer,user.token)
        self.getRequest(request: getList, parameters: [:], success: { (_ responseObject: Any) in
            success(responseObject as! Dictionary<String, Any>)
        }) { (_ error: Int?) in
            guard let error = error else { return }
            failure(self.checkErrorCode(code: error))
        }
    }
    
    func getAccountsFromServer(success : @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        let getList = String(format:"%@getmyauth?token=%@",kAPIServer,user.token)
        self.getRequest(request: getList, parameters: [:], success: { (_ responseObject: Any) in
            success(responseObject as! Dictionary<String, Any>)
        }) { (_ error: Int?) in
            guard let error = error else { return }
            failure(self.checkErrorCode(code: error))
        }
    }
    
    func changePassword (login : String, code : Int, newPassword : String, success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        let parameters = ["login" : login,
                          "code" : code,
                          "new_password" : newPassword] as [String : Any]
        self.putRequest(request: kChangePassword, parameters: parameters, success: { (_ responseObject: Any) in
            success(responseObject as! Dictionary<String, Any>)
        }) { (_ error: Int?) in
            guard let error = error else { return }
            failure(self.checkErrorCode(code: error))
            
        }
    }
    
    func changePush (state : Int, success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        let parameters = ["token" : user.token,
                          "send_push" : state] as [String : Any]
        self.putRequest(request: kChangePush, parameters: parameters, success: { (_ responseObject: Any) in
            success(responseObject as! Dictionary<String, Any>)
        }) { (_ error: Int?) in
            guard let error = error else { return }
            failure(self.checkErrorCode(code: error))
        }
    }

    //MARK: -
    func checkInternetConnection () -> Bool {
        return NetworkReachabilityManager()!.isReachable
    }
    
    func showError(viewController : UIViewController, code : Int) {
        switch (code) {
        case 300 : viewController.alert(message: ErrorMessegess.NoInternet.text, title: "Ошибка")
            break
        case 408 : viewController.alert(message: ErrorMessegess.UserNotFound.text, title: "Ошибка")
            break
        case 401 : viewController.alert(message: ErrorMessegess.PasswordIncorect.text, title:"Ошибка")
            break
        case 402 : viewController.alert(message: ErrorMessegess.PushCodeIncorect.text, title:"Ошибка" )
            break
        case 404 : viewController.alert(message: ErrorMessegess.NoServer.text, title: "Ошибка")
            break
        default:
            break
        }
    }
    
    // MARK: - Private function
    private func updateUserFromRealm (response : [String: Any]) {
        let currentUser = User().currentUser()
        let responseUser = User()
        let token = Messaging.messaging().fcmToken
        responseUser.firebaseToken = token!
        responseUser.getUserFromServerResponse(response: response)
        self.user.updateUser(newUser: responseUser)
        let realm = try! Realm()
        let usersArray = realm.objects(User.self)
        print("\(usersArray)")
    }
    
    private func authFirebaseToken() {
        self.authFirebaseToken(token: user.token, firebaseToken: user.firebaseToken, success: { (_ responseObject: Dictionary<String, Any>) in
            
        }) { (_ error: Int?) in
            
        }
    }
    
    private func getRequest (request : String , parameters: [AnyHashable: Any], success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        AF.request(request, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
            switch(response.result) {
            case .success(let value):
                if let responseObject = value as? Dictionary<String, Any> {
                    success(responseObject)
                }
                break
            case .failure(_):
                failure(response.response?.statusCode)
                print(response.result)
                break
            }
        }
    }
    
    private func deleteRequest (request : String , parameters: [String: Any], success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        AF.request(request, method: .delete, parameters: parameters, encoding: URLEncoding.default, headers: ["Content-Type": "application/x-www-form-urlencoded"]).responseJSON { response in
            switch(response.result) {
            case .success(let value):
                if let responseObject = value as? Dictionary<String, Any> {
                    success(responseObject)
                }
                break
            case .failure(_):
                failure(response.response?.statusCode)
                print(response.result)
                break
            }
        }
    }
    
    private func putRequest (request : String , parameters: [String: Any], success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        AF.request(request, method: .put, parameters: parameters, encoding: URLEncoding.default, headers: ["Content-Type": "application/x-www-form-urlencoded"]).responseJSON { response in
            switch(response.result) {
            case .success(let value):
                if let responseObject = value as? Dictionary<String, Any> {
                    success(responseObject)
                }
                break
            case .failure(_):
                failure(response.response?.statusCode)
                print(response.result)
                break
            }
        }
    }
    
    private func checkErrorCode (code:Int) -> Int {
        var errorCode = 0
        if !self.checkInternetConnection() {
            errorCode = 300
        } else if code == 411 {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.showErrorAndGoToStartScreen()
        } else {
            errorCode = code
        }
        return errorCode
    }
    
    private func postRequest (request : String , parameters: [String: Any], success: @escaping (_ responseObject: Dictionary<String, Any>) -> Void, failure: @escaping (_ error: Int?) -> Void) {
        
        
        AF.request(request, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: ["Content-Type": "application/x-www-form-urlencoded"]).responseJSON { response in
            switch(response.result) {
            case .success(let value):
                if let responseObject = value as? Dictionary<String, Any> {
                    success(responseObject)
                }
                break
            case .failure(_):
                failure(response.response?.statusCode)
                print(response.result)
                break
            }
        }
    }
}
