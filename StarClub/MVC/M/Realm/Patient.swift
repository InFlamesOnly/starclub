//
//  Patient.swift
//  StarClub
//
//  Created by Dima on 18.03.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit
import RealmSwift

class Patient: Object {
    @Persisted var patientId : Int
    @Persisted var count : Int
    @Persisted var name : String
    @Persisted var sex : String
    @Persisted var age : String
    
    func getPatientFromServerResponse (response : Dictionary<String, Any>) {
        self.patientId = response ["id"] as? Int ?? 0
        self.count = response ["count"] as? Int ?? 0
        self.name = response ["name"] as? String ?? ""
        self.sex = response ["sex"] as? String ?? ""
        self.age = response ["age"] as? String ?? ""
        
    }
    
    class func mapResponseToArrayObject (response : Dictionary<String, Any>) -> Array<Patient> {
        var array = Array<Patient>()
        if let dataArray = response["data"] as? [[String: Any]] {
            for objDict in dataArray {
                let object = Patient()
                object.getPatientFromServerResponse(response: objDict)
                array.append(object)
            }
        }
        return array
    }
}
