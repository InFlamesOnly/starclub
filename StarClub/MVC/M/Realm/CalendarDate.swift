//
//  CalendarDate.swift
//  StarClub
//
//  Created by Hackintosh on 7/5/19.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit
import RealmSwift

class CalendarDate: Object {
    @Persisted var articleId : Int
    @Persisted var title : String
    @Persisted var shortContent : String
    @Persisted var fullContent : String
    @Persisted var startDate : String
    @Persisted var startTime : String
    @Persisted var endDate : String
    @Persisted var endTime : String
    @Persisted var location : String
    @Persisted var image : String

    
    func getArticleFromServerResponse (response : Dictionary<String, Any>) {
        let result = (response as [String : AnyObject])["article"]
        
        self.articleId = result! ["article_id"] as? Int ?? 0
        self.title = result! ["title"] as? String ?? ""
        self.shortContent = result! ["short_content"] as? String ?? ""
        self.fullContent = result! ["full_content"] as? String ?? ""
        self.startDate = result! ["start_date"] as? String ?? ""
        self.startTime = result! ["start_time"] as? String ?? ""
        self.endDate = result! ["end_date"] as? String ?? ""
        self.endTime = result! ["end_time"] as? String ?? ""
        self.location = result! ["location"] as? String ?? ""
        self.image = result! ["img"] as? String ?? ""
    }
    
    class func mapResponseToArrayObject (response : Dictionary<String, Any>) -> Array<CalendarDate> {
        var array = Array<CalendarDate>()
        if let artArray = response["result"] as? [[String: Any]] {
            for articleDict in artArray {
                let article = CalendarDate()
                article.getArticleFromServerResponse(response: articleDict)
                array.append(article)
            }
        }
        return array
    }

}
