//
//  ArticleAcademy.swift
//  StarClub
//
//  Created by Dima on 23.05.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit
import RealmSwift

class ArticleAcademy: Object {
    @Persisted var articleId = 0
    @Persisted var title = ""
    @Persisted var link = ""
    @Persisted var shortContent = ""
    @Persisted var category = ""
    
    func getArticleFromServerResponse (response : Dictionary<String, Any>) {
        let result = (response as [String : AnyObject])["article"]
        
        self.articleId = result! ["article_id"] as? Int ?? 0
        self.title = result! ["title"] as? String ?? ""
        self.link = result! ["link"] as? String ?? ""
        
        self.shortContent = result! ["short_content"] as? String ?? ""
        self.category = result! ["category"] as? String ?? ""
    }
    
    class func mapResponseToArrayObject (response : Dictionary<String, Any>) -> Array<ArticleAcademy> {
        var array = Array<ArticleAcademy>()
        if let artArray = response["result"] as? [[String: Any]] {
            for articleDict in artArray {
                let article = ArticleAcademy()
                article.getArticleFromServerResponse(response: articleDict)
                array.append(article)
            }
        }
        return array
    }
}
