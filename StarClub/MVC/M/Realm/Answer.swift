//
//  Answer.swift
//  StarClub
//
//  Created by macOS on 21.12.17.
//  Copyright © 2017 macOS. All rights reserved.
//

import UIKit
import RealmSwift

class Answer: Object {
    @Persisted var value = ""
    @Persisted var isTrue = false
    @Persisted var color = 0
//    @Persisted var color : Array<UIColor> = []
    
    func getAnswerFromServerResponse (response : Dictionary<String, Any>) {
        self.value = response ["value"] as? String ?? ""
        self.isTrue = response ["istrue"] as? Bool ?? true
        self.color = 1
    }
}
