//
//  LegalConsultationAnswer.swift
//  StarClub
//
//  Created by macOS on 21.02.18.
//  Copyright © 2018 macOS. All rights reserved.
//

import UIKit
import RealmSwift

class LegalConsultation: Object {
    @Persisted var  consId : Int
    @Persisted var text : String
    @Persisted var  answer : String
    @Persisted var  isMyQuestion : Bool
    @Persisted var isPrivate : Bool
    @Persisted var isAnswered : Bool
    @Persisted var isPublished : Bool
    
    
    func getlegalConsultationFromServerResponse (response : Dictionary<String, Any>) {
        let legalConsultation = (response as [String : AnyObject])["question"]
        self.consId = legalConsultation! ["question_id"] as? Int ?? 0
        self.text = legalConsultation! ["question_text"] as? String ?? ""
        self.answer = legalConsultation! ["answer"] as? String ?? ""
        self.isMyQuestion = legalConsultation! ["my_question"] as? Bool ?? false
        self.isPrivate = legalConsultation! ["private"] as? Bool ?? false
        self.isAnswered = legalConsultation! ["answered"] as? Bool ?? false
        self.isPublished = legalConsultation! ["status"] as? Bool ?? false
    }
    
    class func mapResponseToArrayObject (response : Dictionary<String, Any>) -> Array<LegalConsultation> {
        var array = Array<LegalConsultation>()
        if let legalConsultationArray = response["question_result"] as? [[String: Any]] {
            for legalConsultationDict in legalConsultationArray {
                let lc = LegalConsultation()
                lc.getlegalConsultationFromServerResponse(response: legalConsultationDict)
                array.append(lc)
            }
        }
        return array
    }


}
