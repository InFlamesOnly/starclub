//
//  Article.swift
//  StarClub
//
//  Created by macOS on 21.12.17.
//  Copyright © 2017 macOS. All rights reserved.
//

import UIKit
import RealmSwift

class Article: Object {
    @Persisted var articleId = 0
    @Persisted var title = ""
    @Persisted var content = ""
    @Persisted var image = ""
    @Persisted var bonus = 0
    @Persisted var opros = 0
    @Persisted var written = ""
    @Persisted var isAnsweredCell = false
    
    //"opros": 0
    func getArticleFromServerResponse (response : Dictionary<String, Any>) {
        let result = (response as [String : AnyObject])["article"]
        let test = (response as [String : AnyObject])["test"]
        
        self.articleId = result! ["article_id"] as? Int ?? 0
        self.title = result! ["title"] as? String ?? ""
        self.content = result! ["content"] as? String ?? ""
        
        self.image = result! ["image"] as? String ?? ""
        self.bonus = result! ["bonus"] as? Int ?? 0
        self.written = result! ["written"] as? String ?? ""
        self.isAnsweredCell = test! ["isQuestionArticle"] as? Bool ?? false
        self.opros = result! ["opros"] as? Int ?? 0
    }
    
    class func mapResponseToArrayObject (response : Dictionary<String, Any>) -> Array<Article> {
        var array = Array<Article>()
        if let artArray = response["result"] as? [[String: Any]] {
            for articleDict in artArray {
                let article = Article()
                article.getArticleFromServerResponse(response: articleDict)
                array.append(article)
            }
        }
        return array
    }
}
