//
//  Achivment.swift
//  StarClub
//
//  Created by macOS on 22.12.17.
//  Copyright © 2017 macOS. All rights reserved.
//

import UIKit
import RealmSwift

class Achivment: Object {
    @Persisted var imageURL = ""
    @Persisted var sum = 0
    @Persisted var mounthMax = 0
    @Persisted var mounthSum = 0
    @Persisted var currentMounthName = ""
    let mounth = List<Mounth>()
    
    func getAchivmentFromServerResponse (response : Dictionary<String, Any>) {
        let result = (response as [String : AnyObject])["achievements"]
        
        self.imageURL = result! ["image_url"] as? String ?? ""
        self.sum = result! ["sum"] as? Int ?? 0
        self.mounthMax = result! ["mounth_max"] as? Int ?? 0
        self.currentMounthName = result! ["current_mounth_name"] as? String ?? ""
        self.mounthSum = result! ["mounth_sum"] as? Int ?? 0
        
        if let mounthArray = result!["history"] as? [[String: Any]] {
            for mounthDict in mounthArray {
                let mounth = Mounth()
                mounth.getMounthFromServerResponse(response: mounthDict)
                self.mounth.append(mounth)
            }
        }
    }
    
}

