//
//  Conference.swift
//  StarClub
//
//  Created by macOS on 27.12.17.
//  Copyright © 2017 macOS. All rights reserved.
//

import UIKit
import RealmSwift

class Conference: Object {
    @Persisted var conferenceId = 0
    @Persisted var title = ""
    @Persisted var content = ""
    @Persisted var image = ""
    
    func getConferenceFromServerResponse (response : Dictionary<String, Any>) {
        let result = (response as [String : AnyObject])["conference"]
        
        self.conferenceId = result! ["conference_id"] as? Int ?? 0
        self.title = result! ["title"] as? String ?? ""
        self.content = result! ["content"] as? String ?? ""
        self.image = result! ["image"] as? String ?? ""
        
    }
    
    class func mapResponseToArrayObject (response : Dictionary<String, Any>) -> Array<Conference> {
        var array = Array<Conference>()
        if let conferenceArray = response["conference_result"] as? [[String: Any]] {
            for confDict in conferenceArray {
                let conf = Conference()
                conf.getConferenceFromServerResponse(response: confDict)
                array.append(conf)
            }
        }
        return array
    }

}
