//
//  Mounth.swift
//  StarClub
//
//  Created by macOS on 22.12.17.
//  Copyright © 2017 macOS. All rights reserved.
//

import UIKit
import RealmSwift

class Mounth: Object {
    
    @Persisted var name = ""
    @Persisted var bonus = 0
    @Persisted var time = 0

    func getMounthFromServerResponse (response : Dictionary<String, Any>) {
        self.name = response ["name"] as? String ?? ""
        self.bonus = response ["bonus"] as? Int ?? 0
        self.time = response ["time"] as? Int ?? 0
    }
}
