//
//  User.swift
//  StarClub
//
//  Created by macOS on 19.12.17.
//  Copyright © 2017 macOS. All rights reserved.
//

import UIKit
import RealmSwift

class User: Object {
    @Persisted var name = ""
    @Persisted var token = ""
    @Persisted var firebaseToken = ""
    @Persisted var achievement = 0
    @Persisted var push = 0
    @Persisted var specific = 0
    
    func getUserFromServerResponse (response : Dictionary<String, Any>) {
        let user = (response as [String : AnyObject])["user"]
        self.name = user! ["name"] as? String ?? ""
        self.token = user! ["token"] as? String ?? ""
        self.achievement = user! ["achievement"] as? Int ?? 0
        self.push = user! ["send_push"] as? Int ?? 0
        self.specific = user! ["speciality"] as? Int ?? 0
    }
    
    func currentUser () -> User {
        var user = User()
        let realm = try! Realm()
        let usersArray = realm.objects(User.self)
        if usersArray.count == 1 {
            let currentUser = usersArray[0]
            user = currentUser
        }
        try! realm.write {
            realm.add(user)
        }
        return user
    }
    
    func save () {
        let realm = try! Realm()
        let usersArray = realm.objects(User.self)
        if usersArray.count == 0 {
            try! realm.write {
                realm.add(self)
            }
        }
    }
    
    func updateUser (newUser : User) {
        let realm = try! Realm()
        let user = realm.objects(User.self)[0]
        try! realm.write {
            user.achievement = newUser.achievement
            user.firebaseToken = newUser.firebaseToken
            user.token = newUser.token
            user.name = newUser.name
            user.push = newUser.push
            user.specific = newUser.specific
        }
    }
}
