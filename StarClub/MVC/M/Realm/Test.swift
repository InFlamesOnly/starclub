//
//  Test.swift
//  StarClub
//
//  Created by macOS on 21.12.17.
//  Copyright © 2017 macOS. All rights reserved.
//

import UIKit
import RealmSwift

class Test: Object {
    
    @Persisted var title = ""
    let answers = List<Answer>()

    
    func getTestFromServerResponse (response : Dictionary<String, Any>) {
        let result = (response as [String : AnyObject])["test"]
        self.title = result! ["title"] as? String ?? ""
    }
    
    class func mapResponseToArrayObject (response : Dictionary<String, Any>) -> Array<Test> {
        var array = Array<Test>()
        if let testArray = response["result"] as? [[String: Any]] {
            for testDict in testArray {
                let dict = testDict["test"] as? [String: Any]
                let test = Test()
                test.getTestFromServerResponse(response: testDict)
                if let answArray = dict!["answers"] as? [[String: Any]] {
                    for answDict in answArray {
                        let answer = Answer()
                        answer.getAnswerFromServerResponse(response: answDict)
                        test.answers.append(answer)
                    }
                }
                array.append(test)
            }
        }

        return array
    }
    
//    if let testArray = responseObject["result"] as? [[String: Any]] {
//        for testDict in testArray {
//            let dict = testDict["test"] as? [String: Any]
//            let test = Test()
//            test.getTestFromServerResponse(response: testDict)
//            if let answArray = dict!["answers"] as? [[String: Any]] {
//                for answDict in answArray {
//                    let answer = Answer()
//                    answer.getAnswerFromServerResponse(response: answDict)
//                    test.answers.append(answer)
//                }
//            }
//            self.testArray.append(test)
//        }
//    }

}
