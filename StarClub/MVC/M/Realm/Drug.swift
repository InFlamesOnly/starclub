//
//  Drugs.swift
//  StarClub
//
//  Created by macOS on 23.12.17.
//  Copyright © 2017 macOS. All rights reserved.
//

import UIKit
import RealmSwift

class Drug: Object {
    @Persisted var drugsImage = ""
    @Persisted var flowerImage = ""
    @Persisted var name = ""
    
    func addDrugsToGastroDataBase () {
        let realm = try! Realm()
        let drugsArray = realm.objects(Drug.self)
        if drugsArray.count == 0 {
            let drug1 = Drug()
            drug1.drugsImage = "DrugGastro1"
            drug1.flowerImage = "DrugGastroImage3"
            drug1.name = "Домрид SR таблетки 30 мг №10, №30"
            
            let drug2 = Drug()
            drug2.drugsImage = "DrugGastro2"
            drug2.flowerImage = "DrugGastroImage4"
            drug2.name = "Укрлив таблетки 250 мг №100, №30"
            
            let drug3 = Drug()
            drug3.drugsImage = "DrugGastro3"
            drug3.flowerImage = "DrugGastroImage2"
            drug3.name = "Золопент таблетки 20 мг №14"
            
            let drug4 = Drug()
            drug4.drugsImage = "DrugGastro4"
            drug4.flowerImage = "DrugGastroImage5"
            drug4.name = "Золопент таблетки 40 мг №14, №30"
            
            let drug5 = Drug()
            drug5.drugsImage = "DrugGastro5"
            drug5.flowerImage = "DrugGastroImage6"
            drug5.name = "Газоспазам таблетки 20/125 мг №30"
            
            try! realm.write {
                realm.add(drug1)
                realm.add(drug2)
                realm.add(drug3)
                realm.add(drug4)
                realm.add(drug5)
            }

        }
    }
    
    func removeAllDrug () {
        let realm = try! Realm()
        let allDrugs = realm.objects(Drug.self)
        try! realm.write {
            realm.delete(allDrugs)
        }
    }
    
    func addDrugsToRealmDataBase () {
        let realm = try! Realm()
        let drugsArray = realm.objects(Drug.self)
        if drugsArray.count == 0 {
            
            let drug1 = Drug()
            drug1.drugsImage = "Drug1"
            drug1.flowerImage = "Pizza1"

            drug1.name = "Воксид таблетки 0,2 мг, 0,3 мг №30"
        
            let drug2 = Drug()
            drug2.drugsImage = "Drug2"
            drug2.flowerImage = "Pizza2"
            drug2.name = "Глимакс таблетки 2мг, 3мг, 4мг  №30 или №60"
        
            let drug3 = Drug()
            drug3.drugsImage = "Drug3"
            drug3.flowerImage = "Pizza3"
            drug3.name = "Глютазон таблетки 15 мг, 30 мг, 45 мг №30"
        
            let drug4 = Drug()
            drug4.drugsImage = "Drug4"
            drug4.flowerImage = "Pizza4"
            drug4.name = "Дуглимакс таблетки 1 мг, 2 мг №30 или №60"
        
            let drug5 = Drug()
            drug5.drugsImage = "Drug5"
            drug5.flowerImage = "Pizza5"
            drug5.name = "Метамин таблетки 500 мг, 850 мг, 1000 мг №30, №90 или №100"
        
            let drug6 = Drug()
            drug6.drugsImage = "Drug6"
            drug6.flowerImage = "Pizza6"
            drug6.name = "Метамин SR таблетки 500 мг №30 или №90"
        
            let drug7 = Drug()
            drug7.drugsImage = "Drug7"
            drug7.flowerImage = "Pizza7"
            drug7.name = "Етсет таблетки 10 мг, 20 мг, 40 мг и 80 мг №28 или №84"
        
            try! realm.write {
                realm.add(drug1)
                realm.add(drug2)
                realm.add(drug3)
                realm.add(drug4)
                realm.add(drug5)
                realm.add(drug6)
                realm.add(drug7)
            }
        }
    }
    
    func addDrugsToCardioDataBase () {
        let realm = try! Realm()
        let drugsArray = realm.objects(Drug.self)
        if drugsArray.count == 0 {
            let drug1 = Drug()
            drug1.drugsImage = "Cardio1"
            drug1.flowerImage = "Cardio"
            drug1.name = "Етсет таблетки 10 мг, 20 мг, 40 мг и 80 мг №28 или №84"
            
            
            let drug2 = Drug()
            drug2.drugsImage = "Cardio2"
            drug2.flowerImage = "Cardio"
            drug2.name = "Лоспирин таблетки 75 мг, № 30 или №120"
            
            let drug3 = Drug()
            drug3.drugsImage = "Cardio3"
            drug3.flowerImage = "Cardio"
            drug3.name = "Озалекс таблетки 10 мг и 20 мг №28"
            
            let drug4 = Drug()
            drug4.drugsImage = "Cardio4"
            drug4.flowerImage = "Cardio"
            drug4.name = "Хипотел таблетки 40 мг и 80 мг №28"
            
            let drug5 = Drug()
            drug5.drugsImage = "Cardio5"
            drug5.flowerImage = "Cardio"
            drug5.name = "Укрлив таблетки 250 мг №30 или №100"
            
            let drug6 = Drug()
            drug6.drugsImage = "Cardio6"
            drug6.flowerImage = "Cardio"
            drug6.name = "Золопент таблетки 20, 60 мг №14 или №30"
            
            
            let drug7 = Drug()
            drug7.drugsImage = "Cardio7"
            drug7.flowerImage = "Cardio"
            drug7.name = "Клосарт таблетки 25 мг, 50 мг, 100 мг №28, №30, №84, №90 или №100"
            
            let drug8 = Drug()
            drug8.drugsImage = "Cardio8"
            drug8.flowerImage = "Cardio"
            drug8.name = "Платогрiл таблетки 75 мг №28 или №84"
            
            let drug9 = Drug()
            drug9.drugsImage = "Cardio9"
            drug9.flowerImage = "Cardio"
            drug9.name = "Семлопiн таблетки 2,5 мг и 5 мг №28"
            
            try! realm.write {
                realm.add(drug1)
                realm.add(drug2)
                realm.add(drug3)
                realm.add(drug4)
                realm.add(drug5)
                realm.add(drug6)
                realm.add(drug7)
                realm.add(drug8)
                realm.add(drug9)
            }
        }
    }
    
    func allDrugs () {
        let realm = try! Realm()
        let drugsArray = realm.objects(Drug.self)
        if drugsArray.count == 0 {
            let drug1 = Drug()
            drug1.drugsImage = "DrugGastro1"
            drug1.flowerImage = "DrugGastroImage3"
            drug1.name = "Домрид SR таблетки 30 мг №10, №30"
            
            let drug2 = Drug()
            drug2.drugsImage = "DrugGastro2"
            drug2.flowerImage = "DrugGastroImage4"
            drug2.name = "Укрлив таблетки 250 мг №100, №30"
            
            let drug3 = Drug()
            drug3.drugsImage = "DrugGastro3"
            drug3.flowerImage = "DrugGastroImage2"
            drug3.name = "Золопент таблетки 20 мг №14"
            
            let drug4 = Drug()
            drug4.drugsImage = "DrugGastro4"
            drug4.flowerImage = "DrugGastroImage5"
            drug4.name = "Золопент таблетки 40 мг №14, №30"
            
            let drug5 = Drug()
            drug5.drugsImage = "DrugGastro5"
            drug5.flowerImage = "DrugGastroImage6"
            drug5.name = "Газоспазам таблетки 20/125 мг №30"
            
            let drug6 = Drug()
            drug6.drugsImage = "Drug1"
            drug6.flowerImage = "Pizza1"
            drug6.name = "Воксид таблетки 0,2 мг, 0,3 мг №30"
            
            let drug7 = Drug()
            drug7.drugsImage = "Drug2"
            drug7.flowerImage = "Pizza2"
            drug7.name = "Глимакс таблетки 2мг, 3мг, 4мг  №30 или №60"
            
            let drug8 = Drug()
            drug8.drugsImage = "Drug3"
            drug8.flowerImage = "Pizza3"
            drug8.name = "Глютазон таблетки 15 мг, 30 мг, 45 мг №30"
            
            let drug9 = Drug()
            drug9.drugsImage = "Drug4"
            drug9.flowerImage = "Pizza4"
            drug9.name = "Дуглимакс таблетки 1 мг, 2 мг №30 или №60"
            
            let drug10 = Drug()
            drug10.drugsImage = "Drug5"
            drug10.flowerImage = "Pizza5"
            drug10.name = "Метамин таблетки 500 мг, 850 мг, 1000 мг №30, №90 или №100"
            
            let drug11 = Drug()
            drug11.drugsImage = "Drug6"
            drug11.flowerImage = "Pizza6"
            drug11.name = "Метамин SR таблетки 500 мг №30 или №90"
            
            let drug12 = Drug()
            drug12.drugsImage = "Drug7"
            drug12.flowerImage = "Pizza7"
            drug12.name = "Етсет таблетки 10 мг, 20 мг, 40 мг и 80 мг №28 или №84"
            
            let drug13 = Drug()
            drug13.drugsImage = "Cardio1"
            drug13.flowerImage = "Cardio"
            drug13.name = "Етсет таблетки 10 мг, 20 мг, 40 мг и 80 мг №28 или №84"
            
            
            let drug14 = Drug()
            drug14.drugsImage = "Cardio2"
            drug14.flowerImage = "Cardio"
            drug14.name = "Лоспирин таблетки 75 мг, № 30 или №120"
            
            let drug15 = Drug()
            drug15.drugsImage = "Cardio3"
            drug15.flowerImage = "Cardio"
            drug15.name = "Озалекс таблетки 10 мг и 20 мг №28"
            
            let drug16 = Drug()
            drug16.drugsImage = "Cardio4"
            drug16.flowerImage = "Cardio"
            drug16.name = "Хипотел таблетки 40 мг и 80 мг №28"
            
            let drug17 = Drug()
            drug17.drugsImage = "Cardio5"
            drug17.flowerImage = "Cardio"
            drug17.name = "Укрлив таблетки 250 мг №30 или №100"
            
            let drug18 = Drug()
            drug18.drugsImage = "Cardio6"
            drug18.flowerImage = "Cardio"
            drug18.name = "Золопент таблетки 20, 60 мг №14 или №30"
            
            
            let drug19 = Drug()
            drug19.drugsImage = "Cardio7"
            drug19.flowerImage = "Cardio"
            drug19.name = "Клосарт таблетки 25 мг, 50 мг, 100 мг №28, №30, №84, №90 или №100"
            
            let drug20 = Drug()
            drug20.drugsImage = "Cardio8"
            drug20.flowerImage = "Cardio"
            drug20.name = "Платогрiл таблетки 75 мг №28 или №84"
            
            let drug21 = Drug()
            drug21.drugsImage = "Cardio9"
            drug21.flowerImage = "Cardio"
            drug21.name = "Семлопiн таблетки 2,5 мг и 5 мг №28"
            
            try! realm.write {
                realm.add(drug6)
                realm.add(drug7)
                realm.add(drug8)
                realm.add(drug9)
                realm.add(drug10)
                realm.add(drug11)
                realm.add(drug12)
                realm.add(drug1)
                realm.add(drug2)
                realm.add(drug3)
                realm.add(drug4)
                realm.add(drug5)
                realm.add(drug13)
                realm.add(drug14)
                realm.add(drug15)
                realm.add(drug16)
                realm.add(drug17)
                realm.add(drug18)
                realm.add(drug19)
                realm.add(drug20)
                realm.add(drug21)
            }
            
        }
    }
}
