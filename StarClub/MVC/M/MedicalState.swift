//
//  MedicalState.swift
//  StarClub
//
//  Created by Dima on 23.04.2019.
//  Copyright © 2019 macOS. All rights reserved.
//

import UIKit

class MedicalState: NSObject {
    
    var esAg = false
    var simptAg = false
    var heart = false
    var twoType = false
    var difficultСases = false
    var otherIssues = false
    
    static let sharedInstance: MedicalState = {
        let instance = MedicalState()
        return instance
    }()
    
    func isEs () {
        MedicalState.sharedInstance.esAg = true
        MedicalState.sharedInstance.simptAg = false
        MedicalState.sharedInstance.heart = false
        MedicalState.sharedInstance.twoType = false
        MedicalState.sharedInstance.difficultСases = false
        MedicalState.sharedInstance.otherIssues = false
    }
    
    func isSimptAg () {
        MedicalState.sharedInstance.esAg = false
        MedicalState.sharedInstance.simptAg = true
        MedicalState.sharedInstance.heart = false
        MedicalState.sharedInstance.twoType = false
        MedicalState.sharedInstance.difficultСases = false
        MedicalState.sharedInstance.otherIssues = false
    }
    
    func isHeart () {
        MedicalState.sharedInstance.esAg = false
        MedicalState.sharedInstance.simptAg = false
        MedicalState.sharedInstance.heart = true
        MedicalState.sharedInstance.twoType = false
        MedicalState.sharedInstance.difficultСases = false
        MedicalState.sharedInstance.otherIssues = false
    }
    
    func isTwoType () {
        MedicalState.sharedInstance.esAg = false
        MedicalState.sharedInstance.simptAg = false
        MedicalState.sharedInstance.heart = false
        MedicalState.sharedInstance.twoType = true
        MedicalState.sharedInstance.difficultСases = false
        MedicalState.sharedInstance.otherIssues = false
    }
    
    func isDifficultСases () {
        MedicalState.sharedInstance.esAg = false
        MedicalState.sharedInstance.simptAg = false
        MedicalState.sharedInstance.heart = false
        MedicalState.sharedInstance.twoType = false
        MedicalState.sharedInstance.difficultСases = true
        MedicalState.sharedInstance.otherIssues = false
    }
    
    func isOtherIssues () {
        MedicalState.sharedInstance.esAg = false
        MedicalState.sharedInstance.simptAg = false
        MedicalState.sharedInstance.heart = false
        MedicalState.sharedInstance.twoType = false
        MedicalState.sharedInstance.difficultСases = false
        MedicalState.sharedInstance.otherIssues = true
    }
}
