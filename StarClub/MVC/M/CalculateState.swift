//
//  CalculateState.swift
//  StarClub
//
//  Created by macOS on 07.05.18.
//  Copyright © 2018 macOS. All rights reserved.
//

import UIKit

class CalculateState: NSObject {
    
    var isFirstCalculate = false
    var isSecondCalculate = false
    var isThirdCalculate = false
    var isFourCalculate = false
    
    static let sharedInstance: CalculateState = {
        let instance = CalculateState()
        return instance
    }()

}
