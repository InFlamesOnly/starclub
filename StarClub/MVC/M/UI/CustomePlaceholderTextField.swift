//
//  CustomePlaceholderTextField.swift
//  StarClub
//
//  Created by macOS on 12.12.17.
//  Copyright © 2017 macOS. All rights reserved.
//

import UIKit

enum Style {
    case Login
    case Password
    
    var text: String {
        switch self {
        case .Login: return "Номер телефону"
        case .Password: return "Пароль"
        }
    }
}

class CustomePlaceholderTextField: UITextField {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
//        let textField = PhoneNumberTextField()
//
//        PartialFormatter().formatPartial("+336895555")
    }
    
    func createTextFieldWithStyle (style : Style) {
        let attributes = [
            NSAttributedStringKey.foregroundColor: UIColor.black,
            NSAttributedStringKey.font : UIFont(name: "Rubik-Regular", size: 12)!
        ]
        
        if style == Style.Login  {
            self.attributedPlaceholder = NSAttributedString(string: Style.Login.text, attributes:attributes)
        } else {
            self.attributedPlaceholder = NSAttributedString(string: Style.Password.text, attributes:attributes)
        }
    }
}
