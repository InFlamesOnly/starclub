//
//  CodeView.swift
//  StarClub
//
//  Created by macOS on 19.12.17.
//  Copyright © 2017 macOS. All rights reserved.
//

import UIKit

protocol CodeSwipeViewDelegate : NSObjectProtocol {
    func goToNewPasswordSwipeView()
    func backToForgotPasswordSwipeView()
}

class CodeSwipeView: UIView, UITextFieldDelegate {

    var delegate : CodeSwipeViewDelegate?
    @IBOutlet weak var codeTextField: CustomePlaceholderTextField!
    @IBOutlet weak var newPasswordTextField: CustomePlaceholderTextField!
    @IBOutlet weak var acceptPasswordTextField: CustomePlaceholderTextField!
    
    @IBOutlet weak var codeBorderView: RoundViewWithBorder!
    @IBOutlet weak var newPasswordBorderView: RoundViewWithBorder!
    @IBOutlet weak var acceptPasswordBorderView: RoundViewWithBorder!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @IBAction func back(_ sender: Any) {
        print("tap back from forgot password")
        self.delegate?.backToForgotPasswordSwipeView()
    }
    
    @IBAction func newPassword(_ sender: Any) {
        print("tap change password")
        self.delegate?.goToNewPasswordSwipeView()
    }
    
    //MARK: - TextFieldDelegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == codeTextField {
            if range.location < 6 {
                return true
            } else {
                return false
            }
        }
        return true
    }
}
