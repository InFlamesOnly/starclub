//
//  ForgotePasswordSwipeView.swift
//  StarClub
//
//  Created by macOS on 14.12.17.
//  Copyright © 2017 macOS. All rights reserved.
//

import UIKit

protocol ForgotePasswordSwipeViewDelegate : NSObjectProtocol {
    func goToCodeSwipeView()
    func backToLoginSwipeView()
}


class ForgotePasswordSwipeView: UIView, UITextFieldDelegate {
    
    var delegate : ForgotePasswordSwipeViewDelegate?
    @IBOutlet weak var loginTextField: CustomePlaceholderTextField!
    @IBOutlet weak var loginBorderView: RoundViewWithBorder!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @IBAction func back(_ sender: Any) {
        print("tap back to login view controller")
        self.delegate?.backToLoginSwipeView()
    }
    
    @IBAction func getCodeFromServer(_ sender: Any) {
        print("tap get code from server")
        self.delegate?.goToCodeSwipeView()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == loginTextField {
            if textField.text?.count == 0 {
                textField.text = TELEPHONECODE
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == loginTextField {
            if textField.text == TELEPHONECODE {
                textField.text = ""
                textField.placeholder = Style.Login.text
            }
            if textField.text ==  TELEPHONECODE {
                textField.placeholder = Style.Login.text
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == loginTextField {
            if range.location < 19 {
                return TelephoneNumberValidator.formattedTextField(textField, shouldChangeCharactersIn: range, replacementString: string)
            } else {
                return false
            }
        }
        return true
    }
}

