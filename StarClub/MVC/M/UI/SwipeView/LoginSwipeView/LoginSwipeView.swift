//
//  LoginSwipeView.swift
//  StarClub
//
//  Created by macOS on 19.12.17.
//  Copyright © 2017 macOS. All rights reserved.
//

import UIKit

protocol LoginSwipeViewDelegate : NSObjectProtocol {
    func loginUser()
    func goToForgotPasswordSwipeView()
}

class LoginSwipeView: UIView, UITextFieldDelegate {

    var delegate : LoginSwipeViewDelegate?
    
    @IBOutlet weak var loginTextField: CustomePlaceholderTextField!
    @IBOutlet weak var passwordTextField: CustomePlaceholderTextField!
    @IBOutlet weak var loginBorderView: RoundViewWithBorder!
    @IBOutlet weak var passwordBorderView: RoundViewWithBorder!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @IBAction func login(_ sender: Any) {
        print("tap login")
        self.delegate?.loginUser()
    }
    
    @IBAction func changePassword(_ sender: Any) {
        print("tap forgot password")
        self.delegate?.goToForgotPasswordSwipeView()
    }
    
    //MARK: - TextFieldDelegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == loginTextField {
            if textField.text?.count == 0 {
                textField.text = TELEPHONECODE
            }
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("return tap")
        self.passwordTextField.resignFirstResponder()
        self.loginTextField.resignFirstResponder()
        self.delegate?.loginUser()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == loginTextField {
            if textField.text == TELEPHONECODE {
                textField.text = ""
                textField.placeholder = Style.Login.text
            }
            if textField.text ==  TELEPHONECODE {
                textField.placeholder = Style.Login.text
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == loginTextField {
            if range.location < 19 {
                return TelephoneNumberValidator.formattedTextField(textField, shouldChangeCharactersIn: range, replacementString: string)
            } else {
                return false
            }
        }
        return true
    }

}
