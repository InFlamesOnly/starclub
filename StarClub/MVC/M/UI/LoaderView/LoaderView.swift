//
//  LoaderView.swift
//  StarClub
//
//  Created by macOS on 19.12.17.
//  Copyright © 2017 macOS. All rights reserved.
//

import UIKit

class LoaderView: UIView {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.addBlur()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addBlur()
    }
    
    func startFromView (view : UIView) {
        self.frame = view.frame
        view.addSubview(self)
        self.addIndicator()
    }
    
    private func addBlur () {
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(blurEffectView)
    }
    
   private func addIndicator () {
        let indicator = SpringIndicator()
        indicator.frame.size = CGSize(width: 100, height: 100)
        indicator.center = self.center
        indicator.lineColor = PINK
        self.addSubview(indicator)
        indicator.start()
    }
}
