//
//  HolidayView.swift
//  StarClub
//
//  Created by macOS on 26.02.18.
//  Copyright © 2018 macOS. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class HolidayView: UIView {
    
    @IBOutlet weak var holidayImageView: UIImageView!
    @IBOutlet weak var holidayText: UILabel!

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    @IBAction func close(_ sender: Any) {
        self.removeFromSuperview()
    }
    
    private func addBlackBacground() {
        let view = UIView()
        view.frame = CGRect(x: self.frame.origin.x, y: self.frame.origin.y, width: self.frame.size.width, height: self.frame.size.height)
        view.backgroundColor = UIColor.black
        view.alpha = 0.6
        self.addSubview(view)
    }
    
    func createView(imageViewURL: String, text: String) {
        self.holidayText.text = text
        
        AF.request(imageViewURL).responseImage { response in
            if case .success(let image) = response.result {
                self.holidayImageView.image = image
                print("image downloaded: \(image)")
            }
        }
    }
    
    private func addImage() {
        let view = UIImageView()
        view.image = UIImage(named: "Holiday")
        view.frame.size = CGSize(width: 320, height: 320)
        view.center = self.center
        self.addSubview(view)
    }
}
