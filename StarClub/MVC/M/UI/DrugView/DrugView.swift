//
//  DrugView.swift
//  StarClub
//
//  Created by macOS on 23.12.17.
//  Copyright © 2017 macOS. All rights reserved.
//

import UIKit

class DrugView: UIView {
    
    @IBOutlet weak var drugInstructonWebView: UIWebView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
//        self.drugInstructonWebView.loadRequest(URLRequest(url: URL(fileURLWithPath: "www.google.com.ua")) )
//        self.addBlur()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
//        self.drugInstructonWebView.loadRequest(URLRequest(url: URL(fileURLWithPath: "www.google.com.ua")) )
//        self.addBlur()
    }
    
    @IBAction func close(_ sender: Any) {
        self.removeFromSuperview()
    }
    
    private func addBlur (){
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(blurEffectView)
    }
}
