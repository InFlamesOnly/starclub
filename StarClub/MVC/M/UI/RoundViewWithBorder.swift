//
//  RoundViewWithBorder.swift
//  StarClub
//
//  Created by macOS on 12.12.17.
//  Copyright © 2017 macOS. All rights reserved.
//

import UIKit

class RoundViewWithBorder: UIView {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.layer.cornerRadius = BORDERRADIUS
        self.clipsToBounds = true
        self.layer.borderColor = LIGHTGREY.cgColor
        self.layer.borderWidth = 1.0
    }
    
    func errorFields () {
        self.layer.borderColor = PINK.cgColor
    }
    func fieldsIsCorrect () {
       self.layer.borderColor = LIGHTGREY.cgColor
    }
}
