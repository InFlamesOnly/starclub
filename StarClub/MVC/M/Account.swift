//
//  Account.swift
//  StarClub
//
//  Created by macOS on 16.11.2018.
//  Copyright © 2018 macOS. All rights reserved.
//

import UIKit

class Account: NSObject {
    
    var id = 0
    var name = ""
    var token = ""
    var phone = ""
    
    override init() {
        super.init()
    }
    
    convenience init (id : Int, name : String, token : String) {
        self.init()
        self.id = id
        self.name = name
        self.token = token
    }
    
    convenience init(response : Dictionary<String, Any>) {
        self.init()
        self.id = response ["id"] as? Int ?? 0
        self.name = response ["name"] as? String ?? ""
        self.token = response ["token"] as? String ?? ""
        self.phone = response ["phone"] as? String ?? ""
    }

}
