//
//  AppDelegate.swift
//  StarClub
//
//  Created by macOS on 12.12.17.
//  Copyright © 2017 macOS. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import FirebaseMessaging
import Firebase
import UserNotifications
import RealmSwift
import KYDrawerController

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        self.registreNotificationToApp(application: application)
        UIApplication.shared.statusBarStyle = .darkContent
        IQKeyboardManager.shared.isEnabled = true
        let BarButtonItemAppearance = UIBarButtonItem.appearance()
        BarButtonItemAppearance.setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.clear], for: .normal)
        
        self.addRealmMigration()
        
        if self.checkUserIsFind() {
            self.menu()
        }
        
        if let remoteNotification = launchOptions?[.remoteNotification] as?  [AnyHashable : Any] {
            let aps = remoteNotification["aps" as String] as? [String:AnyObject]
            if let textJson = remoteNotification["gcm.notification.data"] as? String {
                if let data = textJson.data(using: String.Encoding.utf8) {
                    do {
                        if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:Any] {
                            if let contentId = json["content_id"] as? Int, let type = json["type"] as? Int {
                                if User().currentUser().token.isEmpty { return true }
                                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                    self.showPushViewCOntroller(contentId: contentId, type: type)
                                }
                            }
                        }
                    } catch {
                        print("Something went wrong")
                    }
                }
                print("data")
            }
        }
        return true
    }
    
    
    
    func addRealmMigration () {
        let config = Realm.Configuration(
            schemaVersion: 1,
            migrationBlock: { migration, oldSchemaVersion in
                if (oldSchemaVersion < 1) {
                }
            })
        Realm.Configuration.defaultConfiguration = config
        let realm = try! Realm()
    }
    
    func whiteNavigation () {
        UINavigationBar.appearance().barTintColor = .black
        UINavigationBar.appearance().tintColor = .white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        UINavigationBar.appearance().isTranslucent = false
    }
    
    func checkUserIsFind () -> Bool {
        let realm = try! Realm()
        let usersArray = realm.objects(User.self)
        if usersArray.count == 1 {
            return true
        } else {
            return false
        }
    }
    
    func menu () {
        RequsetManager.sharedInstance.isShowMedMenu(success: { (response) in
            let respBool = response ["show"] as? Int ?? 0
            if respBool == 1 {
                UserDefaults.standard.set(1, forKey: "isShow")
            } else {
                UserDefaults.standard.set(0, forKey: "isShow")
            }
        }) { (errorCode) in
            
        }
    }
    
    //MARK : - NOTIFICATION
    func registreNotificationToApp(application : UIApplication) {
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options: [UNAuthorizationOptions.sound ], completionHandler: { (granted, error) in
                if error == nil {
                    DispatchQueue.main.async {
                        UIApplication.shared.registerForRemoteNotifications()
                    }

                }
            })
        } else {
            let settings  = UIUserNotificationSettings(types: [UIUserNotificationType.alert , UIUserNotificationType.badge , UIUserNotificationType.sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
            DispatchQueue.main.async {
                application.registerForRemoteNotifications()
            }
            
        }
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("User Info === \(notification.request.content.userInfo)")
        if let notInfo = notification.request.content.userInfo as? NSDictionary {
            if let imageUrl = notInfo.value(forKey: "image_url") as? String {
                let text = notInfo.value(forKey: "text") as! String
                self.addHolidayFromServerRespons(imageUrl: imageUrl, text: text)
            }
        }
       
        if let aps = notification.request.content.userInfo["aps"] as? NSDictionary {
            if let alert = aps["alert"] as? NSDictionary {
                if let body = alert["body"] as? NSString {
                    if body == "Гороскоп на лето" {
                        UserDefaults.standard.set(true, forKey: "holiday")
                    }
                }
            }
        }
        // Handle code here.
        completionHandler([UNNotificationPresentationOptions.sound , UNNotificationPresentationOptions.alert , UNNotificationPresentationOptions.badge])
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("User Info === \(response.notification.request.content.userInfo)")
        let state: UIApplicationState = UIApplication.shared.applicationState
        
        if let notInfo = response.notification.request.content.userInfo as? NSDictionary {
            if let imageUrl = notInfo.value(forKey: "image_url") as? String {
            let text = notInfo.value(forKey: "text") as! String
            
            if state == .inactive {
                _ = Timer.scheduledTimer(withTimeInterval: 1.6, repeats: false) { (timer) in
                    self.addHolidayFromServerRespons(imageUrl: imageUrl, text: text)
                }
            }
            else {
               self.addHolidayFromServerRespons(imageUrl: imageUrl, text: text)
            }
            } else {
                if let textJson = notInfo["gcm.notification.data"] as? String {
                    if let data = textJson.data(using: String.Encoding.utf8) {
                        do {
                            if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:Any] {
                                if let contentId = json["content_id"] as? Int, let type = json["type"] as? Int {
                                    if User().currentUser().token.isEmpty { return }
                                    if state == .inactive {
                                        _ = Timer.scheduledTimer(withTimeInterval: 1.6, repeats: false) { (timer) in
                                                self.showPushViewCOntroller(contentId: contentId, type: type)
                                        }
                                    } else {
                                        self.showPushViewCOntroller(contentId: contentId, type: type)
                                    }
                                }
                            }

                        } catch {
                            print("Something went wrong")
                        }
                    }
                    print("data")
                }
            }
            
        }
        if let aps = response.notification.request.content.userInfo["aps"] as? NSDictionary {
            if let alert = aps["alert"] as? NSDictionary {
                if let body = alert["body"] as? NSString {
                    if body == "Гороскоп на лето" {
                        UserDefaults.standard.set(true, forKey: "holiday")
                    }
                }
            }
        }
        completionHandler()
    }
    
    func needMenuPushShowing () {
        
    }
    
    func showPushViewCOntroller (contentId : Int, type : Int) {
        if let rootViewController = UIApplication.topViewController() {
            if let drawer = rootViewController as? KYDrawerController {
                if let nc = drawer.drawerViewController as? UINavigationController {
                    if let myDrawer = nc.viewControllers.first as? DraverMenuVC {
                        self.goToTheNeedItem(type: type, contentId: contentId, vc: myDrawer)
                    }
                }
            }
        }
    }
    
    func goToTheNeedItem (type : Int, contentId : Int, vc : DraverMenuVC) {
        if type == 1 { vc.drawerMenu?.findDrawerFromName(name: "Стрічка") }
        if type == 2 { vc.drawerMenu?.findDrawerFromName(name: "Календар заходів") }
        if type == 3 { vc.drawerMenu?.findDrawerFromName(name: "Препарати") }
        if type == 4 { vc.drawerMenu?.findDrawerFromName(name: "Статті") }
        if type == 5 { vc.drawerMenu?.findDrawerFromName(name: "Конференції") }
        if type == 6 { vc.drawerMenu?.findDrawerFromName(name: "ICPC-2 Класифікація ПМД") }
        if type == 7 { vc.drawerMenu?.findDrawerFromName(name: "Популярні запитання") }
        if type == 8 { vc.drawerMenu?.findDrawerFromName(name: "Усі запитання") }
        if type == 9 { vc.drawerMenu?.findDrawerFromName(name: "Запитання щодо гіпертонічної хвороби") }
        if type == 10 { vc.drawerMenu?.findDrawerFromName(name: "Вопросы по симптоматической АГ") }
        if type == 11 { vc.drawerMenu?.findDrawerFromName(name: "Вопросы по СД 2 типа") }
        if type == 12 { vc.drawerMenu?.findDrawerFromName(name: "Консиліум щодо складних клінічних випадків") }
        if type == 13 { vc.drawerMenu?.findDrawerFromName(name: "Другие вопросы") }
        if type == 14 { vc.drawerMenu?.findDrawerFromName(name: "Індекс маси тіла") }
        if type == 15 { vc.drawerMenu?.findDrawerFromName(name: "Ризик ССЗ за шкалою SCORE") }
        if type == 16 { vc.drawerMenu?.findDrawerFromName(name: "Швидкість клубочкової фільтрації") }
        if type == 17 { vc.drawerMenu?.findDrawerFromName(name: "Бібліотека") }
        if type == 18 { vc.drawerMenu?.findDrawerFromName(name: "Відеокейси") }
        if type == 19 { vc.drawerMenu?.findDrawerFromName(name: "Консиліум") }
        if type == 20 { vc.drawerMenu?.findDrawerFromName(name: "Протоколи лікування") }
        if type == 21 { vc.drawerMenu?.findDrawerFromName(name: "Консиліум допомагає") }
        if type == 22 { vc.drawerMenu?.findDrawerFromName(name: "Опитування") }
    }
    
    func addHolidayFromServerRespons (imageUrl : String, text : String) {
        if let rootViewController = UIApplication.topViewController() {
            if let hv = Bundle.main.loadNibNamed("HolidayView", owner: self, options: nil)?.first as? HolidayView {
                hv.frame = CGRect(x: 0, y: 64, width: rootViewController.view.frame.size.width, height: rootViewController.view.frame.size.height - 64)
                rootViewController.view.addSubview(hv)
                hv.createView(imageViewURL: imageUrl, text: text)
            }
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let characterSet = CharacterSet(charactersIn: "<>")
        let deviceTokenString: String = ((deviceToken.description as NSString).trimmingCharacters(in: characterSet) as NSString).replacingOccurrences(of: " ", with: "") as String
        print(deviceTokenString)
    }
    
    func application(application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        Messaging.messaging().apnsToken = deviceToken as Data
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        let token = Messaging.messaging().fcmToken
        let user = User()
        user.firebaseToken = token!
        user.save()
        print("FCM token: \(token ?? "")")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        let isHoliday = userInfo["holiday"] as? Bool
        if isHoliday == true {
            let defaults = UserDefaults.standard
            defaults.set(true, forKey: "holiday")
        }
//        let defaults = UserDefaults.standard
//        if defaults.bool(forKey: "holiday") {
//            defaults.set(false, forKey: "holiday")
//        }
        print(userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        Messaging.messaging().appDidReceiveMessage(userInfo)
        print(userInfo)
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print(error.localizedDescription)
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        self.checkToken()
    }
    
    func checkToken () {
        RequsetManager.sharedInstance.checkUserToken(success: { (_ responseObject: Dictionary<String, Any>) in
        }) { (_ error: Int?) in
            if error == 408 {
                self.showErrorAndGoToStartScreen()
            }
            if error == 411 {
                self.showErrorAndGoToStartScreen()
            }
        }
    }
    
    func showErrorAndGoToStartScreen () {
        if let rootViewController = UIApplication.topViewController() {
            let alert = UIAlertController(title: "Ошибка", message: "Пользователь не найден", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                let user = User().currentUser()
                let newUser = User()
                newUser.specific = 0
                newUser.firebaseToken = ""
                newUser.token = ""
                newUser.name = ""
                newUser.achievement = 0
                newUser.push = 0
                newUser.updateUser(newUser: newUser)
                if user.token == "" {
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let controller = storyboard.instantiateViewController(withIdentifier: "StartVC") as! UINavigationController
                    rootViewController.present(controller, animated: true, completion: nil)
                }
            }))
            rootViewController.present(alert, animated: true, completion: nil)
        }
    }
    
}

extension UIApplication {
    class func topViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        
        if let tab = base as? UITabBarController {
            let moreNavigationController = tab.moreNavigationController
            
            if let top = moreNavigationController.topViewController, top.view.window != nil {
                return topViewController(base: top)
            } else if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        
        return base
    }
}
//
//extension UIWindow {
//    public var visibleViewController: UIViewController? {
//        return UIWindow.visibleViewController(from: rootViewController)
//    }
//    public static func visibleViewController(from viewController: UIViewController?) -> UIViewController? {
//        switch viewController {
//        case let navigationController as UINavigationController:
//            return UIWindow.visibleViewController(from: navigationController.visibleViewController ?? navigationController.topViewController)
//            
//        case let tabBarController as UITabBarController:
//            return UIWindow.visibleViewController(from: tabBarController.selectedViewController)
//            
//        case let presentingViewController where viewController?.presentedViewController != nil:
//            return UIWindow.visibleViewController(from: presentingViewController?.presentedViewController)
//            
//        default:
//            return viewController
//        }
//    }
//}

